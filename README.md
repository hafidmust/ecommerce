### Day 31
- testing room database
- testing datastore
- testing auth repository
### Day 30
- testing all function in api service

### Day 20
- implement api payment method (fulfillment)
- implement api rating

### Day 19
- create payment method
- layout rating
- layout transaction

### Day 18
- badge whishlist
- grid layout wishlist
- wishlist to cart
- checkout page
### Day 17
-wishlist (layout, dao, activity)
-update checkbox (done)

### Day 16
- total bayar
- checkbox pilih semua
- hapus

### Day 15
- checkbox cart

### Day 14
- cart add, update quantity, delete (done)

### Day 13
- create product review (done)
- add viewpager product (done)
### Day 12
- New chapter (detail product, review, add to cart)

### Day 11
- Filter
- Show shimmer

### Day 10
- Search Product
- Switch Linear / Grid layout

### Day 9
- fixing bottomsheet, top app bar
- add error view paging, fixing 
- grid layout

### Day 8
- membuat bottom sheet dinamis, termasuk membuat horizontalnya
- hit api product paging

### Day 7
- New chapter -> Store
- Migrating to Compose :construction:

### Day 6
- implement authenticator :construction:
- fixing flow profile

### Day 5
- intent camera :white_check_mark:
- save token to datastore :white_check_mark:
- hit profile api :white_check_mark:
- datastore onboarding :white
