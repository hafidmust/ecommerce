package com.hafidmust.ecommerce.core.source.network.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TokenRequest(

    @Json(name = "token")
    val token: String
)
