package com.hafidmust.ecommerce.core.source.network.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class StoreResponse(

    @Json(name = "code")
    val code: Int,

    @Json(name = "message")
    val message: String,

    @Json(name = "data")
    val data: DataStore,
)

@JsonClass(generateAdapter = true)
data class ItemsItem(

    @Json(name = "image")
    val image: String,

    @Json(name = "sale")
    val sale: Int,

    @Json(name = "productId")
    val productId: String,

    @Json(name = "store")
    val store: String,

    @Json(name = "productRating")
    val productRating: Any,

    @Json(name = "brand")
    val brand: String,

    @Json(name = "productName")
    val productName: String,

    @Json(name = "productPrice")
    val productPrice: Int
)

@JsonClass(generateAdapter = true)
data class DataStore(

    @Json(name = "pageIndex")
    val pageIndex: Int,

    @Json(name = "itemsPerPage")
    val itemsPerPage: Int,

    @Json(name = "currentItemCount")
    val currentItemCount: Int,

    @Json(name = "totalPages")
    val totalPages: Int,

    @Json(name = "items")
    val items: List<ItemsItem>
)
