package com.hafidmust.ecommerce.core.source.network

sealed class Result<out R> {
    data class Success<out T>(val data: T) : Result<T>()
    data class Error(val msg: String) : Result<Nothing>()
    object Loading : Result<Nothing>()
}
