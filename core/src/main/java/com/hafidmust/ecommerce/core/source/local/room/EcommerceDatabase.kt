package com.hafidmust.ecommerce.core.source.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.hafidmust.ecommerce.core.source.local.entity.CartItemEntity
import com.hafidmust.ecommerce.core.source.local.entity.DataWishListEntity
import com.hafidmust.ecommerce.core.source.local.entity.NotificationEntity

@Database(
    entities = [CartItemEntity::class, DataWishListEntity::class, NotificationEntity::class],
    version = 5,
    exportSchema = false
)
abstract class EcommerceDatabase : RoomDatabase() {
    abstract fun cartDao(): CartDao
    abstract fun wishlistDao(): WishlistDao
    abstract fun notificationDao(): NotificationDao
}
