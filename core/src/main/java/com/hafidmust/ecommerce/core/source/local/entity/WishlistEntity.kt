package com.hafidmust.ecommerce.core.source.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

data class ProductVariantItemWishlist(
    val variantPrice: Int,
    val variantName: String
)

@Entity(tableName = "wishlist_item")
data class DataWishListEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val image: String,
    val productId: String,
    val description: String,
    val totalRating: Int,
    val store: String,
    val productName: String,
    val totalSatisfaction: Int,
    val sale: Int,
    val productVariant: String,
    val stock: Int,
    val productRating: Double,
    val brand: String,
    val productPrice: Int,
    val totalReview: Int,
    val isChecked: Boolean = false
)

fun DataWishListEntity.toCartItemEntity(): CartItemEntity {
    return CartItemEntity(
        image = image,
        productId = productId,
        description = description,
        totalRating = totalRating,
        store = store,
        productName = productName,
        totalSatisfaction = totalSatisfaction,
        sale = sale,
        productVariant = productVariant,
        stock = stock,
        productRating = productRating,
        brand = brand,
        productPrice = productPrice,
        totalReview = totalReview,
        isChecked = false
    )
}
