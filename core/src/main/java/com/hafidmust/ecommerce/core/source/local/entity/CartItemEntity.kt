package com.hafidmust.ecommerce.core.source.local.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.errorprone.annotations.Keep
import com.hafidmust.ecommerce.core.source.network.request.ItemsItem
import com.hafidmust.ecommerce.core.source.network.request.TransactionRequest
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "cart_item")
data class CartItemEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val productId: String,
    val productName: String,
    val productPrice: Int,
    val image: String,
    val brand: String,
    val description: String,
    val store: String,
    val sale: Int,
    val stock: Int,
    val totalRating: Int,
    val totalReview: Int,
    val totalSatisfaction: Int,
    val productRating: Double,
    val productVariant: String,
    val quantity: Int = 1,
    var isChecked: Boolean = false,
) : Parcelable

fun CartItemEntity.toFulfillment(payment: String) = TransactionRequest(
    payment = payment,
    items = listOf(
        ItemsItem(
            quantity = quantity,
            productId = productId,
            variantName = productVariant
        )
    )
)

fun CartItemEntity.toCheckout() = CheckoutEntity(
    productId = productId,
    productName = productName,
    productPrice = productPrice,
    image = image,
    brand = brand,
    description = description,
    store = store,
    sale = sale,
    stock = stock,
    totalRating = totalRating,
    totalReview = totalReview,
    totalSatisfaction = totalSatisfaction,
    productRating = productRating,
    productVariant = productVariant,
    quantity = quantity,
    isChecked = isChecked
)
