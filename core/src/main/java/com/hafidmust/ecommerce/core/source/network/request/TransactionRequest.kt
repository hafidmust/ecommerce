package com.hafidmust.ecommerce.core.source.network.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TransactionRequest(

    @Json(name = "payment")
    val payment: String,

    @Json(name = "items")
    val items: List<ItemsItem>
)

@JsonClass(generateAdapter = true)
data class ItemsItem(

    @Json(name = "quantity")
    val quantity: Int,

    @Json(name = "productId")
    val productId: String,

    @Json(name = "variantName")
    val variantName: String
)
