package com.hafidmust.ecommerce.core.source.local.entity

import com.hafidmust.ecommerce.core.source.network.request.ItemsItem
import com.hafidmust.ecommerce.core.source.network.request.TransactionRequest

data class CheckoutEntity(
    val id: Int = 0,
    val productId: String,
    val productName: String,
    val productPrice: Int,
    val image: String,
    val brand: String,
    val description: String,
    val store: String,
    val sale: Int,
    val stock: Int,
    val totalRating: Int,
    val totalReview: Int,
    val totalSatisfaction: Int,
    val productRating: Double,
    val productVariant: String,
    var quantity: Int = 1,
    var isChecked: Boolean = false,
)

fun CheckoutEntity.toFulfillment(payment: String) = TransactionRequest(
    payment = payment,
    items = listOf(
        ItemsItem(
            quantity = quantity,
            productId = productId,
            variantName = productVariant
        )
    )
)
