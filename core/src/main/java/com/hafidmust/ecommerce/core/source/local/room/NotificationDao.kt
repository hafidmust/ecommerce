package com.hafidmust.ecommerce.core.source.local.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.hafidmust.ecommerce.core.source.local.entity.NotificationEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface NotificationDao {

    @Query("SELECT * FROM notification")
    fun getAllNotification(): Flow<List<NotificationEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNotification(data: NotificationEntity)

    @Query("UPDATE notification SET isRead = :isRead WHERE id = :id")
    suspend fun updateIsRead(isRead: Boolean, id: Int)

    @Query("SELECT COUNT(*) FROM notification WHERE isRead = 0")
    fun getUnreadNotification(): Flow<Int>

    @Query("DELETE FROM notification")
    fun deleteAllNotification()
}
