package com.hafidmust.ecommerce.core.source.network.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ProductReviewResponse(
    @Json(name = "code")
    val code: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "data")
    val `data`: List<DataReview>
) {
    @JsonClass(generateAdapter = true)
    data class DataReview(
        @Json(name = "userName")
        val userName: String,
        @Json(name = "userImage")
        val userImage: String,
        @Json(name = "userRating")
        val userRating: Int,
        @Json(name = "userReview")
        val userReview: String
    )
}
