package com.hafidmust.ecommerce.core.source.network.response

import com.hafidmust.ecommerce.core.source.local.entity.CartItemEntity
import com.hafidmust.ecommerce.core.source.local.entity.DataWishListEntity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DetailProductResponse(
    @Json(name = "code")
    val code: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "data")
    val `data`: Data
) {
    @JsonClass(generateAdapter = true)
    data class Data(
        @Json(name = "productId")
        val productId: String,
        @Json(name = "productName")
        val productName: String,
        @Json(name = "productPrice")
        val productPrice: Int,
        @Json(name = "image")
        val image: List<String>,
        @Json(name = "brand")
        val brand: String,
        @Json(name = "description")
        val description: String,
        @Json(name = "store")
        val store: String,
        @Json(name = "sale")
        val sale: Int,
        @Json(name = "stock")
        val stock: Int,
        @Json(name = "totalRating")
        val totalRating: Int,
        @Json(name = "totalReview")
        val totalReview: Int,
        @Json(name = "totalSatisfaction")
        val totalSatisfaction: Int,
        @Json(name = "productRating")
        val productRating: Double,
        @Json(name = "productVariant")
        val productVariant: List<ProductVariant>
    ) {
        @JsonClass(generateAdapter = true)
        data class ProductVariant(
            @Json(name = "variantName")
            val variantName: String,
            @Json(name = "variantPrice")
            val variantPrice: Int
        )
    }
}

fun DetailProductResponse.Data.toCartEntity(selectedVariant: String): CartItemEntity {
    return CartItemEntity(
        productId = productId,
        productName = productName,
        productPrice = productPrice,
        image = image[0],
        brand = brand,
        description = description,
        store = store,
        sale = sale,
        stock = stock,
        totalRating = totalRating,
        totalReview = totalReview,
        totalSatisfaction = totalSatisfaction,
        productRating = productRating,
        productVariant = selectedVariant
    )
}

fun DetailProductResponse.Data.toWishlistEntity(): DataWishListEntity {
    return DataWishListEntity(
        image = image[0],
        productId = productId,
        description = description,
        totalRating = totalRating,
        store = store,
        productName = productName,
        totalSatisfaction = totalSatisfaction,
        sale = sale,
        productVariant = productVariant[0].variantName,
        stock = stock,
        productRating = productRating,
        brand = brand,
        productPrice = productPrice,
        totalReview = totalReview,
        isChecked = false
    )
}
