package com.hafidmust.ecommerce.core.source.local.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.hafidmust.ecommerce.core.source.local.entity.CartItemEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface CartDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(cartItemEntity: CartItemEntity)

    @Delete
    suspend fun delete(vararg cartItemEntity: CartItemEntity)

    @Update
    suspend fun update(vararg cartItemEntity: CartItemEntity)

    @Query("SELECT * FROM cart_item")
    fun getAllCart(): Flow<List<CartItemEntity>>

    @Query("SELECT * FROM cart_item WHERE productId = :productId")
    fun getCartByProductId(productId: String): Flow<CartItemEntity>

    @Query("UPDATE cart_item SET quantity = :newQuantity WHERE productId = :productId")
    suspend fun updateQuantity(productId: String, newQuantity: Int)

    @Query("UPDATE cart_item SET isChecked = :checked WHERE productId = :productId ")
    suspend fun updateCheckCart(checked: Boolean, productId: String)

    @Query("SELECT COUNT(productId) FROM cart_item")
    fun getQuantity(): Flow<Int?>

    @Query("SELECT quantity FROM cart_item WHERE productId = :productId")
    fun getQuantityByProductId(productId: String): Flow<Int?>

    @Query("DELETE FROM cart_item")
    fun deleteAllCart()
}
