package com.hafidmust.ecommerce.core.source.local.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.hafidmust.ecommerce.core.source.local.entity.DataWishListEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface WishlistDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(data: DataWishListEntity)

    @Delete
    suspend fun delete(data: DataWishListEntity)

    @Query("SELECT * FROM wishlist_item order by id desc")
    fun getWishlist(): Flow<List<DataWishListEntity>>

    @Update
    suspend fun update(data: DataWishListEntity)

    @Query("SELECT isChecked FROM wishlist_item WHERE productId = :productId")
    fun getIsChecked(productId: String): Flow<Boolean>

    @Query("UPDATE wishlist_item SET isChecked = :isChecked WHERE productId = :productId")
    suspend fun updateIsChecked(isChecked: Boolean, productId: String)

    @Query("SELECT COUNT(*) FROM wishlist_item")
    fun getQuantity(): Flow<Int>

    @Query("SELECT * FROM wishlist_item WHERE productId = :productId")
    fun getWishlistbyId(productId: String): Flow<DataWishListEntity>

    @Query("DELETE FROM wishlist_item")
    fun deleteAllWishlist()
}
