package com.hafidmust.ecommerce.core.di

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.hafidmust.ecommerce.core.source.SessionManager
import com.hafidmust.ecommerce.core.source.network.ApiService
import com.hafidmust.ecommerce.core.source.network.AuthAuthenticator
import com.hafidmust.ecommerce.core.source.network.AuthInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideAuthAuthenticator(sessionManager: SessionManager, @ApplicationContext context: Context): AuthAuthenticator = AuthAuthenticator(
        sessionManager = sessionManager,
        context = context
    )

    @Provides
    @Singleton
    fun provideOkHttpClient(
        @ApplicationContext context: Context,
        authInterceptor: AuthInterceptor,
        authAuthenticator: AuthAuthenticator
    ): OkHttpClient {
        return OkHttpClient.Builder()
//            .addInterceptor(ApiKeyInterceptor())
//            .addInterceptor(authInterceptor)
            .authenticator(authAuthenticator)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .addInterceptor(ChuckerInterceptor(context))
            .build()
    }

    @Singleton
    @Provides
    fun provideConverterFactory(): MoshiConverterFactory = MoshiConverterFactory.create()

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient, moshiConverterFactory: MoshiConverterFactory): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://192.168.153.125:5000")
            .client(okHttpClient)
            .addConverterFactory(moshiConverterFactory)
            .build()
    }

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }
}
class ApiKeyInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val requestBuilder = originalRequest.newBuilder()
            .header("API_KEY", "6f8856ed-9189-488f-9011-0ff4b6c08edc")
        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}
