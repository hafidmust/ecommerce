package com.hafidmust.ecommerce.core.source.network.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RatingRequest(
    @Json(name = "invoiceId")
    val invoiceId: String,
    @Json(name = "rating")
    val rating: Int,
    @Json(name = "review")
    val review: String
)
