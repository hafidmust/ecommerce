package com.hafidmust.ecommerce.core.source.network.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TransactionResponse(
    @Json(name = "code")
    val code: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "data")
    val `data`: List<Data>
) {
    @JsonClass(generateAdapter = true)
    data class Data(
        @Json(name = "invoiceId")
        val invoiceId: String,
        @Json(name = "status")
        val status: Boolean,
        @Json(name = "date")
        val date: String,
        @Json(name = "time")
        val time: String,
        @Json(name = "payment")
        val payment: String,
        @Json(name = "total")
        val total: Int,
        @Json(name = "items")
        val items: List<Item>,
        @Json(name = "rating")
        val rating: Int?,
        @Json(name = "review")
        val review: String?,
        @Json(name = "image")
        val image: String,
        @Json(name = "name")
        val name: String
    ) {
        @JsonClass(generateAdapter = true)
        data class Item(
            @Json(name = "productId")
            val productId: String,
            @Json(name = "variantName")
            val variantName: String,
            @Json(name = "quantity")
            val quantity: Int
        )
    }
}
