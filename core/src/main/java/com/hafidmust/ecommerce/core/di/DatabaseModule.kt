package com.hafidmust.ecommerce.core.di

import android.content.Context
import androidx.room.Room
import com.hafidmust.ecommerce.core.source.local.room.EcommerceDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun provideEcommerceDatabase(
        @ApplicationContext context: Context
    ): EcommerceDatabase {
        return Room.databaseBuilder(
            context,
            EcommerceDatabase::class.java,
            "Ecommerce.db"
        ).fallbackToDestructiveMigration().build()
    }

    @Provides
    @Singleton
    fun provideCartDao(database: EcommerceDatabase) = database.cartDao()

    @Provides
    @Singleton
    fun provideWishlistDao(database: EcommerceDatabase) = database.wishlistDao()

    @Provides
    @Singleton
    fun provideNotificationDao(database: EcommerceDatabase) = database.notificationDao()
}
