package com.hafidmust.ecommerce.core.source.network

import com.hafidmust.ecommerce.core.source.network.request.RatingRequest
import com.hafidmust.ecommerce.core.source.network.request.TokenRequest
import com.hafidmust.ecommerce.core.source.network.request.TransactionRequest
import com.hafidmust.ecommerce.core.source.network.request.UserRequest
import com.hafidmust.ecommerce.core.source.network.response.DetailPayment
import com.hafidmust.ecommerce.core.source.network.response.DetailProductResponse
import com.hafidmust.ecommerce.core.source.network.response.FulfillmentResponse
import com.hafidmust.ecommerce.core.source.network.response.LoginResponse
import com.hafidmust.ecommerce.core.source.network.response.ProductReviewResponse
import com.hafidmust.ecommerce.core.source.network.response.ProfileResponse
import com.hafidmust.ecommerce.core.source.network.response.RatingResponse
import com.hafidmust.ecommerce.core.source.network.response.RefreshResponse
import com.hafidmust.ecommerce.core.source.network.response.RegisterResponse
import com.hafidmust.ecommerce.core.source.network.response.SearchResponse
import com.hafidmust.ecommerce.core.source.network.response.StoreResponse
import com.hafidmust.ecommerce.core.source.network.response.TransactionResponse
import okhttp3.MultipartBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @POST("register")
    suspend fun register(
        @Header("API_KEY") apiKey: String,
        @Body userRequest: UserRequest
    ): RegisterResponse

    @POST("login")
    suspend fun login(
        @Header("API_KEY") apiKey: String,
        @Body userRequest: UserRequest
    ): LoginResponse

    @POST("refresh")
    suspend fun refresh(
        @Header("API_KEY") apiKey: String,
        @Body token: TokenRequest
    ): retrofit2.Response<RefreshResponse>

    @Multipart
    @POST("profile")
    suspend fun profile(
        @Header("Authorization") token: String,
        @Part userImage: MultipartBody.Part?,
        @Part user: MultipartBody.Part,
    ): ProfileResponse

    @POST("search?query=lenovo")
    suspend fun testApi(
        @Header("Authorization") token: String
    ): SearchResponse

    @POST("products")
    suspend fun products(
        @Header("Authorization") token: String,
        @Query("search") search: String?,
        @Query("brand") brand: String?,
        @Query("lowest") lowest: Int?,
        @Query("highest") highest: Int?,
        @Query("sort") sort: String?,
        @Query("limit") limit: Int?,
        @Query("page") page: Int?,
    ): StoreResponse

    @POST("search")
    suspend fun search(
        @Header("Authorization") token: String,
        @Query("query") query: String
    ): SearchResponse

    @GET("products/{id}")
    suspend fun productDetail(
        @Header("Authorization") token: String,
        @Path("id") productId: String
    ): DetailProductResponse

    @GET("review/{id}")
    suspend fun productReview(
        @Header("Authorization") token: String,
        @Path("id") productId: String
    ): ProductReviewResponse

    @GET("payment")
    suspend fun paymentMethod(
        @Header("Authorization") token: String,
    ): DetailPayment

    @POST("fulfillment")
    suspend fun fulfillment(
        @Header("Authorization") token: String,
        @Body request: TransactionRequest
    ): FulfillmentResponse

    @POST("rating")
    suspend fun rating(
        @Header("Authorization") token: String,
        @Body request: RatingRequest
    ): RatingResponse

    @GET("transaction")
    suspend fun transaction(
        @Header("Authorization") token: String,
    ): TransactionResponse
}
