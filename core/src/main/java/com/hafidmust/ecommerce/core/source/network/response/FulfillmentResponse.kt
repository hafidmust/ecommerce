package com.hafidmust.ecommerce.core.source.network.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FulfillmentResponse(
    @Json(name = "code")
    val code: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "data")
    val `data`: Data
) {
    @JsonClass(generateAdapter = true)
    data class Data(
        @Json(name = "invoiceId")
        val invoiceId: String,
        @Json(name = "status")
        val status: Boolean,
        @Json(name = "date")
        val date: String,
        @Json(name = "time")
        val time: String,
        @Json(name = "payment")
        val payment: String,
        @Json(name = "total")
        val total: Int
    )
}
