package com.hafidmust.ecommerce.core.source.network.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ProfileResponse(

    @Json(name = "code")
    val code: Int,

    @Json(name = "data")
    val data: DataProfile,

    @Json(name = "message")
    val message: String
)

@JsonClass(generateAdapter = true)
data class DataProfile(

    @Json(name = "userImage")
    val userImage: String,

    @Json(name = "userName")
    val userName: String
)
