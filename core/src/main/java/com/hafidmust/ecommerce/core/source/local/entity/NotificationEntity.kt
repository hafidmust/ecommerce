package com.hafidmust.ecommerce.core.source.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "notification")
data class NotificationEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val title: String,
    val body: String,
    val image: String,
    val type: String,
    val date: String,
    val time: String,
    val isRead: Boolean = false
)
