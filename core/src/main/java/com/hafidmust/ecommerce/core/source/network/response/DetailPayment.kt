package com.hafidmust.ecommerce.core.source.network.response

import android.os.Parcelable
import androidx.annotation.Keep
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
@JsonClass(generateAdapter = true)
data class DetailPayment(

    @Json(name = "code")
    val code: Int,

    @Json(name = "data")
    val data: List<DataItem>,

    @Json(name = "message")
    val message: String
)

@JsonClass(generateAdapter = true)
data class DataItem(

    @Json(name = "item")
    val item: List<ItemItemPayment>,

    @Json(name = "title")
    val title: String
)

@Keep
@Parcelize
@JsonClass(generateAdapter = true)
data class ItemItemPayment(

    @Json(name = "image")
    val image: String,

    @Json(name = "label")
    val label: String,

    @Json(name = "status")
    val status: Boolean
) : Parcelable
