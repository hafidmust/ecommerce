package com.hafidmust.ecommerce.core.source.network.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserRequest(

    @Json(name = "password")
    val password: String,

    @Json(name = "firebaseToken")
    val firebaseToken: String,

    @Json(name = "email")
    val email: String
)
