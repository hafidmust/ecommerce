package com.hafidmust.ecommerce.core.source.network.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PaymentMethodResponse(
    @Json(name = "code")
    val code: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "data")
    val `data`: List<DataPayment>
) {
    data class DataPayment(
        @Json(name = "title")
        val title: String,
        @Json(name = "item")
        val item: List<ItemPaymentMethod>
    ) {
        data class ItemPaymentMethod(
            @Json(name = "label")
            val label: String,
            @Json(name = "image")
            val image: String,
            @Json(name = "status")
            val status: Boolean
        )
    }
}
