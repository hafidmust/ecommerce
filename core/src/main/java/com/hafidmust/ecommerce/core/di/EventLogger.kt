package com.hafidmust.ecommerce.core.di

import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject

class EventLogger @Inject constructor(private val firebaseAnalytics: FirebaseAnalytics) {
    fun logEvent(eventName: String, bundle: Bundle?) =
        firebaseAnalytics.logEvent(eventName, bundle)

    fun logEventItem(value: Bundle?) = firebaseAnalytics.logEvent(
        FirebaseAnalytics.Event.SELECT_ITEM,
        value
    )
    fun logParamEvent(value: Bundle?) = firebaseAnalytics.logEvent(
        FirebaseAnalytics.Param.ITEM_NAME,
        value
    )

    fun logSearch(value: Bundle?) = firebaseAnalytics.logEvent(
        FirebaseAnalytics.Event.SEARCH,
        value
    )

    fun logViewItemList(data: Bundle?) = firebaseAnalytics.logEvent(
        FirebaseAnalytics.Event.VIEW_ITEM_LIST,
        data
    )

    fun logSelectItem(data: Bundle?) = firebaseAnalytics.logEvent(
        FirebaseAnalytics.Event.SELECT_ITEM,
        data
    )

    fun logViewItem(data: Bundle?) = firebaseAnalytics.logEvent(
        FirebaseAnalytics.Event.VIEW_ITEM,
        data
    )
    fun logAddToCart(data: Bundle?) = firebaseAnalytics.logEvent(
        FirebaseAnalytics.Event.ADD_TO_CART,
        data
    )

    fun logRemoveCart(data: Bundle?) = firebaseAnalytics.logEvent(
        FirebaseAnalytics.Event.REMOVE_FROM_CART,
        data
    )

    fun logViewCart(data: Bundle?) = firebaseAnalytics.logEvent(
        FirebaseAnalytics.Event.VIEW_CART,
        data
    )

    fun logAddToWishlist(data: Bundle?) = firebaseAnalytics.logEvent(
        FirebaseAnalytics.Event.ADD_TO_WISHLIST,
        data
    )

    fun logBeginCheckout(data: Bundle?) = firebaseAnalytics.logEvent(
        FirebaseAnalytics.Event.BEGIN_CHECKOUT,
        data
    )

    fun logPaymentInfo(data: Bundle?) = firebaseAnalytics.logEvent(
        FirebaseAnalytics.Event.ADD_PAYMENT_INFO,
        data
    )

    fun logPurchase(data: Bundle?) = firebaseAnalytics.logEvent(
        FirebaseAnalytics.Event.PURCHASE,
        data
    )

    fun logButtonClick(data: Bundle?) = firebaseAnalytics.logEvent(
        FirebaseAnalytics.Event.SELECT_CONTENT,
        data
    )
}
