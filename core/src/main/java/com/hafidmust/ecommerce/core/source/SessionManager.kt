package com.hafidmust.ecommerce.core.source

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class SessionManager @Inject constructor(
    private val dataStore: DataStore<Preferences>
) {

    private val URI_KEY = stringPreferencesKey("URI_KEY")
    private val NAME_KEY = stringPreferencesKey("NAME_KEY")
    private val ACCESS_TOKEN_KEY = stringPreferencesKey("ACCESS_TOKEN_KEY")
    private val REFRESH_TOKEN_KEY = stringPreferencesKey("REFRESH_TOKEN_KEY")
    private val ONBOARDING_KEY = booleanPreferencesKey("ONBOARDING_KEY")
    private val THEME_KEY = booleanPreferencesKey("THEME_KEY")

    suspend fun setTheme(isDark: Boolean) {
        dataStore.edit { preferences ->
            preferences[THEME_KEY] = isDark
        }
    }

    suspend fun getTheme(): Flow<Boolean> {
        return dataStore.data.map { pref ->
            pref[THEME_KEY] ?: false
        }
    }

    suspend fun setAccessToken(accessToken: String) {
        dataStore.edit { preferences ->
            preferences[ACCESS_TOKEN_KEY] = accessToken
        }
    }

    suspend fun setRefreshToken(refreshToken: String) {
        dataStore.edit { pref ->
            pref[REFRESH_TOKEN_KEY] = refreshToken
        }
    }

    suspend fun setFirstInstall(firstInstall: Boolean) {
        dataStore.edit { preferences ->
            preferences[ONBOARDING_KEY] = firstInstall
        }
    }

    suspend fun setProfile(uriImage: String, userName: String) {
        dataStore.edit { preferences ->
            preferences[URI_KEY] = uriImage
            preferences[NAME_KEY] = userName
        }
    }

    suspend fun deleteToken() {
        dataStore.edit { pref ->
            pref.remove(ACCESS_TOKEN_KEY)
            pref.remove(REFRESH_TOKEN_KEY)
//            pref[ACCESS_TOKEN_KEY] ?: ""
//            pref[REFRESH_TOKEN_KEY] ?: ""
        }
    }

    suspend fun deleteAll() {
        dataStore.edit {
            it.clear()
        }
    }

    suspend fun deletePreference() {
        dataStore.edit { pref ->
            pref.remove(ACCESS_TOKEN_KEY)
            pref.remove(REFRESH_TOKEN_KEY)
            pref.remove(NAME_KEY)
            pref.remove(URI_KEY)
//            pref[ACCESS_TOKEN_KEY] ?: ""
//            pref[REFRESH_TOKEN_KEY] ?: ""
        }
    }

    fun getAccessToken(): Flow<String> {
        return dataStore.data.map { pref ->
            pref[ACCESS_TOKEN_KEY] ?: ""
        }
    }

    fun getNameProfile(): Flow<String> {
        return dataStore.data.map { pref ->
            pref[NAME_KEY] ?: ""
        }
    }

    fun getUriProfile(): Flow<String> {
        return dataStore.data.map { pref ->
            pref[URI_KEY] ?: ""
        }
    }

    fun getRefreshToken(): Flow<String> {
        return dataStore.data.map { pref ->
            pref[REFRESH_TOKEN_KEY] ?: ""
        }
    }

    fun getFirstInstall(): Flow<Boolean> {
        return dataStore.data.map { pref ->
            pref[ONBOARDING_KEY] ?: true
        }
    }
}
