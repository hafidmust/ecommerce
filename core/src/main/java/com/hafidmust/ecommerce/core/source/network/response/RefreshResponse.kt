package com.hafidmust.ecommerce.core.source.network.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RefreshResponse(
    @Json(name = "code")
    val code: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "data")
    val data: com.hafidmust.ecommerce.core.source.network.response.Data
) {

    data class Data(
        @Json(name = "accessToken")
        val accessToken: String,
        @Json(name = "refreshToken")
        val refreshToken: String,
        @Json(name = "expiresAt")
        val expiresAt: Int
    )
}
