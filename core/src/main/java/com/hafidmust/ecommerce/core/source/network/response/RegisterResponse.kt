package com.hafidmust.ecommerce.core.source.network.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RegisterResponse(

    @Json(name = "code")
    val code: Int,

    @Json(name = "data")
    val data: Data,

    @Json(name = "message")
    val message: String
)

@JsonClass(generateAdapter = true)
data class Data(

    @Json(name = "accessToken")
    val accessToken: String,

    @Json(name = "expiresAt")
    val expiresAt: Int,

    @Json(name = "refreshToken")
    val refreshToken: String
)
