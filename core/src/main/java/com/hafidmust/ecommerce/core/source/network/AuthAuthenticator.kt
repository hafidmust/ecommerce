package com.hafidmust.ecommerce.core.source.network

import android.content.Context
import android.util.Log
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.hafidmust.ecommerce.core.source.SessionManager
import com.hafidmust.ecommerce.core.source.network.request.TokenRequest
import com.hafidmust.ecommerce.core.source.network.response.RefreshResponse
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Inject

class AuthAuthenticator @Inject constructor(
    @ApplicationContext private val context: Context,
    private val sessionManager: SessionManager
) : Authenticator {
    override fun authenticate(route: Route?, response: Response): Request? {
        synchronized(this) {
            return runBlocking {
                try {
                    val token = runBlocking {
                        sessionManager.getRefreshToken().first()
                    }

                    val newToken = getNewToken(token)

                    if (!newToken.isSuccessful || newToken.body() == null) {
                        Log.d("token2", newToken.body()?.data?.accessToken.toString())
                        // delete token
                        sessionManager.deleteToken()
                        Log.d("isi token", sessionManager.getRefreshToken().first())
                    }
                    newToken.body()?.let {
                        sessionManager.setAccessToken(it.data.accessToken)
                        Log.d("zzzzz", it.data.accessToken)
                        sessionManager.setRefreshToken(it.data.refreshToken)
                        response.request.newBuilder()
                            .header("Authorization", "Bearer ${it.data.accessToken}")
                            .build()
                    }
                } catch (e: HttpException) {
                    if (e.code() == 401) {
                        sessionManager.deleteToken()
                        null
                    } else {
                        null
                    }
                }
            }
        }
    }

    private suspend fun getNewToken(refreshToken: String?): retrofit2.Response<RefreshResponse> {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.HEADERS
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(ChuckerInterceptor(context))
            .addInterceptor(loggingInterceptor).build()

        val retrofit = Retrofit.Builder()
            .baseUrl("http://192.168.153.125:5000")
            .addConverterFactory(MoshiConverterFactory.create())
            .client(okHttpClient)
            .build()
        val service = retrofit.create(ApiService::class.java)
        return service.refresh(
            apiKey = "6f8856ed-9189-488f-9011-0ff4b6c08edc",
            token = TokenRequest(refreshToken.toString())
        )
    }
}
