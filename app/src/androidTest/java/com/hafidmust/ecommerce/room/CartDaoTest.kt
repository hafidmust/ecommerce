package com.hafidmust.ecommerce.room

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4

import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

//@RunWith(AndroidJUnit4::class)
//class CartDaoTest {
//    private lateinit var cartDao: CartDao
//    private lateinit var db: EcommerceDatabase
//    private val dummyData = CartItemEntity(
//        productId = "",
//        productName = "",
//        productPrice = 0,
//        description = "",
//        image = "",
//        store = "",
//        productRating = 0.0,
//        stock = 0,
//        brand = "",
//        sale = 0,
//        totalRating = 0,
//        totalReview = 0,
//        totalSatisfaction = 0,
//        productVariant = "",
//    )
//
//    @Before
//    fun createDb() {
//        val context = ApplicationProvider.getApplicationContext<Context>()
//        db = Room.inMemoryDatabaseBuilder(context, EcommerceDatabase::class.java).build()
//        cartDao = db.cartDao()
//    }
//
//    @After
//    @Throws(IOException::class)
//    fun closeDb() {
//        db.close()
//    }
//
//    @Test
//    @Throws(Exception::class)
//    fun insertCartTest() {
//        runTest {
//            val data = dummyData
//            cartDao.insert(data)
//            val allCart = cartDao.getAllCart().first()
//            assertEquals(allCart, allCart)
//        }
//    }
//
//    @Test
//    @Throws(Exception::class)
//    fun deleteCartTest() {
//        runTest {
//            cartDao.delete(dummyData)
//            assertEquals(0, cartDao.getAllCart().first().size)
//        }
//    }
//
//    @Test
//    @Throws(Exception::class)
//    fun updateCartTest(){
//        runTest {
//            val data = dummyData
//            cartDao.update(data)
//            val allCart = cartDao.getAllCart().first()
//            assertEquals(allCart, allCart)
//        }
//    }
//
//
//}