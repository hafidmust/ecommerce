package com.hafidmust.ecommerce.datastore

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.test.core.app.ApplicationProvider
import com.hafidmust.ecommerce.core.source.SessionManager
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class SessionManagerTest {

    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore("datastore_test")
    private lateinit var sessionManager: SessionManager
    private val EMPTY_STRING = ""

    @Before
    fun createDatastore() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        sessionManager = SessionManager(context.dataStore)
    }

    @Test
    fun setThemeTest() {
        runTest {
            sessionManager.setTheme(true)
            val getTheme = sessionManager.getTheme().first()
            assertTrue(getTheme)
        }
    }

    @Test
    fun getThemeTest() {
        runTest {
            val theme = sessionManager.getTheme().first()
            assertTrue(!theme)
        }
    }

    @Test
    fun saveRefreshTokenTest() = runTest {
        val dummyRefresh = "dummy"
        sessionManager.setRefreshToken("dummy")
        val actual = sessionManager.getRefreshToken().first()
        assertEquals(dummyRefresh, actual)
    }

    @Test
    fun setFirstInstallTest() = runTest {
        sessionManager.setFirstInstall(true)
        val actual = sessionManager.getFirstInstall().first()
        assertTrue(actual)
    }

    @Test
    fun setProfileTest() = runTest {
        sessionManager.setProfile("1.png", "hafid")
        val actual = sessionManager.getNameProfile().first()
        assertTrue(actual.isNotEmpty())
    }

    @Test
    fun deleteTokenTest() = runTest {
        sessionManager.setAccessToken("asd")
        sessionManager.setRefreshToken("jkl")
        sessionManager.deleteToken()
        val actual = sessionManager.getAccessToken().first()
        assertEquals("", actual)
    }

    @Test
    fun deleteAllTest() = runTest {
        sessionManager.setFirstInstall(false)
        sessionManager.setAccessToken("asd")
        sessionManager.setRefreshToken("jkl")
        sessionManager.setProfile("1.png", "hafid")
        sessionManager.deleteAll()
        val actual = sessionManager.getFirstInstall().first()
        assertTrue(actual)
    }

    @Test
    fun deletePreferenceTest() = runTest {
        sessionManager.setAccessToken("asd")
        sessionManager.setRefreshToken("jkl")
        sessionManager.setProfile("1.png", "hafid")
        sessionManager.deletePreference()

        val actual = sessionManager.getUriProfile().first()
        assertEquals(EMPTY_STRING, actual)
    }

    @Test
    fun getAccessTokenTest() = runTest {
        val actual = sessionManager.getAccessToken().first()
        assertEquals(EMPTY_STRING, actual)
    }

    @Test
    fun getNameProfileTest() = runTest {
        val actual = sessionManager.getNameProfile().first()
        assertEquals(EMPTY_STRING, actual)
    }

    @Test
    fun getUriProfileTest() = runTest {
        val actual = sessionManager.getUriProfile().first()
        assertEquals(EMPTY_STRING, actual)
    }

    @Test
    fun getRefreshTokenTest() = runTest {
        val actualValue = sessionManager.getRefreshToken().first()
        assertTrue(actualValue.isEmpty())
    }

    @Test
    fun getFirstInstallTest() = runTest {
        val actualValue = sessionManager.getFirstInstall().first()
        assertTrue(actualValue)
    }
}
