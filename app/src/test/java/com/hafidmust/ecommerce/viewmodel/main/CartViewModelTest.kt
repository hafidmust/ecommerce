package com.hafidmust.ecommerce.viewmodel.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.hafidmust.ecommerce.data.repository.CartRepository
import com.hafidmust.ecommerce.core.source.local.entity.CartItemEntity
import com.hafidmust.ecommerce.ui.main.cart.CartViewModel
import com.hafidmust.ecommerce.utils.DispatcherMainRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class CartViewModelTest {

    private lateinit var cartViewModel: CartViewModel

    @Mock
    private lateinit var cartRepository: CartRepository

    @get:Rule
    var rule = InstantTaskExecutorRule()

    @get:Rule
    var mainDispatcherRule = DispatcherMainRule()

    private val DUMMY_DATA = CartItemEntity(
        productId = "12345",
        productName = "Product A",
        productPrice = 100,
        image = "product_a.jpg",
        brand = "Brand X",
        description = "Lorem ipsum dolor sit amet...",
        store = "Store 1",
        sale = 20,
        stock = 50,
        totalRating = 4,
        totalReview = 10,
        totalSatisfaction = 90,
        productRating = 4.5,
        productVariant = "Variant A",
        quantity = 2,
        isChecked = true
    )

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        cartViewModel = CartViewModel(cartRepository)
        whenever(cartRepository.getCart()).thenReturn(flowOf(listOf(DUMMY_DATA)))
    }

    @Test
    fun deleteItemCartTest() = runTest {
        val expectedValue = Unit
        whenever(
            cartRepository.deleteItemCart(
                DUMMY_DATA
            )
        ).thenReturn(expectedValue)

        val actualValue = cartViewModel.deleteItemCart(
            DUMMY_DATA
        )
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun deleteAllTest() = runTest {
        val expectedValue = Unit
        whenever(
            cartRepository.deleteAll(
                listOf(DUMMY_DATA)
            )
        ).thenReturn(expectedValue)

        val actualValue = cartViewModel.deleteAll(listOf(DUMMY_DATA))
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun updateQuantityTest() = runTest {
        val expectedValue = Unit
        whenever(
            cartRepository.updateQuantity(
                DUMMY_DATA.productId,
                1
            )
        ).thenReturn(expectedValue)

        val actualValue = cartViewModel.updateQuantity(
            DUMMY_DATA,
            1
        )
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun updateCheckStateTest() = runTest {
        val expectedValue = Unit
        whenever(
            cartRepository.updateCheckCart(
                true,
                DUMMY_DATA.productId
            )
        ).thenReturn(expectedValue)

        val actualValue = cartViewModel.updateCheckState(
            true,
            DUMMY_DATA.productId
        )
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun updateTest() = runTest {
        val expectedValue = Unit
        whenever(
            cartRepository.update(
                listOf(DUMMY_DATA)
            )
        ).thenReturn(expectedValue)

        val actualValue = cartViewModel.update(
            listOf(DUMMY_DATA)
        )
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun carts() = runTest {
        val actualValue = cartViewModel.carts.first()
        assertEquals(listOf(DUMMY_DATA), actualValue)
    }

    @Test
    fun calculateTotalTest() = runTest {
        val actualValue = cartViewModel.calculateTotals(
            listOf(DUMMY_DATA, DUMMY_DATA)
        )
        assertEquals(400, actualValue)
    }
}
