package com.hafidmust.ecommerce.viewmodel.prelogin

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.liveData
import com.hafidmust.ecommerce.data.repository.AuthRepository
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.core.source.network.request.UserRequest
import com.hafidmust.ecommerce.core.source.network.response.Data
import com.hafidmust.ecommerce.core.source.network.response.RegisterResponse
import com.hafidmust.ecommerce.ui.prelogin.register.RegisterViewModel
import com.hafidmust.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class RegisterViewModelTest {

    private lateinit var registerViewModel: RegisterViewModel

    @Mock
    private lateinit var authRepository: AuthRepository

    @get:Rule
    var rule = InstantTaskExecutorRule()

    @get:Rule
    var mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        registerViewModel = RegisterViewModel(authRepository)
    }

    @Test
    fun registerSuccessTest() = runBlocking {
        val expectedValue = RegisterResponse(
            code = 200,
            message = "Register Success",
            data = Data(
                accessToken = "",
                expiresAt = 0,
                refreshToken = ""
            )
        )

        whenever(
            authRepository.register(
                userRequest = UserRequest(
                    email = "",
                    password = "",
                    firebaseToken = ""
                )
            )
        ).thenReturn(liveData { Result.Success(expectedValue) })

        val actualValue = registerViewModel.register(
            email = "",
            password = ""
        )

        actualValue.observeForever {
            if (it is Result.Success) {
                assertEquals(expectedValue, it.data)
            }
        }
    }

    @Test
    fun registerErrorTest() = runTest {
        val expectedValue = RuntimeException("Register Failed")
        whenever(
            authRepository.register(
                userRequest = UserRequest(
                    email = "",
                    password = "",
                    firebaseToken = ""
                )
            )
        ).thenReturn(liveData { throw expectedValue })

        try {
            val actualValue = registerViewModel.register(
                email = "",
                password = ""
            )
        } catch (e: Exception) {
            assertEquals(expectedValue.message, e.message)
//            actualValue.observeForever {
//                if (it is Result.Error){
//                    assertEquals(expectedValue.message, it.msg)
//                }
//            }
        }
    }
}
