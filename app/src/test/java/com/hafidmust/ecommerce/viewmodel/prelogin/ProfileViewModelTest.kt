package com.hafidmust.ecommerce.viewmodel.prelogin

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.liveData
import com.hafidmust.ecommerce.data.repository.AuthRepository
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.core.source.network.response.DataProfile
import com.hafidmust.ecommerce.core.source.network.response.ProfileResponse
import com.hafidmust.ecommerce.ui.prelogin.profile.ProfileViewModel
import com.hafidmust.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import okhttp3.MultipartBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(JUnit4::class)
class ProfileViewModelTest {

    private lateinit var profileViewModel: ProfileViewModel

    @Mock
    private lateinit var authRepository: AuthRepository

    private val DUMMY_USERNAME = MultipartBody.Part.createFormData("", "")

    @get:Rule
    var rule = InstantTaskExecutorRule()

    @get:Rule
    var mainDispatcherRule = MainDispatcherRule(UnconfinedTestDispatcher())

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        profileViewModel = ProfileViewModel(authRepository)
        whenever(authRepository.getAccessToken()).thenReturn(flowOf(""))
    }

    @Test
    fun uploadProfileSuccessTest() = runTest {
        val expectedValue = ProfileResponse(
            code = 200,
            message = "Profile Updated",
            data = DataProfile(
                userImage = "",
                userName = "",
            )
        )
        whenever(
            authRepository.profile(
                token = "",
                image = null,
                username = DUMMY_USERNAME
            )
        ).thenReturn(liveData { Result.Success(expectedValue) })

        val actualValue = profileViewModel.uploadProfile(null, DUMMY_USERNAME)
        actualValue.observeForever {
            if (it is Result.Success) {
                assertEquals(expectedValue, it.data)
            }
        }
    }

    @Test
    fun uploadProfileErrorTest() = runTest {
        val expectedValue = RuntimeException("upload profile error")
        whenever(
            authRepository.profile(
                token = "",
                image = null,
                username = DUMMY_USERNAME
            )
        ).thenReturn(liveData { throw expectedValue })

        try {
            val actualValue = profileViewModel.uploadProfile(null, DUMMY_USERNAME)
        } catch (e: Exception) {
            assertEquals(expectedValue.message, e.message)
        }
    }
}
