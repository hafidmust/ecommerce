package com.hafidmust.ecommerce.viewmodel.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.liveData
import com.hafidmust.ecommerce.data.repository.CartRepository
import com.hafidmust.ecommerce.data.repository.TransactionRepository
import com.hafidmust.ecommerce.core.source.local.entity.CheckoutEntity
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.core.source.network.request.TransactionRequest
import com.hafidmust.ecommerce.core.source.network.response.FulfillmentResponse
import com.hafidmust.ecommerce.ui.main.checkout.CheckoutViewModel
import com.hafidmust.ecommerce.utils.DispatcherMainRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class CheckoutViewModelTest {

    private lateinit var checkoutViewModel: CheckoutViewModel

    @Mock
    private lateinit var transactionRepository: TransactionRepository

    @Mock
    private lateinit var cartRepository: CartRepository

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var dispatcherMainRule = DispatcherMainRule()

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        checkoutViewModel = CheckoutViewModel(transactionRepository, cartRepository)
    }

    @Test
    fun fulfillmentSuccessTest() = runTest {
        val expectedValue = FulfillmentResponse.Data(
            "1",
            true,
            "2023-09-28",
            "10:00",
            "Credit Card",
            100
        )
        whenever(
            transactionRepository.fulfillment(
                TransactionRequest(
                    payment = "",
                    items = listOf()
                )
            )
        ).thenReturn(liveData { emit(Result.Success(expectedValue)) })

        val actualValue = checkoutViewModel.fulfillment(
            TransactionRequest(
                payment = "",
                items = listOf()
            )
        )
        actualValue.observeForever {
            if (it is Result.Success) {
                assertEquals(expectedValue, it.data)
            }
        }
    }

    @Test
    fun fulfillmentErrorTest() = runTest {
        val expectedValue = RuntimeException("Fulfillment Failed")
        whenever(
            transactionRepository.fulfillment(
                TransactionRequest(
                    payment = "",
                    items = listOf()
                )
            )
        ).thenReturn(liveData { emit(Result.Error(expectedValue.toString())) })

        val actualValue = checkoutViewModel.fulfillment(
            TransactionRequest(
                payment = "",
                items = listOf()
            )
        )
        actualValue.observeForever {
            if (it is Result.Error) {
                assertEquals(expectedValue.toString(), it.msg)
            }
        }
    }

    @Test
    fun setCheckoutTest() = runTest {
        val expectedValue = Unit
        val actualValue = checkoutViewModel.setCheckout(
            listOf()
        )
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun updateQuantityTest() = runTest {
        val expectedValue = Unit

        val actualValue = checkoutViewModel.updateQuantity(
            newQuantity = 1,
            item = CheckoutEntity(
                id = 1,
                productId = "PROD123",
                productName = "Product Name",
                productPrice = 100,
                image = "https://example.com/product_image.jpg",
                brand = "Brand Name",
                description = "Product Description",
                store = "Store Name",
                sale = 10,
                stock = 50,
                totalRating = 4,
                totalReview = 20,
                totalSatisfaction = 85,
                productRating = 4.5,
                productVariant = "Color: Red, Size: M",
                quantity = 2,
                isChecked = true
            )

        )
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun totalProductPrice() = runTest {
        val dummy = CheckoutEntity(
            id = 1,
            productId = "PROD123",
            productName = "Product Name",
            productPrice = 100,
            image = "https://example.com/product_image.jpg",
            brand = "Brand Name",
            description = "Product Description",
            store = "Store Name",
            sale = 10,
            stock = 50,
            totalRating = 4,
            totalReview = 20,
            totalSatisfaction = 85,
            productRating = 4.5,
            productVariant = "Color: Red, Size: M",
            quantity = 2,
            isChecked = true
        )
        checkoutViewModel.setCheckout(
            listOf(
                dummy,
                dummy
            )
        )
        val actualValue = checkoutViewModel.totalProductPrice
        assertEquals(400, actualValue)
    }
}
