package com.hafidmust.ecommerce.viewmodel.prelogin

import com.hafidmust.ecommerce.data.repository.AuthRepository
import com.hafidmust.ecommerce.ui.prelogin.onboarding.OnboardingViewModel
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class OnboardingViewModelTest {

    private lateinit var onboardingViewModel: OnboardingViewModel

    @Mock
    private lateinit var authRepository: AuthRepository

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        onboardingViewModel = OnboardingViewModel(authRepository)
    }

    @Test
    fun getFirstInstallTest() = runTest {
        val expectedValue = flowOf(false)
        whenever(authRepository.getFirstInstall()).thenReturn(expectedValue)

        val actualValue = onboardingViewModel.getFirstInstall()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun setOnboardingState() = runTest {
        val expectedValue = Unit
        whenever(authRepository.setFirstInstall(true)).thenReturn(expectedValue)

        val actualValue = onboardingViewModel.setOnboardingState(true)
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun getAccessTokenTest() = runTest {
        val expectedValue = flowOf("")
        whenever(authRepository.getAccessToken()).thenReturn(expectedValue)

        val actualValue = onboardingViewModel.getAccessToken()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun getRefreshTokenTest() = runTest {
        val expectedValue = flowOf("")
        whenever(authRepository.getRefreshToken()).thenReturn(expectedValue)

        val actualValue = onboardingViewModel.getRefreshToken()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun getThemeTest() = runTest {
        val expectedValue = flowOf(true)
        whenever(authRepository.getTheme()).thenReturn(expectedValue)

        val actualValue = onboardingViewModel.getTheme()
        assertEquals(expectedValue, actualValue)
    }
}
