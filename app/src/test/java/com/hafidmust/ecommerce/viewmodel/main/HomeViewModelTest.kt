package com.hafidmust.ecommerce.viewmodel.main

import com.hafidmust.ecommerce.data.repository.AuthRepository
import com.hafidmust.ecommerce.data.repository.CartRepository
import com.hafidmust.ecommerce.ui.main.home.HomeViewModel
import com.hafidmust.ecommerce.utils.DispatcherMainRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class HomeViewModelTest {
    private lateinit var homeViewModel: HomeViewModel

    @Mock
    private lateinit var authRepository: AuthRepository

    @Mock
    private lateinit var cartRepository: CartRepository

    @get:Rule
    val dispatcherMainRule = DispatcherMainRule()

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        homeViewModel = HomeViewModel(
            authRepository,
            cartRepository
        )
    }

    @Test
    fun getQuantityTest() = runTest {
        val expectedValue = 5
        whenever(cartRepository.getQuantity()).thenReturn(flowOf(expectedValue))

        val actualValue = homeViewModel.getQuantity().first()

        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun setThemeTest() = runTest {
        whenever(authRepository.setTheme(true)).thenReturn(Unit)

        val actualValue = homeViewModel.setTheme(true)

        assertEquals(Unit, actualValue)
    }

    @Test
    fun getThemeTest() = runTest {
        val expectedValue = true
        whenever(authRepository.getTheme()).thenReturn(flowOf(expectedValue))

        val actualValue = homeViewModel.getTheme().first()
        assertEquals(expectedValue, actualValue)
    }
}
