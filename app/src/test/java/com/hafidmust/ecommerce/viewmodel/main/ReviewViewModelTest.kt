package com.hafidmust.ecommerce.viewmodel.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.liveData
import com.hafidmust.ecommerce.data.repository.MainRepository
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.core.source.network.response.ProductReviewResponse
import com.hafidmust.ecommerce.ui.main.review.ReviewViewModel
import com.hafidmust.ecommerce.utils.DispatcherMainRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class ReviewViewModelTest {
    private lateinit var reviewViewModel: ReviewViewModel

    @Mock
    private lateinit var mainRepository: MainRepository

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var dispatcherMainRule = DispatcherMainRule()

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        reviewViewModel = ReviewViewModel(
            mainRepository
        )
    }

    @Test
    fun reviewSuccessTest() {
        val expectedData = listOf(
            ProductReviewResponse.DataReview(
                "",
                "",
                5,
                ""
            )
        )

        whenever(mainRepository.reviewProduct("")).thenReturn(
            liveData { emit(Result.Success(expectedData)) }
        )

        val actualData = reviewViewModel.getReviewProduct("")
        actualData.observeForever {
            if (it is Result.Success) {
                assertEquals(expectedData, it.data)
            }
        }
    }

    @Test
    fun reviewErrorTest() = runTest {
        val expectedValue = RuntimeException("send review error")
        whenever(mainRepository.reviewProduct("")).thenReturn(
            liveData { emit(Result.Error(expectedValue.toString())) }
        )

        val actualValue = reviewViewModel.getReviewProduct("")
        actualValue.observeForever {
            if (it is Result.Error) {
                assertEquals(expectedValue.toString(), it.msg)
            }
        }
    }
}
