package com.hafidmust.ecommerce.viewmodel.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.liveData
import com.hafidmust.ecommerce.data.repository.TransactionRepository
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.core.source.network.response.TransactionResponse
import com.hafidmust.ecommerce.ui.main.transaction.TransactionViewModel
import com.hafidmust.ecommerce.utils.DispatcherMainRule
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class TransactionViewModelTest {
    private lateinit var transactionViewModel: TransactionViewModel

    @Mock
    private lateinit var repository: TransactionRepository

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var dispatcherMainRule = DispatcherMainRule()

    private val dummyData = TransactionResponse.Data(
        "INV123",
        true,
        "2023-09-27",
        "14:30",
        "Credit Card",
        500,
        listOf(
            TransactionResponse.Data.Item("PROD001", "Product 1", 100),
            TransactionResponse.Data.Item("PROD002", "Product 2", 200),
            TransactionResponse.Data.Item("PROD003", "Product 3", 300)
        ),
        4,
        "Great experience!",
        "https://example.com/invoice_image.jpg",
        "John Doe"
    )

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        transactionViewModel = TransactionViewModel(
            repository
        )
    }

    @Test
    fun transactionSuccessTest() {
        val expectedData = listOf(
            dummyData
        )

        whenever(repository.transaction()).thenReturn(
            liveData { emit(Result.Success(expectedData)) }
        )

        val actualData = transactionViewModel.transaction()
        actualData.observeForever {
            if (it is Result.Success) {
                TestCase.assertEquals(expectedData, it.data)
            }
        }
    }

    @Test
    fun transactionErrorTest() = runTest {
        val expectedValue = RuntimeException("send review error")
        whenever(repository.transaction()).thenReturn(
            liveData { emit(Result.Error(expectedValue.toString())) }
        )

        val actualValue = transactionViewModel.transaction()
        actualValue.observeForever {
            if (it is Result.Error) {
                TestCase.assertEquals(expectedValue.toString(), it.msg)
            }
        }
    }
}
