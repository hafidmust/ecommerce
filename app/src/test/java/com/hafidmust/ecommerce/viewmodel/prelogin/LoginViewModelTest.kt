package com.hafidmust.ecommerce.viewmodel.prelogin

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.liveData
import com.hafidmust.ecommerce.data.repository.AuthRepository
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.core.source.network.response.DataLogin
import com.hafidmust.ecommerce.core.source.network.response.LoginResponse
import com.hafidmust.ecommerce.ui.prelogin.login.LoginViewModel
import com.hafidmust.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class LoginViewModelTest {

    private lateinit var loginViewModel: LoginViewModel

    @Mock
    private lateinit var authRepository: AuthRepository

    @get:Rule
    var rule = InstantTaskExecutorRule()

    @get:Rule
    var mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        loginViewModel = LoginViewModel(authRepository)
    }

    @Test
    fun loginSuccessTest() {
        runTest {
            val expectedValue = LoginResponse(
                code = 200,
                message = "Login Success",
                data = DataLogin(
                    userImage = "",
                    userName = "",
                    accessToken = "",
                    refreshToken = "",
                    expiresAt = 0
                )
            )

            whenever(authRepository.login("", "")).thenReturn(
                liveData {
                    emit(Result.Success(expectedValue))
                }
            )

            val actualValue = loginViewModel.login("", "")
            actualValue.observeForever {
                if (it is Result.Success) {
                    assertEquals(expectedValue, it.data)
                }
            }
        }
    }

    @Test
    fun loginErrorTest() {
        val expectedValue = RuntimeException("Login Failed")
        whenever(authRepository.login("", "")).thenReturn(
            liveData {
                emit(Result.Error(expectedValue.toString()))
            }
        )

        val actualValue = loginViewModel.login("", "")
        actualValue.observeForever {
            if (it is Result.Error) {
                assertEquals(expectedValue.toString(), it.msg)
            }
        }
    }
}
