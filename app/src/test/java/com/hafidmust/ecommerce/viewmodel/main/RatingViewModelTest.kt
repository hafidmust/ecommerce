package com.hafidmust.ecommerce.viewmodel.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.liveData
import com.hafidmust.ecommerce.data.repository.TransactionRepository
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.core.source.network.request.RatingRequest
import com.hafidmust.ecommerce.ui.main.rating.RatingViewModel
import com.hafidmust.ecommerce.utils.DispatcherMainRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class RatingViewModelTest {

    private lateinit var ratingViewModel: RatingViewModel

    @Mock
    private lateinit var transactionRepository: TransactionRepository

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var dispatcherMainRule = DispatcherMainRule()

    private val dummyRequest = RatingRequest(
        invoiceId = "",
        rating = 5,
        review = ""
    )

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        ratingViewModel = RatingViewModel(transactionRepository)
    }

    @Test
    fun sendRatingSuccessTest() = runTest {
        val expectedValue = 200
        whenever(
            transactionRepository.rating(
                dummyRequest
            )
        ).thenReturn(
            liveData { emit(Result.Success(expectedValue)) }
        )

        val actualValue = ratingViewModel.postRating(
            dummyRequest
        )
        actualValue.observeForever {
            if (it is Result.Success) {
                assertEquals(expectedValue, it.data)
            }
        }
    }

    @Test
    fun sendRatingErrorTest() = runTest {
        val expectedValue = RuntimeException("send rating error")
        whenever(
            transactionRepository.rating(
                dummyRequest
            )
        ).thenReturn(
            liveData { emit(Result.Error(expectedValue.toString())) }
        )

        val actualValue = ratingViewModel.postRating(
            dummyRequest
        )
        actualValue.observeForever {
            if (it is Result.Error) {
                assertEquals(expectedValue.toString(), it.msg)
            }
        }
    }
}
