package com.hafidmust.ecommerce.viewmodel.main

import com.hafidmust.ecommerce.data.repository.MainRepository
import com.hafidmust.ecommerce.core.source.local.entity.NotificationEntity
import com.hafidmust.ecommerce.ui.main.notification.NotificationViewModel
import com.hafidmust.ecommerce.utils.DispatcherMainRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(JUnit4::class)
class NotificationViewModelTest {

    @get:Rule
    var standardTestRule = DispatcherMainRule()

    private lateinit var notificationViewModel: NotificationViewModel

    @Mock
    private lateinit var mainRepository: MainRepository

    private val DUMMY_DATA = NotificationEntity(
        title = "New Message",
        body = "You have received a new message.",
        image = "message_icon.png",
        type = "Message",
        date = "2023-09-27",
        time = "10:30 AM",
        isRead = false
    )

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        notificationViewModel = NotificationViewModel(mainRepository)
        whenever(mainRepository.getAllNotification()).thenReturn(flowOf(listOf(DUMMY_DATA)))
        whenever(mainRepository.getUnreadNotification()).thenReturn(flowOf(1))
    }

    @Test
    fun insertNotificationTest() = runTest {
        val expectedValue = Unit
        whenever(
            mainRepository.insertNotification(
                DUMMY_DATA
            )
        ).thenReturn(expectedValue)
    }

    @Test
    fun updateNotificationTest() = runTest {
        val expectedValue = Unit
        whenever(
            mainRepository.updateNotification(
                false,
                1
            )
        ).thenReturn(expectedValue)

        val actualValue = notificationViewModel.updateNotification(
            false,
            1
        )
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun getAllNotificationTest() = runTest {
        val actual = notificationViewModel.getAllNotification.first()
        assertEquals(listOf(DUMMY_DATA), actual)
    }

    @Test
    fun getUnreadNotificationTest() = runTest {
        val actual = notificationViewModel.getUnreadNotification.first()
        assertEquals(1, actual)
    }
}
