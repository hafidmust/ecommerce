package com.hafidmust.ecommerce.viewmodel.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.hafidmust.ecommerce.data.repository.AuthRepository
import com.hafidmust.ecommerce.data.repository.CartRepository
import com.hafidmust.ecommerce.data.repository.WishlistRepository
import com.hafidmust.ecommerce.ui.main.MainViewModel
import com.hafidmust.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class MainViewModelTest {
    private lateinit var mainViewModel: MainViewModel

    @Mock
    private lateinit var authRepository: AuthRepository

    @Mock
    private lateinit var cartRepository: CartRepository

    @Mock
    private lateinit var wishlistRepository: WishlistRepository

    @get:Rule
    var rule = InstantTaskExecutorRule()

    @get:Rule
    var mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        mainViewModel = MainViewModel(
            authRepository = authRepository,
            cartRepository = cartRepository,
            wishlistRepository = wishlistRepository
        )
    }

    @Test
    fun getTokenTest() = runTest {
        val expectedValue = ""
        whenever(authRepository.getRefreshToken()).thenReturn(flowOf(expectedValue))

        val actualValue = mainViewModel.getToken().first()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun getNameTest() = runTest {
        val expectedValue = ""
        whenever(authRepository.getNameProfile()).thenReturn(flowOf(expectedValue))

        val actualValue = mainViewModel.getName().first()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun getUriTest() = runTest {
        val expectedValue = ""
        whenever(authRepository.getUriProfile()).thenReturn(flowOf(expectedValue))

        val actualValue = mainViewModel.getUri().first()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun getQuantityTest() = runTest {
        val expectedValue = 0
        whenever(cartRepository.getQuantity()).thenReturn(flowOf(expectedValue))

        val actualValue = mainViewModel.getQuantity().first()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun getQuantityWishlistTest() = runTest {
        val expectedValue = 0
        whenever(wishlistRepository.getQuantity()).thenReturn(flowOf(expectedValue))

        val actualValue = mainViewModel.getQuantityWishlist().first()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun deletePrefTest() = runTest {
        val expectedValue = Unit
        whenever(authRepository.deletePref()).thenReturn(expectedValue)

        val actualValue = mainViewModel.deletePref()
        assertEquals(expectedValue, actualValue)
    }
}
