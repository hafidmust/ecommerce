package com.hafidmust.ecommerce.viewmodel.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.hafidmust.ecommerce.data.repository.CartRepository
import com.hafidmust.ecommerce.data.repository.MainRepository
import com.hafidmust.ecommerce.data.repository.WishlistRepository
import com.hafidmust.ecommerce.core.source.local.entity.CartItemEntity
import com.hafidmust.ecommerce.core.source.local.entity.DataWishListEntity
import com.hafidmust.ecommerce.core.source.network.response.DetailProductResponse
import com.hafidmust.ecommerce.ui.main.detail.DetailViewModel
import com.hafidmust.ecommerce.utils.DispatcherMainRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class DetailViewModelTest {
    private lateinit var detailViewModel: DetailViewModel

    @Mock
    private lateinit var mainRepository: MainRepository

    @Mock
    private lateinit var cartRepository: CartRepository

    @Mock
    private lateinit var wishlistRepository: WishlistRepository

    @get:Rule
    var dispatcherMainRule = DispatcherMainRule()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val DUMMY_DATA = CartItemEntity(
        productId = "PROD123",
        productName = "Product Name",
        productPrice = 100,
        image = "https://example.com/product_image.jpg",
        brand = "Brand Name",
        description = "Product Description",
        store = "Store Name",
        sale = 10,
        stock = 50,
        totalRating = 4,
        totalReview = 20,
        totalSatisfaction = 85,
        productRating = 4.5,
        productVariant = "Variant A",
        quantity = 2,
        isChecked = false
    )

    private val DUMMY_NEW_DATA = CartItemEntity(
        productId = "PROD123",
        productName = "Product Name",
        productPrice = 100,
        image = "https://example.com/product_image.jpg",
        brand = "Brand Name",
        description = "Product Description",
        store = "Store Name",
        sale = 10,
        stock = 50,
        totalRating = 4,
        totalReview = 20,
        totalSatisfaction = 85,
        productRating = 4.5,
        productVariant = "Variant A",
        quantity = 3,
        isChecked = false
    )

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        detailViewModel = DetailViewModel(mainRepository, cartRepository, wishlistRepository)
        whenever(cartRepository.getItemById("")).thenReturn(flowOf(DUMMY_DATA))
        runBlocking {
            whenever(cartRepository.insertCart(DUMMY_DATA)).thenReturn(Unit)
        }
    }

//    @Test
//    fun detailProductSuccessTest() = runTest {
//        val expectedValue = DetailProductResponse.Data(
//            productId = "PROD123",
//            productName = "Product Name",
//            productPrice = 100,
//            image = emptyList(),
//            brand = "Brand Name",
//            description = "Product Description",
//            store = "Store Name",
//            sale = 10,
//            stock = 50,
//            totalRating = 4,
//            totalReview = 20,
//            totalSatisfaction = 85,
//            productRating = 4.5,
//            productVariant = emptyList()
//        )
//        whenever(mainRepository.detailProduct("1")).thenReturn(
//            flowOf(
//                com.hafidmust.ecommerce.data.source.network.Result.Success(
//                    expectedValue
//                )
//            )
//        )
//
//        val actualValue = detailViewModel.detailProduct("1").first()
//        if (actualValue is com.hafidmust.ecommerce.data.source.network.Result.Success) {
//            assertEquals(expectedValue, actualValue.data)
//        }
//    }

//    @Test
//    fun detailProductErrorTest() = runTest {
//        val expectedValue = RuntimeException("Detail Product Failed")
//        whenever(mainRepository.detailProduct("1")).thenReturn(
//            flowOf(
//                com.hafidmust.ecommerce.data.source.network.Result.Error(
//                    expectedValue.toString()
//                )
//            )
//        )
//
//        val actualValue = detailViewModel.detailProduct("1").first()
//        if (actualValue is com.hafidmust.ecommerce.data.source.network.Result.Error) {
//            assertEquals(expectedValue.toString(), actualValue.msg)
//        }
//    }
    // todo: wishlist

    @Test
    fun wishlistTest() = runTest {
        val dummyValue = listOf<DataWishListEntity>(
            DataWishListEntity(
                image = "https://example.com/image1.jpg",
                productId = "PROD001",
                description = "Product Description 1",
                totalRating = 4,
                store = "Store Name 1",
                productName = "Product Name 1",
                totalSatisfaction = 80,
                sale = 20,
                productVariant = "Color: Red, Size: M",
                stock = 100,
                productRating = 4.5,
                brand = "Brand 1",
                productPrice = 150,
                totalReview = 50
            ),
            DataWishListEntity(
                image = "https://example.com/image2.jpg",
                productId = "PROD002",
                description = "Product Description 2",
                totalRating = 5,
                store = "Store Name 2",
                productName = "Product Name 2",
                totalSatisfaction = 90,
                sale = 15,
                productVariant = "Color: Blue, Size: L",
                stock = 75,
                productRating = 4.8,
                brand = "Brand 2",
                productPrice = 200,
                totalReview = 40
            )
        )
        whenever(wishlistRepository.getWishlist()).thenReturn(flowOf(dummyValue))

        val actualValue = detailViewModel.wishlist
        assertEquals(null, actualValue)
    }
//    @Test
//    fun insertCart() = runTest {
//        val expectedValue = Unit
//        whenever(cartRepository.insertCart(
//            CartItemEntity(
//                productId = "PROD123",
//                productName = "Product Name",
//                productPrice = 100,
//                image = "https://example.com/product_image.jpg",
//                brand = "Brand Name",
//                description = "Product Description",
//                store = "Store Name",
//                sale = 10,
//                stock = 50,
//                totalRating = 4,
//                totalReview = 20,
//                totalSatisfaction = 85,
//                productRating = 4.5,
//                productVariant = "Variant A",
//                quantity = 1,
//                isChecked = false
//            )
//        )).thenReturn(expectedValue)
//
//        val actualValue = detailViewModel.insertCart(
//            product = CartItemEntity(
//                productId = "PROD123",
//                productName = "Product Name",
//                productPrice = 100,
//                image = "https://example.com/product_image.jpg",
//                brand = "Brand Name",
//                description = "Product Description",
//                store = "Store Name",
//                sale = 10,
//                stock = 50,
//                totalRating = 4,
//                totalReview = 20,
//                totalSatisfaction = 85,
//                productRating = 4.5,
//                productVariant = "Variant A",
//                quantity = 1,
//                isChecked = false
//            )
//        )
//
//        assertEquals(expectedValue, actualValue)
//    }

    @Test
    fun `test insertCart when item exists`() = runTest {
        val existingProduct = DUMMY_DATA
        val newProduct = DUMMY_NEW_DATA

        Mockito.`when`(cartRepository.getItemById(existingProduct.productId))
            .thenReturn(flowOf(existingProduct))

        detailViewModel.insertCart(newProduct)

        Mockito.verify(cartRepository).updateQuantity(existingProduct.productId, 5)
        Mockito.verify(cartRepository, Mockito.never()).insertCart(newProduct)
    }

    @Test
    fun `test insertCart when item does not exist`() = runTest {
        whenever(cartRepository.insertCart(DUMMY_NEW_DATA)).thenReturn(Unit)
        val actualValue = cartRepository.insertCart(DUMMY_NEW_DATA)
        assertEquals(Unit, actualValue)
    }
}
