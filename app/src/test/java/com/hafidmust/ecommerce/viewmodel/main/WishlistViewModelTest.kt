package com.hafidmust.ecommerce.viewmodel.main

import com.hafidmust.ecommerce.data.repository.CartRepository
import com.hafidmust.ecommerce.data.repository.WishlistRepository
import com.hafidmust.ecommerce.core.source.local.entity.DataWishListEntity
import com.hafidmust.ecommerce.ui.main.wishlist.WishlistViewModel
import com.hafidmust.ecommerce.utils.DispatcherMainRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class WishlistViewModelTest {
    private lateinit var wishlistViewModel: WishlistViewModel

    @Mock
    private lateinit var wishlistRepository: WishlistRepository

    @Mock
    private lateinit var cartRepository: CartRepository

    @get:Rule
    var mainDispatcherRule = DispatcherMainRule()

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        wishlistViewModel = WishlistViewModel(
            wishlistRepository,
            cartRepository
        )
    }

    @Test
    fun getWishlistTest() = runTest {
        val expectedValue = listOf(
            DataWishListEntity(
                image = "https://example.com/image1.jpg",
                productId = "PROD001",
                description = "Product Description 1",
                totalRating = 4,
                store = "Store Name 1",
                productName = "Product Name 1",
                totalSatisfaction = 80,
                sale = 20,
                productVariant = "Color: Red, Size: M",
                stock = 100,
                productRating = 4.5,
                brand = "Brand 1",
                productPrice = 150,
                totalReview = 50
            )
        )

        whenever(wishlistRepository.getWishlist()).thenReturn(
            flowOf(expectedValue)
        )

        val actualValue = wishlistViewModel.wishlist
        assertEquals(null, actualValue)
    }
}
