package com.hafidmust.ecommerce.room

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.hafidmust.ecommerce.core.source.local.entity.CartItemEntity
import com.hafidmust.ecommerce.core.source.local.room.CartDao
import com.hafidmust.ecommerce.core.source.local.room.EcommerceDatabase
import junit.framework.TestCase
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import java.io.IOException

@RunWith(RobolectricTestRunner::class)
class CartDaoTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var cartDao: CartDao
    private lateinit var db: EcommerceDatabase
    private val dummyData = CartItemEntity(
        id = 1,
        productId = "",
        productName = "",
        productPrice = 0,
        description = "",
        image = "",
        store = "",
        productRating = 0.0,
        stock = 0,
        brand = "",
        sale = 0,
        totalRating = 0,
        totalReview = 0,
        totalSatisfaction = 0,
        productVariant = "",
    )

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, EcommerceDatabase::class.java).build()
        cartDao = db.cartDao()
    }

    @Test
    @Throws(Exception::class)
    fun insertCartTest() {
        runBlocking {
            val data = dummyData
            cartDao.insert(data)
            val allCart = cartDao.getAllCart().first()
            TestCase.assertEquals(allCart, allCart)
        }
    }

    @Test
    @Throws(Exception::class)
    fun deleteCartTest() {
        runTest {
            cartDao.delete(dummyData)
            TestCase.assertEquals(0, cartDao.getAllCart().first().size)
        }
    }

    @Test
    @Throws(Exception::class)
    fun updateCartTest() {
        runTest {
            val data = dummyData
            cartDao.insert(data)
            cartDao.update(data)
            val allCart = cartDao.getAllCart().first()
            TestCase.assertEquals(listOf(data), allCart)
        }
    }

    @Test
    fun getAllCartTest() = runTest {
        val getAllCart = cartDao.getAllCart().first()
        assertEquals(emptyList<CartItemEntity>(), getAllCart)
    }

    @Test
    fun getCartByIdTest() {
        runTest {
            val data = dummyData
            cartDao.insert(data)
            val getDataById = cartDao.getCartByProductId(data.productId).first()
            TestCase.assertEquals(data, getDataById)
        }
    }

    @Test
    fun updateQuantity() = runTest {
        val data = dummyData
        cartDao.insert(data)
        cartDao.updateQuantity(data.productId, 2)
        val getQuantity = cartDao.getQuantityByProductId(data.productId).first()
        TestCase.assertEquals(2, getQuantity)
    }

    @Test
    fun updateCheckCart() = runTest {
        val data = dummyData
        cartDao.insert(data)
        cartDao.updateCheckCart(true, data.productId)
        val getCheckCart = cartDao.getCartByProductId(data.productId).first()
        TestCase.assertEquals(true, getCheckCart.isChecked)
    }

    @Test
    fun getQuantity() = runTest {
        val getQuantity = cartDao.getQuantity().first()
        TestCase.assertEquals(0, getQuantity)
    }

    @Test
    fun getQuantityByProductId() = runTest {
        val data = dummyData
        cartDao.insert(data)
        val getQuantityByProductId = cartDao.getQuantityByProductId(data.productId).first()
        TestCase.assertEquals(1, getQuantityByProductId)
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }
}
