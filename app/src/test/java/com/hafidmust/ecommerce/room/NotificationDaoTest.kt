package com.hafidmust.ecommerce.room

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.hafidmust.ecommerce.core.source.local.entity.NotificationEntity
import com.hafidmust.ecommerce.core.source.local.room.EcommerceDatabase
import com.hafidmust.ecommerce.core.source.local.room.NotificationDao
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import java.io.IOException

@RunWith(RobolectricTestRunner::class)
class NotificationDaoTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var notificationDao: NotificationDao
    private lateinit var db: EcommerceDatabase
    private val dummyNotification = NotificationEntity(
        id = 1,
        title = "",
        body = "",
        image = "",
        isRead = false,
        date = "",
        type = "",
        time = ""
    )

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, EcommerceDatabase::class.java).build()
        notificationDao = db.notificationDao()
    }

    @Test
    fun getAllNotificationTest() {
        runBlocking {
            val data = dummyNotification
            notificationDao.insertNotification(data)
            val result = notificationDao.getAllNotification().first()
            assertEquals(listOf(data), result)
        }
    }

    @Test
    fun insertNotificationTest() {
        runBlocking {
            val data = dummyNotification
            notificationDao.insertNotification(data)
            val result = notificationDao.getAllNotification().first()
            assertTrue(result.isNotEmpty())
        }
    }

    @Test
    fun updateIsReadTest() {
        runBlocking {
            val data = dummyNotification
            notificationDao.insertNotification(data)
            notificationDao.updateIsRead(true, 1)
            val result = notificationDao.getAllNotification().first()
            assertEquals(true, result[0].isRead)
        }
    }

    @Test
    fun getUnreadNotificationTest() {
        runBlocking {
            val data = dummyNotification
            notificationDao.insertNotification(data)
            val result = notificationDao.getUnreadNotification().first()
            assertEquals(1, result)
        }
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }
}
