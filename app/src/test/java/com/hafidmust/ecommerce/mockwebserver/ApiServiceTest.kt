package com.hafidmust.ecommerce.mockwebserver

import com.hafidmust.ecommerce.core.source.network.ApiService
import com.hafidmust.ecommerce.core.source.network.request.RatingRequest
import com.hafidmust.ecommerce.core.source.network.request.TokenRequest
import com.hafidmust.ecommerce.core.source.network.request.TransactionRequest
import com.hafidmust.ecommerce.core.source.network.request.UserRequest
import com.hafidmust.ecommerce.core.source.network.response.Data
import com.hafidmust.ecommerce.core.source.network.response.DataLogin
import com.hafidmust.ecommerce.core.source.network.response.DataProfile
import com.hafidmust.ecommerce.core.source.network.response.DetailProductResponse
import com.hafidmust.ecommerce.core.source.network.response.FulfillmentResponse
import com.hafidmust.ecommerce.core.source.network.response.LoginResponse
import com.hafidmust.ecommerce.core.source.network.response.ProductReviewResponse
import com.hafidmust.ecommerce.core.source.network.response.ProfileResponse
import com.hafidmust.ecommerce.core.source.network.response.RatingResponse
import com.hafidmust.ecommerce.core.source.network.response.RegisterResponse
import com.hafidmust.ecommerce.core.source.network.response.SearchResponse
import com.hafidmust.ecommerce.core.source.network.response.TransactionResponse
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import okhttp3.MultipartBody
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@RunWith(JUnit4::class)
class ApiServiceTest {
    private lateinit var mockWebServer: MockWebServer
    private lateinit var service: ApiService

    private val dummy_api_key = "6f8856ed-9189-488f-9011-0ff4b6c08edc"
    private val DUMMY_LOGIN_REQUEST =
        UserRequest(password = "12345678", firebaseToken = "", email = "hafid@gmail.com")
    private val DUMMY_TOKEN_REQUEST = TokenRequest(token = "")
    private val DUMMY_TOKEN = "token"

    @Before
    fun setup() {
        val factory = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        mockWebServer = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(MoshiConverterFactory.create(factory))
            .build()
            .create(ApiService::class.java)
    }

    @Test
    fun `should return correct endpoint login`() {
        runBlocking {
            val expected = LoginResponse(
                code = 200,
                data = DataLogin(
                    userImage = "",
                    userName = "",
                    accessToken = "",
                    expiresAt = 6000,
                    refreshToken = ""
                ),
                message = "OK"
            )
            mockWebServer.enqueue(
                MockResponse().setBody(MockResponseJsonReader("mock_response/response_login.json").content)
            )
            val actualResponse =
                service.login(apiKey = dummy_api_key, userRequest = DUMMY_LOGIN_REQUEST)

            assertEquals(expected, actualResponse)
        }
    }

    @Test
    fun `should return correct register`() {
        runBlocking {
            val expectedResponse = RegisterResponse(
                code = 200,
                data = Data(
                    accessToken = "",
                    expiresAt = 6000,
                    refreshToken = ""
                ),
                message = "OK"
            )

            mockWebServer.enqueue(
                MockResponse().setBody(MockResponseJsonReader("mock_response/response_register.json").content)
            )
            val actualResponse =
                service.register(apiKey = dummy_api_key, userRequest = DUMMY_LOGIN_REQUEST)
            assertEquals(expectedResponse, actualResponse)
        }
    }

//    @Test
//    fun `should return correct refresh`(){
//        runBlocking {
//            val expectedResponse = RefreshResponse(
//                code = 200,
//                data = Data(
//                    accessToken = "",
//                    expiresAt = 6000,
//                    refreshToken = ""
//                ),
//                message = "OK")
//
//            mockWebServer.enqueue(MockResponse().setBody(MockResponseJsonReader("mock_response/response_refresh.json").content))
//            val actualResponse = service.refresh(apiKey = dummy_api_key, token = DUMMY_TOKEN_REQUEST)
//            assertEquals(expectedResponse,actualResponse)
//        }
//    }

    @Test
    fun `should return correct profile`() {
        runBlocking {
            val user = MultipartBody.Part.createFormData("user", "")
            val expectedResponse = ProfileResponse(
                code = 200,
                data = DataProfile(
                    userImage = "",
                    userName = ""
                ),
                message = "OK"
            )

            mockWebServer.enqueue(
                MockResponse().setBody(MockResponseJsonReader("mock_response/response_profile.json").content)
            )
            val actualResponse = service.profile(token = "", userImage = null, user = user)
            assertEquals(expectedResponse, actualResponse)
        }
    }

    @Test
    fun `search product`() {
        runBlocking {
            val expectedResponse = SearchResponse(
                code = 200,
                data = arrayListOf(),
                message = "OK"
            )

            mockWebServer.enqueue(
                MockResponse().setBody(MockResponseJsonReader("mock_response/response_search.json").content)
            )
            val actualResponse = service.search(token = DUMMY_TOKEN, query = "")
            assertEquals(expectedResponse, actualResponse)
        }
    }

    @Test
    fun `product detail test`() {
        runBlocking {
            val expectedResponse = DetailProductResponse(
                code = 200,
                message = "OK",
                data = DetailProductResponse.Data(
                    productId = "",
                    productName = "",
                    productPrice = 0,
                    description = "",
                    image = listOf(""),
                    store = "",
                    productRating = 0.0,
                    stock = 0,
                    brand = "",
                    sale = 0,
                    totalRating = 0,
                    totalReview = 0,
                    totalSatisfaction = 0,
                    productVariant = listOf(
                        DetailProductResponse.Data.ProductVariant(
                            variantName = "",
                            variantPrice = 0
                        )
                    ),
                )
            )

            mockWebServer.enqueue(
                MockResponse().setBody(MockResponseJsonReader("mock_response/response_product_detail.json").content)
            )
            val actualResponse = service.productDetail(token = DUMMY_TOKEN, productId = "1")
            assertEquals(expectedResponse, actualResponse)
        }
    }

    @Test
    fun `product review test`() {
        runBlocking {
            val expectedResponse = ProductReviewResponse(
                code = 200,
                message = "OK",
                data = arrayListOf()
            )

            mockWebServer.enqueue(
                MockResponse().setBody(MockResponseJsonReader("mock_response/response_product_review.json").content)
            )
            val actualResponse = service.productReview(token = DUMMY_TOKEN, productId = "")
            assertEquals(expectedResponse, actualResponse)
        }
    }

    @Test
    fun `fulfillment test`() {
        runBlocking {
            val expectedResponse = FulfillmentResponse(
                code = 200,
                data = FulfillmentResponse.Data(
                    invoiceId = "",
                    status = true,
                    date = "",
                    time = "",
                    payment = "",
                    total = 0,
                ),
                message = "OK"
            )

            mockWebServer.enqueue(
                MockResponse().setBody(MockResponseJsonReader("mock_response/response_fulfillment.json").content)
            )
            val actualResponse = service.fulfillment(
                token = DUMMY_TOKEN,
                request = TransactionRequest(
                    payment = "",
                    items = listOf()
                )
            )

            assertEquals(expectedResponse, actualResponse)
        }
    }

    @Test
    fun `rating test`() {
        runBlocking {
            val expectedResponse = RatingResponse(
                code = 200,
                message = ""
            )

            mockWebServer.enqueue(
                MockResponse().setBody(MockResponseJsonReader("mock_response/response_rating.json").content)
            )
            val actualResponse = service.rating(
                token = DUMMY_TOKEN,
                request = RatingRequest(
                    invoiceId = "",
                    rating = 0,
                    review = ""
                )
            )

            assertEquals(expectedResponse, actualResponse)
        }
    }

    @Test
    fun `transaction test`() {
        runBlocking {
            val expectedResponse = TransactionResponse(
                code = 200,
                data = arrayListOf(),
                message = "OK"
            )

            mockWebServer.enqueue(
                MockResponse().setBody(MockResponseJsonReader("mock_response/response_transaction.json").content)
            )
            val actualResponse = service.transaction(token = DUMMY_TOKEN)
            assertEquals(expectedResponse, actualResponse)
        }
    }

    @After
    fun shutdown() {
        mockWebServer.shutdown()
    }
}
