package com.hafidmust.ecommerce.mockwebserver

import java.io.InputStreamReader

class MockResponseJsonReader(path: String) {
    val content: String
    init {
        val reader = InputStreamReader(this.javaClass.classLoader?.getResourceAsStream(path))
        content = reader.readText()
        reader.close()
    }
}
