package com.hafidmust.ecommerce.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.firebase.analytics.FirebaseAnalytics
import com.hafidmust.ecommerce.data.repository.AuthRepository
import com.hafidmust.ecommerce.core.source.SessionManager
import com.hafidmust.ecommerce.core.source.network.ApiService
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.core.source.network.request.UserRequest
import com.hafidmust.ecommerce.core.source.network.response.DataLogin
import com.hafidmust.ecommerce.core.source.network.response.LoginResponse
import com.hafidmust.ecommerce.core.source.network.response.ProfileResponse
import com.hafidmust.ecommerce.core.source.network.response.RegisterResponse
import com.hafidmust.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import okhttp3.MultipartBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class AuthRepositoryTest {

    private lateinit var repository: AuthRepository

    @Mock
    private lateinit var apiService: ApiService

    @Mock
    private lateinit var sessionManager: SessionManager

    @Mock
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var rule = MainDispatcherRule()

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        repository = AuthRepository(
            apiService = apiService,
            sessionManager = sessionManager,
            firebaseAnalytics = mock()
        )
    }

    @Test
    fun `should set theme to false`() {
        runBlocking {
            whenever(sessionManager.setTheme(false)).thenReturn(Unit)
            val actualValue = repository.setTheme(false)

            assertEquals(Unit, actualValue)
        }
    }

    @Test
    fun `should get theme`() {
        runBlocking {
            whenever(sessionManager.getTheme()).thenReturn(flowOf(false))
            val actualValue = repository.getTheme().first()

            assertEquals(false, actualValue)
        }
    }

    @Test
    fun registerTest() {
        runBlocking {
            val dummyResponse = mock(RegisterResponse::class.java)
            whenever(
                apiService.register(
                    apiKey = "6f8856ed-9189-488f-9011-0ff4b6c08edc",
                    userRequest = UserRequest("", "", "")
                )
            ).thenReturn(dummyResponse)

            val actual = repository.register(
                userRequest = UserRequest("", "", "")
            )
            actual.observeForever {
                if (it is Result.Success) {
                    assertEquals(dummyResponse, it.data)
                }
            }
        }
    }

    @Test
    fun loginTest() = runBlocking {
        val dummyData = LoginResponse(
            code = 200,
            data = DataLogin(
                accessToken = "",
                expiresAt = 6000,
                userImage = "",
                userName = "",
                refreshToken = ""
            ),
            message = "OK"
        )
        whenever(
            apiService.login(
                apiKey = "6f8856ed-9189-488f-9011-0ff4b6c08edc",
                userRequest = UserRequest("", "", "")
            )
        ).thenReturn(dummyData)

        val actual = repository.login(email = "", password = "")
        actual.observeForever {
            if (it is Result.Success) {
                assertEquals(dummyData, it.data)
            }
        }
    }

    @Test
    fun profileTest() {
        runBlocking {
            val dummyResponse = mock(ProfileResponse::class.java)
            println("test $dummyResponse")
            whenever(
                apiService.profile(
                    token = "",
                    userImage = MultipartBody.Part.createFormData("user", ""),
                    user = MultipartBody.Part.createFormData("user", "")
                )
            ).thenReturn(dummyResponse)

            val actualResponse = repository.profile(
                token = "",
                image = MultipartBody.Part.createFormData("user", ""),
                username = MultipartBody.Part.createFormData("user", "")
            )
            actualResponse.observeForever {
                if (it is Result.Success) {
                    assertEquals(dummyResponse, it.data)
                }
            }
        }
    }

    @Test
    fun setProfileTest() = runBlocking {
        whenever(sessionManager.setProfile("1.png", "hafid")).thenReturn(Unit)
        val actual = repository.setProfile("1.png", "hafid")
        assertEquals(Unit, actual)
    }

    @Test
    fun setFirstInstall() = runBlocking {
        whenever(sessionManager.setFirstInstall(false)).thenReturn(Unit)
        val actual = repository.setFirstInstall(false)
        assertEquals(Unit, actual)
    }

    @Test
    fun getAccessTokenTest() = runBlocking {
        whenever(sessionManager.getAccessToken()).thenReturn(flowOf(""))
        val actual = repository.getAccessToken().first()
        assertEquals("", actual)
    }

    @Test
    fun logoutTest() = runBlocking {
        whenever(sessionManager.deleteToken()).thenReturn(Unit)
        val actual = repository.logout()
        assertEquals(Unit, actual)
    }

    @Test
    fun getUriProfileTest() = runBlocking {
        whenever(sessionManager.getUriProfile()).thenReturn(flowOf(""))
        val actual = repository.getUriProfile().first()
        assertEquals("", actual)
    }

    @Test
    fun getNameProfileTest() = runBlocking {
        whenever(sessionManager.getNameProfile()).thenReturn(flowOf(""))
        val actual = repository.getNameProfile().first()
        assertEquals("", actual)
    }

    @Test
    fun getFirstInstallTest() = runBlocking {
        whenever(sessionManager.getFirstInstall()).thenReturn(flowOf(false))
        val actual = repository.getFirstInstall().first()
        assertEquals(false, actual)
    }

    @Test
    fun getRefreshTokenTest() = runBlocking {
        whenever(sessionManager.getRefreshToken()).thenReturn(flowOf(""))
        val actual = repository.getRefreshToken().first()
        assertEquals("", actual)
    }

    @Test
    fun setAccessTokenTest() = runBlocking {
        whenever(sessionManager.setAccessToken("")).thenReturn(Unit)
        val actual = repository.setAccessToken("")
        assertEquals(Unit, actual)
    }

    @Test
    fun setRefreshTokenTest() = runBlocking {
        whenever(sessionManager.setRefreshToken("")).thenReturn(Unit)
        val actual = repository.setRefreshToken("")
        assertEquals(Unit, actual)
    }

    @Test
    fun deletePrefTest() = runBlocking {
        whenever(sessionManager.deletePreference()).thenReturn(Unit)
        val actual = repository.deletePref()
        assertEquals(Unit, actual)
    }

    @Test
    fun deleteAllTest() = runBlocking {
        whenever(sessionManager.setFirstInstall(false)).thenReturn(Unit)
        whenever(sessionManager.setAccessToken("")).thenReturn(Unit)
        whenever(sessionManager.setRefreshToken("")).thenReturn(Unit)
        whenever(sessionManager.setProfile("1.png", "hafid")).thenReturn(Unit)
        whenever(sessionManager.deleteAll()).thenReturn(Unit)
        val actual = repository.deleteAll()
        assertEquals(Unit, actual)
    }
}
