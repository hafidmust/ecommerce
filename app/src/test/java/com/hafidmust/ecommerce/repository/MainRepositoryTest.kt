package com.hafidmust.ecommerce.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.hafidmust.ecommerce.data.repository.MainRepository
import com.hafidmust.ecommerce.core.source.SessionManager
import com.hafidmust.ecommerce.core.source.local.entity.NotificationEntity
import com.hafidmust.ecommerce.core.source.local.room.NotificationDao
import com.hafidmust.ecommerce.core.source.network.ApiService
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.core.source.network.response.DetailProductResponse
import com.hafidmust.ecommerce.core.source.network.response.ProductReviewResponse
import com.hafidmust.ecommerce.core.source.network.response.SearchResponse
import com.hafidmust.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class MainRepositoryTest {
    private lateinit var repository: MainRepository

    @Mock
    private lateinit var apiService: ApiService

    @Mock
    private lateinit var sessionManager: SessionManager

    @Mock
    private lateinit var notification: NotificationDao

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        repository = MainRepository(
            apiService = apiService,
            sessionManager = sessionManager,
            notification = notification
        )
    }

    @Test
    fun searchProductTest() = runBlocking {
        val dummyResponse = mock(SearchResponse::class.java)
        whenever(
            apiService.search(
                token = "Bearer",
                query = "query"
            )
        ).thenReturn(dummyResponse)
        val result = repository.searchProduct("query")
        result.observeForever {
            if (it is Result.Success) {
                assertEquals(dummyResponse, it)
            }
        }
    }

//    @Test
//    fun detailProductTest() = runBlocking {
//        val dummyResponse = mock(DetailProductResponse::class.java)
//        whenever(
//            apiService.productDetail(
//                token = "",
//                productId = ""
//            )
//        ).thenReturn(dummyResponse)
//
//        repository.detailProduct("").collect {
//            if (it is Result.Success) {
//                assertEquals(dummyResponse, it)
//            }
//        }
//    }

    @Test
    fun reviewProductTest() = runBlocking {
        val dummyResponse = mock(ProductReviewResponse::class.java)
        whenever(
            apiService.productReview(
                token = "",
                productId = ""
            )
        ).thenReturn(dummyResponse)

        val actualResponse = repository.reviewProduct("")
        actualResponse.observeForever {
            if (it is Result.Success) {
                assertEquals(dummyResponse, it)
            }
        }
    }

    @Test
    fun insertNotificationTest() = runBlocking {
        whenever(notification.insertNotification(mock())).thenReturn(Unit)
        val actual = repository.insertNotification(mock())

        assertEquals(Unit, actual)
    }

    @Test
    fun getAllNotificationTest() = runBlocking {
        val dummyData = mock(NotificationEntity::class.java)
        whenever(notification.getAllNotification()).thenReturn(flowOf(listOf(dummyData)))
        val actual = repository.getAllNotification()
        assertEquals(listOf(dummyData), actual.first())
    }

    @Test
    fun updateNotificationTest() = runBlocking {
        whenever(notification.updateIsRead(true, 1)).thenReturn(Unit)
        val actual = repository.updateNotification(true, 1)
        assertEquals(Unit, actual)
    }

    @Test
    fun getUnreadNotification() = runBlocking {
        val dummyData = flowOf(1)
        whenever(notification.getUnreadNotification()).thenReturn(dummyData)
        val actual = repository.getUnreadNotification().first()
        assertEquals(1, actual)
    }
}
