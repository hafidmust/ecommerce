package com.hafidmust.ecommerce.repository

import com.hafidmust.ecommerce.data.repository.CartRepository
import com.hafidmust.ecommerce.core.source.local.entity.CartItemEntity
import com.hafidmust.ecommerce.core.source.local.room.CartDao
import com.hafidmust.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class CartRepositoryTest {

    private lateinit var repository: CartRepository

    @Mock
    private lateinit var cartDao: CartDao

    private val EXPECTED_VALUE = CartItemEntity(
        productId = "12345",
        productName = "Product A",
        productPrice = 100,
        image = "product_a.jpg",
        brand = "Brand X",
        description = "Lorem ipsum dolor sit amet...",
        store = "Store 1",
        sale = 20,
        stock = 50,
        totalRating = 4,
        totalReview = 10,
        totalSatisfaction = 90,
        productRating = 4.5,
        productVariant = "Variant A",
        quantity = 2,
        isChecked = false
    )

    @get:Rule
    var mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        repository = CartRepository(cartDao)
    }

    @Test
    fun getCartTest() = runBlocking {
        val dummyData = listOf(
            EXPECTED_VALUE
        )
        whenever(cartDao.getAllCart()).thenReturn(flowOf(dummyData))

        val actualValue = repository.getCart().first()
        assertEquals(dummyData, actualValue)
    }

    @Test
    fun insertTestCart() = runBlocking {
        val dummyData = EXPECTED_VALUE
        whenever(cartDao.insert(dummyData)).thenReturn(Unit)

        val actualValue = repository.insertCart(dummyData)
        assertEquals(Unit, actualValue)
    }

    @Test
    fun getItemByIdTest() = runBlocking {
        whenever(cartDao.getCartByProductId("1")).thenReturn(flowOf(EXPECTED_VALUE))
        val actual = repository.getItemById("1").first()
        assertEquals(EXPECTED_VALUE, actual)
    }

    @Test
    fun updateQuantityTest() = runBlocking {
        whenever(cartDao.updateQuantity("1", 2)).thenReturn(Unit)
        val actual = repository.updateQuantity("1", 3)
        assertEquals(Unit, actual)
    }

    @Test
    fun updateCheckCartTest() = runBlocking {
        whenever(cartDao.updateCheckCart(true, "1")).thenReturn(Unit)
        val actual = repository.updateCheckCart(true, "1")
        assertEquals(Unit, actual)
    }

    @Test
    fun updateCartTest() = runBlocking {
        val actualData = CartItemEntity(
            productId = "12345",
            productName = "Product A",
            productPrice = 100,
            image = "product_a.jpg",
            brand = "Brand X",
            description = "Lorem ipsum dolor sit amet...",
            store = "Store 1",
            sale = 20,
            stock = 50,
            totalRating = 4,
            totalReview = 10,
            totalSatisfaction = 90,
            productRating = 4.5,
            productVariant = "Variant A",
            quantity = 2,
            isChecked = false
        )
        whenever(cartDao.update(actualData)).thenReturn(Unit)
        val actual = repository.update(listOf())
        assertEquals(Unit, actual)
    }

    @Test
    fun getQuantityTest() = runBlocking {
        val dummyData = flowOf(1)
        whenever(cartDao.getQuantity()).thenReturn(dummyData)
        val actual = repository.getQuantity().first()
        assertEquals(1, actual)
    }

    @Test
    fun getQuantityByProductId() = runBlocking {
        val dummyData = flowOf(1)
        whenever(cartDao.getQuantityByProductId("1")).thenReturn(dummyData)
        val actual = repository.getQuantityByProductId("1").first()
        assertEquals(1, actual)
    }
}
