package com.hafidmust.ecommerce.repository

import com.hafidmust.ecommerce.data.repository.WishlistRepository
import com.hafidmust.ecommerce.core.source.local.entity.DataWishListEntity
import com.hafidmust.ecommerce.core.source.local.room.WishlistDao
import com.hafidmust.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class WishlistRepositoryTest {
    private lateinit var repository: WishlistRepository

    private val data = DataWishListEntity(
        image = "product_image_1.jpg",
        productId = "12345",
        description = "Product description 1",
        totalRating = 4,
        store = "Store A",
        productName = "Product A",
        totalSatisfaction = 90,
        sale = 10,
        productVariant = "Variant X",
        stock = 50,
        productRating = 4.5,
        brand = "Brand X",
        productPrice = 100,
        totalReview = 20,
        isChecked = true
    )

    @Mock
    private lateinit var wishlistDao: WishlistDao

    @get:Rule
    var mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        repository = WishlistRepository(wishlistDao)
    }

    @Test
    fun getWishlistTest() {
        val expectedValue = flowOf(
            listOf(
                data
            )
        )

        whenever(wishlistDao.getWishlist()).thenReturn(expectedValue)
        val actualValue = repository.getWishlist()
        assertEquals(expectedValue, actualValue)
    }

    @Test
    fun insertWishlistTest() = runTest {
        whenever(
            wishlistDao.insert(
                data
            )
        ).thenReturn(Unit)

        val actualValue = repository.insertWishlist(data)
        assertEquals(Unit, actualValue)
    }

    @Test
    fun deleteWishlistTest() = runTest {
        whenever(
            wishlistDao.delete(
                data
            )
        ).thenReturn(Unit)

        val actualValue = repository.deleteWishlist(data)
        assertEquals(Unit, actualValue)
    }

    @Test
    fun getIsChecked() = runTest {
        whenever(
            wishlistDao.getIsChecked(
                productId = "12345"
            )
        ).thenReturn(flowOf(true))

        val actualValue = repository.getIsChecked("12345").first()
        assertEquals(true, actualValue)
    }

    @Test
    fun updateIsChecked() = runTest {
        whenever(
            wishlistDao.updateIsChecked(
                isChecked = true,
                productId = "12345"
            )
        ).thenReturn(Unit)

        val actualValue = repository.updateIsChecked(true, "12345")
        assertEquals(Unit, actualValue)
    }

    @Test
    fun getQuantity() = runTest {
        whenever(wishlistDao.getQuantity()).thenReturn(flowOf(1))

        val actualValue = repository.getQuantity().first()
        assertEquals(1, actualValue)
    }

    @Test
    fun getWishlistbyId() = runTest {
        whenever(
            wishlistDao.getWishlistbyId(
                productId = "12345"
            )
        ).thenReturn(flowOf(data))

        val actualValue = repository.getWishlistbyId("12345").first()
        assertEquals(data, actualValue)
    }

    @Test
    fun updateTest() = runTest {
        whenever(
            wishlistDao.update(
                data
            )
        ).thenReturn(Unit)

        val actualValue = repository.update(data)
        assertEquals(Unit, actualValue)
    }
}
