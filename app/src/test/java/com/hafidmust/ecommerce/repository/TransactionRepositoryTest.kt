package com.hafidmust.ecommerce.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.hafidmust.ecommerce.data.repository.TransactionRepository
import com.hafidmust.ecommerce.core.source.SessionManager
import com.hafidmust.ecommerce.core.source.network.ApiService
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.core.source.network.request.RatingRequest
import com.hafidmust.ecommerce.core.source.network.request.TransactionRequest
import com.hafidmust.ecommerce.core.source.network.response.DetailPayment
import com.hafidmust.ecommerce.core.source.network.response.FulfillmentResponse
import com.hafidmust.ecommerce.core.source.network.response.RatingResponse
import com.hafidmust.ecommerce.core.source.network.response.TransactionResponse
import com.hafidmust.ecommerce.utils.MainDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class TransactionRepositoryTest {
    private lateinit var repository: TransactionRepository

    @Mock
    private lateinit var apiService: ApiService

    @Mock
    private lateinit var sessionManager: SessionManager

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainDispatcherRule = MainDispatcherRule()

    private val EXPECTED_VALUE = DetailPayment(
        code = 200,
        data = listOf(),
        message = "Success"
    )

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        repository = TransactionRepository(
            apiService = apiService,
            sessionManager = sessionManager
        )
    }

    @Test
    fun paymentMethodTest() = runTest {
        whenever(
            apiService.paymentMethod(
                token = ""
            )
        ).thenReturn(EXPECTED_VALUE)

        val actualValue = repository.paymentMethod()
        actualValue.observeForever {
            if (it is Result.Success) {
                assertEquals(EXPECTED_VALUE.data, it.data)
            }
        }
    }

    @Test
    fun fulfillmentTest() = runTest {
        val expectedValue = FulfillmentResponse(
            code = 200,
            message = "",
            data = FulfillmentResponse.Data(
                invoiceId = "",
                status = true,
                date = "",
                time = "",
                payment = "",
                total = 0
            )
        )

        whenever(
            apiService.fulfillment(
                token = "",
                request = TransactionRequest(
                    payment = "",
                    items = listOf()
                )
            )
        ).thenReturn(expectedValue)

        val actualValue = repository.fulfillment(
            data = TransactionRequest(
                payment = "",
                items = listOf()
            )
        )

        actualValue.observeForever {
            when (it) {
                is Result.Error -> {
                }
                Result.Loading -> {
                }
                is Result.Success -> {
                    assertEquals(expectedValue.data, it.data)
                }
            }
        }
    }

    @Test
    fun ratingTest() = runTest {
        val expectedValue = RatingResponse(
            code = 200,
            message = ""
        )

        whenever(
            apiService.rating(
                token = "",
                request = RatingRequest(
                    invoiceId = "",
                    rating = 0,
                    review = ""
                )
            )
        ).thenReturn(expectedValue)

        val actualValue = repository.rating(
            data = RatingRequest(
                invoiceId = "",
                rating = 0,
                review = ""
            )
        )
        actualValue.observeForever {
            if (it is Result.Success) {
                assertEquals(expectedValue.code, it.data)
            }
        }
    }

    @Test
    fun transactionTest() = runTest {
        val expectedValue = TransactionResponse(
            code = 200,
            message = "",
            data = listOf()
        )

        whenever(
            apiService.transaction(
                token = ""
            )
        ).thenReturn(expectedValue)

        val actualValue = repository.transaction()
        actualValue.observeForever {
            if (it is Result.Success) {
                assertEquals(expectedValue.data, it.data)
            }
        }
    }
}
