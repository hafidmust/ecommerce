package com.hafidmust.ecommerce.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.hafidmust.ecommerce.core.source.SessionManager
import com.hafidmust.ecommerce.core.source.network.ApiService
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.core.source.network.request.UserRequest
import com.hafidmust.ecommerce.core.source.network.response.ErrorResponse
import com.hafidmust.ecommerce.core.source.network.response.LoginResponse
import com.hafidmust.ecommerce.core.source.network.response.ProfileResponse
import com.hafidmust.ecommerce.core.source.network.response.RegisterResponse
import com.squareup.moshi.Moshi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import okhttp3.MultipartBody
import retrofit2.HttpException
import javax.inject.Inject

class AuthRepository @Inject constructor(
    private val apiService: ApiService,
    private val sessionManager: SessionManager,
    private val firebaseAnalytics: FirebaseAnalytics
) {

    suspend fun testRefresh() {
        try {
            apiService.testApi(token = "Bearer ${getAccessToken().first()}")
        } catch (e: Throwable) {
            Log.e("testApi", e.message.toString())
        }
    }

    suspend fun setTheme(isDark: Boolean) {
        sessionManager.setTheme(isDark)
    }

    suspend fun getTheme(): Flow<Boolean> {
        return sessionManager.getTheme()
    }

    fun register(userRequest: UserRequest): LiveData<Result<RegisterResponse>> =
        liveData {
            try {
                emit(Result.Loading)
                val response = apiService.register(
                    apiKey = "6f8856ed-9189-488f-9011-0ff4b6c08edc",
                    userRequest = userRequest
                )
                setAccessToken(
                    accessToken = response.data.accessToken,
                )
                setRefreshToken(refreshToken = response.data.refreshToken)
                emit(Result.Success(response))
            } catch (e: HttpException) {
                val errorResponse = e.response()?.errorBody()?.let {
                    val moshi = Moshi.Builder().build()
                    val adapter = moshi.adapter(ErrorResponse::class.java)
                    adapter.fromJson(it.string())
                }

                if (errorResponse != null) {
                    // Tampilkan pesan kesalahan dalam Toast
                    val errorMessage = errorResponse.message
                    emit(Result.Error(errorMessage))
                }
            } catch (e: Throwable) { // versi global
                when (e) {
                    is HttpException -> {
//                        Log.e("http",e.response()?.errorBody().toString())
                        // serialize
                        try {
                            val errorResponse = e.response()?.errorBody()?.string().let {
                                val moshi = Moshi.Builder().build()
                                val adapter = moshi.adapter(ErrorResponse::class.java)
                                adapter.fromJson(it)
                            }
                            errorResponse?.message?.let { Result.Error(it) }?.let { emit(it) }
                        } catch (e: Exception) {
                            Log.e("zzz", "adf")
                        }
                    }
                }
                emit(Result.Error(e.message.toString()))
                Log.e("Auth Repository", e.message.toString())
            }
        }

    fun login(email: String, password: String): LiveData<Result<LoginResponse>> =
        liveData {
            try {
                emit(Result.Loading)
                val response = apiService.login(
                    apiKey = "6f8856ed-9189-488f-9011-0ff4b6c08edc",
                    userRequest = UserRequest(
                        email = email,
                        password = password,
                        firebaseToken = ""
                    )
                )
                setAccessToken(
                    accessToken = response.data.accessToken,
                )
                setRefreshToken(refreshToken = response.data.refreshToken)
                emit(Result.Success(response))
            } catch (e: HttpException) {
                val errorResponse = e.response()?.errorBody()?.let {
                    val moshi = Moshi.Builder().build()
                    val adapter = moshi.adapter(ErrorResponse::class.java)
                    adapter.fromJson(it.string())
                }

                if (errorResponse != null) {
                    // Tampilkan pesan kesalahan dalam Toast
                    val errorMessage = errorResponse.message
                    emit(Result.Error(errorMessage))
                }
            }
        }

    suspend fun profile(
        token: String,
        image: MultipartBody.Part?,
        username: MultipartBody.Part
    ): LiveData<Result<ProfileResponse>> =
        liveData {
            try {
                emit(Result.Loading)
                val response =
                    apiService.profile(token = "Bearer $token", userImage = image, user = username)
                if (response.data.userImage == "http://192.168.190.125:8080/static/images/") {
                    setProfile("", response.data.userName)
                }
                setProfile(response.data.userImage, response.data.userName)

                emit(Result.Success(response))
            } catch (e: Exception) {
                emit(Result.Error(e.message.toString()))
            }
        }

    suspend fun setProfile(uri: String, name: String) {
        sessionManager.setProfile(uri, name)
    }

    suspend fun setFirstInstall(firstInstall: Boolean) {
        sessionManager.setFirstInstall(firstInstall)
    }

    fun getAccessToken(): Flow<String> {
        return sessionManager.getAccessToken()
    }

    suspend fun logout() = sessionManager.deleteToken()

    fun getUriProfile(): Flow<String> {
        return sessionManager.getUriProfile()
    }

    fun getNameProfile(): Flow<String> {
        return sessionManager.getNameProfile()
    }

    fun getFirstInstall(): Flow<Boolean> {
        return sessionManager.getFirstInstall()
    }

    fun getRefreshToken() = sessionManager.getRefreshToken()

    suspend fun setAccessToken(accessToken: String) = sessionManager.setAccessToken(accessToken)

    suspend fun setRefreshToken(refreshToken: String) = sessionManager.setRefreshToken(refreshToken)

    suspend fun deletePref() = sessionManager.deletePreference()

    suspend fun deleteAll() = sessionManager.deleteAll()

    fun loginEvent(email: String) = firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN) {
        param(FirebaseAnalytics.Param.ITEM_ID, "login")
        param(FirebaseAnalytics.Param.ITEM_NAME, email)
    }

    fun registerEvent(email: String) = firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP) {
        param(FirebaseAnalytics.Param.ITEM_ID, "register")
        param(FirebaseAnalytics.Param.ITEM_NAME, email)
    }
}
