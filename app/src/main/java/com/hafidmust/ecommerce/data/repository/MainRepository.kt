package com.hafidmust.ecommerce.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import com.hafidmust.ecommerce.data.StorePagingSource
import com.hafidmust.ecommerce.core.source.SessionManager
import com.hafidmust.ecommerce.core.source.local.entity.NotificationEntity
import com.hafidmust.ecommerce.core.source.local.room.NotificationDao
import com.hafidmust.ecommerce.core.source.network.ApiService
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.core.source.network.response.DetailProductResponse
import com.hafidmust.ecommerce.core.source.network.response.ItemsItem
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class MainRepository @Inject constructor(
    private val apiService: ApiService,
    private val sessionManager: SessionManager,
    private val notification: NotificationDao
) {
    fun getProducts(
        search: String?,
        brand: String?,
        lowest: Int?,
        highest: Int?,
        sort: String?,
    ): LiveData<PagingData<ItemsItem>> {
        return Pager(
            config = PagingConfig(
                prefetchDistance = 1,
                initialLoadSize = 10,
                pageSize = 10
            ),
            pagingSourceFactory = {
                StorePagingSource(
                    apiService = apiService,
                    sessionManager = sessionManager,
                    search,
                    brand,
                    lowest,
                    highest,
                    sort
                )
            }
        ).liveData
    }

    fun searchProduct(query: String) = liveData {
        emit(Result.Loading)
        try {
            emit(Result.Loading)
            val data = apiService.search(
                token = "Bearer ${sessionManager.getAccessToken().first()}",
                query = query
            )
            emit(Result.Success(data.data))
            Log.d("search product", data.message)
        } catch (e: Throwable) {
            emit(Result.Error(e.message.toString()))
            Log.e("search product", e.localizedMessage.toString())
        }
    }

    fun detailProduct(productId: String) = liveData {
        try {
            emit(Result.Loading)
            val detailProduct = apiService.productDetail(
                "Bearer ${sessionManager.getAccessToken().first()}",
                productId
            )
            emit(Result.Success(detailProduct.data))
        } catch (e: Throwable) {
            emit(Result.Error(e.message.toString()))
        }
    }

    fun reviewProduct(productId: String) = liveData {
        try {
            emit(Result.Loading)
            val reviewProduct = apiService.productReview(
                token = "Bearer ${sessionManager.getAccessToken().first()}",
                productId = productId
            )
            emit(Result.Success(reviewProduct.data))
        } catch (e: Throwable) {
            emit(Result.Error(e.message.toString()))
        }
    }

    suspend fun insertNotification(data: NotificationEntity) = notification.insertNotification(data)

    fun getAllNotification() = notification.getAllNotification()

    suspend fun updateNotification(isRead: Boolean, id: Int) = notification.updateIsRead(isRead, id)
    fun getUnreadNotification() = notification.getUnreadNotification()

    fun deleteAllNotification() = notification.deleteAllNotification()
}
