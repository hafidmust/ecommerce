package com.hafidmust.ecommerce.data.repository

import androidx.lifecycle.liveData
import com.hafidmust.ecommerce.core.source.SessionManager
import com.hafidmust.ecommerce.core.source.network.ApiService
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.core.source.network.request.RatingRequest
import com.hafidmust.ecommerce.core.source.network.request.TransactionRequest
import kotlinx.coroutines.flow.first
import javax.inject.Inject

class TransactionRepository @Inject constructor(
    private val sessionManager: SessionManager,
    private val apiService: ApiService
) {
    fun paymentMethod() = liveData {
        try {
            emit(Result.Loading)
            val payment = apiService.paymentMethod(
                token = "Bearer ${sessionManager.getAccessToken().first()}"
            )
            if (payment.data == null) {
                emit(Result.Error("Empty"))
            } else {
                emit(Result.Success(payment.data))
            }
        } catch (e: Throwable) {
            emit(Result.Error(e.message.toString()))
        }
    }

    fun fulfillment(data: TransactionRequest) = liveData {
        try {
            emit(Result.Loading)
            val fulfillment = apiService.fulfillment(
                token = "Bearer ${sessionManager.getAccessToken().first()}",
                request = data
            )
            emit(Result.Success(fulfillment.data))
        } catch (e: Throwable) {
            emit(Result.Error(e.message.toString()))
        }
    }

    fun rating(data: RatingRequest) = liveData {
        try {
            emit(Result.Loading)
            val rating = apiService.rating(
                token = "Bearer ${sessionManager.getAccessToken().first()}",
                request = data
            )
            emit(Result.Success(rating.code))
        } catch (e: Throwable) {
            emit(Result.Error(e.message.toString()))
        }
    }

    fun transaction() = liveData {
        try {
            emit(Result.Loading)
            val transaction = apiService.transaction(
                token = "Bearer ${sessionManager.getAccessToken().first()}"
            )
            emit(Result.Success(transaction.data))
        } catch (e: Throwable) {
            emit(Result.Error(e.message.toString()))
        }
    }
}
