package com.hafidmust.ecommerce.data.repository

import com.hafidmust.ecommerce.core.source.local.entity.DataWishListEntity
import com.hafidmust.ecommerce.core.source.local.room.WishlistDao
import javax.inject.Inject

class WishlistRepository @Inject constructor(
    private val wishlistDao: WishlistDao
) {
    fun getWishlist() = wishlistDao.getWishlist()
    suspend fun insertWishlist(wishlist: DataWishListEntity) = wishlistDao.insert(wishlist)
    suspend fun deleteWishlist(wishlist: DataWishListEntity) {
        return wishlistDao.delete(wishlist)
    }

    fun getIsChecked(productId: String) = wishlistDao.getIsChecked(productId)

    suspend fun updateIsChecked(isChecked: Boolean, productId: String) = wishlistDao.updateIsChecked(
        isChecked,
        productId
    )

    suspend fun getQuantity() = wishlistDao.getQuantity()

    fun getWishlistbyId(productId: String) = wishlistDao.getWishlistbyId(productId)

    suspend fun update(data: DataWishListEntity) = wishlistDao.update(data)

    fun deleteAllWishlist() = wishlistDao.deleteAllWishlist()
}
