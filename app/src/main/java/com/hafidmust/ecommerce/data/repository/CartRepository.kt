package com.hafidmust.ecommerce.data.repository

import com.hafidmust.ecommerce.core.source.local.entity.CartItemEntity
import com.hafidmust.ecommerce.core.source.local.room.CartDao
import javax.inject.Inject

class CartRepository @Inject constructor(
    private val cartDao: CartDao
) {
    fun getCart() = cartDao.getAllCart()

    suspend fun insertCart(cart: CartItemEntity) = cartDao.insert(cart)

    suspend fun deleteItemCart(cart: CartItemEntity) {
        return cartDao.delete(cart)
    }

    suspend fun deleteAll(cart: List<CartItemEntity>) {
        return cartDao.delete(*cart.toTypedArray())
    }

    suspend fun deleteAll(cart: CartItemEntity) {
        return cartDao.delete()
    }

    fun getItemById(productId: String) = cartDao.getCartByProductId(productId)

    suspend fun updateQuantity(id: String, newQty: Int) = cartDao.updateQuantity(id, newQty)

    suspend fun updateCheckCart(checked: Boolean, productId: String) =
        cartDao.updateCheckCart(checked, productId)

    suspend fun update(cart: List<CartItemEntity>) {
        val data = cart.toTypedArray()
        cartDao.update(*data)
    }

    fun getQuantity() = cartDao.getQuantity()

    fun getQuantityByProductId(productId: String) = cartDao.getQuantityByProductId(productId)

    fun deleteAllCart() = cartDao.deleteAllCart()
}
