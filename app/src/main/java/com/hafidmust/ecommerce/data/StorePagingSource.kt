package com.hafidmust.ecommerce.data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.hafidmust.ecommerce.core.source.SessionManager
import com.hafidmust.ecommerce.core.source.network.ApiService
import com.hafidmust.ecommerce.core.source.network.response.ItemsItem
import kotlinx.coroutines.flow.first
import javax.inject.Inject

class StorePagingSource @Inject constructor(
    private val apiService: ApiService,
    private val sessionManager: SessionManager,
    val search: String?,
    val brand: String?,
    val lowest: Int?,
    val highest: Int?,
    val sort: String?,
) : PagingSource<Int, ItemsItem>() {
    override fun getRefreshKey(state: PagingState<Int, ItemsItem>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ItemsItem> {
        return try {
            val page = params.key ?: 1
            val limit = params.loadSize
            val responseData =
                apiService.products(
                    page = page,
                    token = "Bearer ${sessionManager.getAccessToken().first()}",
                    search = search,
                    brand = brand,
                    lowest = lowest,
                    highest = highest,
                    sort = sort,
                    limit = limit
                )

            LoadResult.Page(
                data = responseData.data.items,
                prevKey = if (page == 1) null else page - 1,
                nextKey = if (responseData.data.totalPages == page || responseData.data.items.isEmpty() || responseData.code == 404) null else page + 1
            )

        } catch (exception: Exception) {
            return LoadResult.Error(exception)
        }
    }
}
