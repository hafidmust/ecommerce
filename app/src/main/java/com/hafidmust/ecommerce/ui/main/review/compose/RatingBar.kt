package com.hafidmust.ecommerce.ui.main.review.compose

import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.filled.StarBorder
import androidx.compose.material.icons.outlined.Star
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun RatingBar(
    maxRating: Int,
    initial: Int
) {
    FlowRow {
        repeat(maxRating) {
            Icon(
                modifier = Modifier.size(17.dp),
                imageVector = if (it <= initial) Icons.Default.Star else Icons.Default.StarBorder,
                contentDescription = null
            )
        }
    }
}

@Preview
@Composable
fun RatingBarPrev() {
    RatingBar(maxRating = 5, initial = 3)
}
