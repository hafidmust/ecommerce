package com.hafidmust.ecommerce.ui.main.checkout

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hafidmust.ecommerce.data.repository.CartRepository
import com.hafidmust.ecommerce.data.repository.TransactionRepository
import com.hafidmust.ecommerce.core.source.local.entity.CartItemEntity
import com.hafidmust.ecommerce.core.source.local.entity.CheckoutEntity
import com.hafidmust.ecommerce.core.source.network.request.TransactionRequest
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CheckoutViewModel @Inject constructor(
    private val transactionRepository: TransactionRepository,
    private val cartRepository: CartRepository
) : ViewModel() {

    private val _checkouts = MutableStateFlow<List<CheckoutEntity>?>(null)
    val checkouts: MutableStateFlow<List<CheckoutEntity>?> get() = _checkouts

    val carts: Flow<List<CartItemEntity>> = cartRepository.getCart()

    fun fulfillment(data: TransactionRequest) = transactionRepository.fulfillment(data)

    fun setCheckout(checkout: List<CheckoutEntity>) {
        _checkouts.value = checkout
    }

    fun updateQuantity(item: CheckoutEntity, newQuantity: Int) {
        viewModelScope.launch {
            _checkouts.value = _checkouts.value?.map {
                if (it.id == item.id) {
                    it.copy(quantity = newQuantity)
                } else {
                    it
                }
            }
        }
    }

    val totalProductPrice: Int
        get() = checkouts.value?.sumOf { it.productPrice * it.quantity } ?: 0
}
