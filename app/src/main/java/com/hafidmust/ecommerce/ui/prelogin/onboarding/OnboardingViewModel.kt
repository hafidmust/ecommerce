package com.hafidmust.ecommerce.ui.prelogin.onboarding

import androidx.lifecycle.ViewModel
import com.hafidmust.ecommerce.data.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class OnboardingViewModel @Inject constructor(private val authRepository: AuthRepository) :
    ViewModel() {

    fun getFirstInstall() = authRepository.getFirstInstall()

    suspend fun setOnboardingState(firstInstall: Boolean) = authRepository.setFirstInstall(firstInstall)

    fun getAccessToken() = authRepository.getAccessToken()
    fun getRefreshToken() = authRepository.getRefreshToken()

    suspend fun getTheme() = authRepository.getTheme()
}
