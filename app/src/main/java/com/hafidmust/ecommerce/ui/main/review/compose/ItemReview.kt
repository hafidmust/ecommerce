package com.hafidmust.ecommerce.ui.main.review.compose

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.hafidmust.ecommerce.ui.ui.theme.poppins

@Composable
fun ItemReview(
    image: String,
    name: String,
    review: String,
    rating: Int
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 16.dp)
    ) {
        Row(
            modifier = Modifier.padding(horizontal = 16.dp)
        ) {
            AsyncImage(
                modifier = Modifier
                    .size(36.dp)
                    .clip(CircleShape),
                model = image,
                contentDescription = null
            )
            Column(
                modifier = Modifier.padding(start = 8.dp)
            ) {
                Text(
                    text = name,
                    style = TextStyle(
                        fontSize = 12.sp,
                        lineHeight = 16.sp,
                        fontFamily = poppins,
                        fontWeight = FontWeight.SemiBold,
                        color = MaterialTheme.colorScheme.onSurfaceVariant,
                    )
                )
                RatingBar(maxRating = 5, initial = rating)
            }
        }

        Text(
            modifier = Modifier
                .padding(top = 8.dp, bottom = 16.dp)
                .padding(horizontal = 16.dp),
            text = review,
            style = TextStyle(
                fontSize = 12.sp,
                lineHeight = 16.sp,
                fontFamily = poppins,
                fontWeight = FontWeight.Normal,
                color = MaterialTheme.colorScheme.onSurfaceVariant,
            )
        )
        Divider(
            thickness = 1.dp
        )
    }
}

@Preview(showBackground = true)
@Composable
fun ItemReviewPrev() {
    ItemReview(
        image = "https://ecs7.tokopedia.net/img/cache/700/product-1/2021/7/16/119744/119744_5e3b3b7a-7b1a-4b1a-9b0a-9b0a0a0a0a0a_700_700",
        name = "John Doe",
        review = "Lorem",
        rating = 4
    )
}
