package com.hafidmust.ecommerce.ui.main.review

import androidx.lifecycle.ViewModel
import com.hafidmust.ecommerce.data.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ReviewViewModel @Inject constructor(private val mainRepository: MainRepository) : ViewModel() {
    fun getReviewProduct(productId: String) = mainRepository.reviewProduct(productId)
}
