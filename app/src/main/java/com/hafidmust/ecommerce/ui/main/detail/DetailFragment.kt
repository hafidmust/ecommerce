package com.hafidmust.ecommerce.ui.main.detail

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.material.chip.Chip
import com.google.android.material.snackbar.Snackbar
import com.hafidmust.ecommerce.core.source.network.response.DetailProductResponse
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.core.source.network.response.toCartEntity
import com.hafidmust.ecommerce.core.source.network.response.toWishlistEntity
import com.hafidmust.ecommerce.databinding.FragmentDetailBinding
import com.hafidmust.ecommerce.ui.adapter.DetailImageAdapter
import com.hafidmust.ecommerce.utils.rupiahFormatter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class DetailFragment : Fragment() {

    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private var param1: String? = null
    private var param2: String? = null

    private val viewModel: DetailViewModel by viewModels()

    private var selectedVariant: DetailProductResponse.Data.ProductVariant? = null
    private var price: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDetailBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findNavController().currentBackStack.value.forEach {
            Log.d("detailFragment", it.toString())
        }
//        val id = "0"
        val id = DetailFragmentArgs.fromBundle(arguments as Bundle).productId
        // remove chip
        binding.groupVariant.removeAllViews()

        binding.topAppBar.title = "Detail Produk"
        binding.topAppBar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        viewModel.detailProduct(id).observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Error -> {
                    Toast.makeText(context, result.msg, Toast.LENGTH_SHORT).show()
                }

                Result.Loading -> {
                     binding.progressBar.visibility = View.VISIBLE
                }

                is Result.Success -> {
                    binding.apply {
                        progressBar.isVisible = false
                        val adapter = result.data.image.let { DetailImageAdapter(it) }
                        viewPagerImage.adapter = adapter
                        dotsIndicator.attachTo(viewPagerImage)
                        price = result.data.productPrice
                        tvDetailPrice.text = result.data.productPrice.let { rupiahFormatter(it) }
                        tvDetailName.text = result.data.productName
                        tvDetailQty.text = "Terjual ${result.data.sale}"
                        chipDetailRating.text =
                            "${result.data.productRating} ( ${result.data.totalRating} )"
                        tvDetailDesc.text = result.data.description
                        tvDetailRating.text = result.data.productRating.toString()
                        tvDetailSatisfying.text =
                            "${result.data.totalSatisfaction}% pembeli merasa puas"
                        tvDetailRatingReview.text =
                            "${result.data.totalRating} rating · ${result.data.totalReview} ulasan"

                        result.data.productVariant.let { setDefaultVariant(it) }

                        result.data.productVariant.forEachIndexed { index, variant ->
                            val chip = Chip(requireActivity())
                            chip.text = variant.variantName
                            chip.isCheckable = true
                            chip.isClickable = true
                            groupVariant.addView(chip)
                            chip.setOnClickListener {
                                updateSelectedVariant(variant)
                            }

                            if (index == 0) {
                                chip.isChecked = true
                                updateSelectedVariant(variant)
                            }
                        }

                    }

 //                    viewLifecycleOwner.lifecycleScope.launch {
 //                        if (viewModel.getIsChecked(id).first() != null) {
 //                            binding.btnFav.isChecked = viewModel.getIsChecked(id).first()!!
 //                        }
 //                    }

                    binding.btnFav.setOnClickListener {
                        val data = result.data.copy().toWishlistEntity()
                        viewLifecycleOwner.lifecycleScope.launch {
                            if (binding.btnFav.isChecked) {
                                viewModel.insertWishlist(data)
                            } else {
                                viewModel.deleteWishlist(data)
                            }
                        }
                    }


                    binding.btnCart.setOnClickListener {
                        viewLifecycleOwner.lifecycleScope.launch {
                            if (viewModel.isQuantityValid(id, result.data.stock)) {
                                selectedVariant?.let {
                                    val reponse = result.data.copy(
                                        productVariant = listOf(it)
                                    )
                                    viewModel.insertCart(reponse.toCartEntity(it.variantName))
 //                                Log.d("qtyCart", qtyCart.toString())
                                    Snackbar.make(view, "Berhasil menambahkan keranjang", Snackbar.LENGTH_SHORT).show()

                                }
                            }else{
                                Snackbar.make(view, "Melebihi batas stock product", Snackbar.LENGTH_SHORT).show()
                            }


 //                            viewModel.insertCart(
 //                                result.data.toCartEntity()
 //                            )

                        }
                    }

                    binding.btnShare.setOnClickListener {
                        val sendIntent = Intent().apply {
                            action = Intent.ACTION_SEND
                            putExtra(
                                Intent.EXTRA_TEXT,
                                "Coba cek ini di Ecommerce, deh. Harganya ${rupiahFormatter(result.data.productPrice)} \n" +
                                        "https://ecommerce.id/${result.data.productId}"
                            )
                            type = "text/plain"
                        }

                        val shareIntent = Intent.createChooser(sendIntent, null)
                        startActivity(shareIntent)
                    }

                    binding.btnBuyNow.setOnClickListener {
                         val toCheckout =
                             DetailFragmentDirections.actionDetailFragmentToCheckoutFragment(
                                 arrayOf(selectedVariant?.variantName?.let { it1 ->
                                     result.data.toCartEntity(
                                         it1
                                     )
                                 })
                             )
                         findNavController().navigate(toCheckout)
                    }
                }

            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.wishlist.collect {
                val isWishlist = it.any { data ->
                    data.productId == id
                }
                binding.btnFav.isChecked = isWishlist
            }
        }

        binding.btnSeeAll.setOnClickListener {
            val toReviewProduct = DetailFragmentDirections.actionDetailFragmentToReviewFragment(id)
            findNavController().navigate(toReviewProduct)
        }
    }

    private fun setDefaultVariant(variants: List<DetailProductResponse.Data.ProductVariant>) {
        if (variants.isNotEmpty()) {
            selectedVariant = variants[0]
            updateTextPrice()
        }
    }

    private fun updateSelectedVariant(variant: DetailProductResponse.Data.ProductVariant) {
        selectedVariant = variant
        updateTextPrice()
    }

    private fun updateTextPrice() {
        selectedVariant?.let { variant ->
            val newPrice = if (variant.variantPrice == 0) {
                price
            } else {
                price?.plus(variant.variantPrice)
            }

            binding.tvDetailPrice.text = newPrice?.let { rupiahFormatter(it) }
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DetailFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            DetailFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
