package com.hafidmust.ecommerce.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hafidmust.ecommerce.core.source.local.entity.CheckoutEntity
import com.hafidmust.ecommerce.databinding.ItemCheckoutBinding
import com.hafidmust.ecommerce.utils.rupiahFormatter

class CheckoutAdapter(
    private val onAddQuantity: (CheckoutEntity) -> Unit,
    private val onSubtractQuantity: (CheckoutEntity) -> Unit,
) :
    ListAdapter<CheckoutEntity, CheckoutAdapter.CheckoutViewHolder>(CheckoutDiffUtils()) {
    inner class CheckoutViewHolder(val binding: ItemCheckoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: CheckoutEntity) {
            with(binding) {
                itemTvTitle.text = data.productName
                itemStok.text = if (data.stock < 10) "Tersisa ${data.stock} " else "Stok ${data.stock}"
                itemVariant.text = data.productVariant
                Glide.with(binding.root)
                    .load(data.image)
                    .into(binding.itemIvProduct)
                tvQty.text = data.quantity.toString()

                itemPrice.text = rupiahFormatter(data.productPrice)
                btnAdd.setOnClickListener {
                    onAddQuantity(data)
                }

                btnRemove.setOnClickListener {
                    onSubtractQuantity(data)
                }
            }
        }
    }

    class CheckoutDiffUtils : DiffUtil.ItemCallback<CheckoutEntity>() {
        override fun areItemsTheSame(oldItem: CheckoutEntity, newItem: CheckoutEntity): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: CheckoutEntity, newItem: CheckoutEntity): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckoutViewHolder {
        val binding =
            ItemCheckoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CheckoutViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CheckoutViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}
