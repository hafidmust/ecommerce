package com.hafidmust.ecommerce.ui.prelogin.login

import androidx.lifecycle.ViewModel
import com.hafidmust.ecommerce.data.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val authRepository: AuthRepository) : ViewModel() {
    fun login(email: String, password: String) = authRepository.login(email, password)
}
