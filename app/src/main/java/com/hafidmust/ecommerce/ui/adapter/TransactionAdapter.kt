package com.hafidmust.ecommerce.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hafidmust.ecommerce.core.source.network.response.TransactionResponse
import com.hafidmust.ecommerce.databinding.ItemTransactionBinding
import com.hafidmust.ecommerce.utils.rupiahFormatter

class TransactionAdapter(
    private val onReviewClick: (TransactionResponse.Data) -> Unit,
) : ListAdapter<TransactionResponse.Data, TransactionAdapter.TransactionViewHolder>(DIFF_CALLBACK) {
    inner class TransactionViewHolder(private val binding: ItemTransactionBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(dataItemTransaction: TransactionResponse.Data) {
            with(binding) {
                tvProductName.text = dataItemTransaction.name
                tvDate.text = dataItemTransaction.date
                Glide.with(binding.root)
                    .load(dataItemTransaction.image)
                    .into(binding.imageProduct)
                tvTotalPrice.text = rupiahFormatter(dataItemTransaction.total)

                binding.btnReview.isVisible =
                    dataItemTransaction.rating == 0 || dataItemTransaction.review?.isEmpty() == true

                binding.btnReview.setOnClickListener {
                    onReviewClick(
                        dataItemTransaction
                    )
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val binding =
            ItemTransactionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TransactionViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

object DIFF_CALLBACK : DiffUtil.ItemCallback<TransactionResponse.Data>() {
    override fun areItemsTheSame(
        oldItem: TransactionResponse.Data,
        newItem: TransactionResponse.Data
    ): Boolean {
        return oldItem.invoiceId == newItem.invoiceId
    }

    override fun areContentsTheSame(
        oldItem: TransactionResponse.Data,
        newItem: TransactionResponse.Data
    ): Boolean {
        return oldItem == newItem
    }
}
