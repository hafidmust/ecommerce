package com.hafidmust.ecommerce.ui.main.checkout

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.NotificationCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDeepLinkBuilder
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.hafidmust.ecommerce.MainActivity
import com.hafidmust.ecommerce.R
import com.hafidmust.ecommerce.core.source.local.entity.NotificationEntity
import com.hafidmust.ecommerce.core.source.local.entity.toCheckout
import com.hafidmust.ecommerce.core.source.local.entity.toFulfillment
import com.hafidmust.ecommerce.core.source.network.response.ItemItemPayment
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.databinding.FragmentCheckoutBinding
import com.hafidmust.ecommerce.core.di.EventLogger
import com.hafidmust.ecommerce.ui.adapter.CheckoutAdapter
import com.hafidmust.ecommerce.ui.main.notification.NotificationViewModel
import com.hafidmust.ecommerce.utils.rupiahFormatter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CheckoutFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class CheckoutFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var _binding: FragmentCheckoutBinding? = null
    private val binding get() = _binding!!
    private var paymentMethod: ItemItemPayment? = null

    private val viewModel: CheckoutViewModel by viewModels()
    private val notificationViewModel: NotificationViewModel by viewModels()

    @Inject
    lateinit var eventLogger: EventLogger

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentCheckoutBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        eventLogger.logBeginCheckout(bundleOf("begin_checkout" to "begin_checkout"))
        navigateBack()

        val dataFromCart = CheckoutFragmentArgs.fromBundle(arguments as Bundle).item
        val transformToCheckout = dataFromCart.map {
            it.toCheckout()
        }
        val checkoutEntity = viewModel.setCheckout(transformToCheckout)

        val checkoutAdapter = CheckoutAdapter(
            onAddQuantity = {
                Log.d("add", "quantity = ${it.quantity} stock = ${it.stock}")
                if (it.quantity < it.stock) {
                    viewModel.updateQuantity(it, it.quantity + 1)
                }
            },
            onSubtractQuantity = {
                if (it.quantity > 1) {
                    viewModel.updateQuantity(it, it.quantity - 1)
                }
            }
        )

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.checkouts.collectLatest { data ->
                checkoutAdapter.submitList(data)

                if (data != null) {
                    binding.tvTotalPay.text =
                        rupiahFormatter(viewModel.totalProductPrice)
                }
            }
        }

        binding.rvCheckout.apply {
            adapter = checkoutAdapter
            itemAnimator?.changeDuration = 0
        }

        Log.d("paymentMethod", paymentMethod.toString())
        findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<ItemItemPayment>(
            "paymentMethod"
        )?.observe(viewLifecycleOwner) {
            Log.d("paymentMethod", paymentMethod.toString())
            binding.tvPaymentMethod.text = it.label
            Glide.with(requireContext())
                .load(it.image)
                .into(binding.ivPaymentMethod)

            paymentMethod = it
            binding.btnPay.isEnabled = true
        }

//        binding.tvTotalPay.text =
//            rupiahFormatter(dataFromCart.sumOf { it.productPrice * it.quantity })

        binding.cardPayment.setOnClickListener {
            findNavController().navigate(R.id.action_checkoutFragment_to_paymentMethodFragment)
        }

        binding.btnPay.setOnClickListener {
            eventLogger.logPurchase(bundleOf("purchase" to "purchase"))
            transformToCheckout.forEach {
                val data = it.toFulfillment(paymentMethod?.label.toString())
                viewModel.fulfillment(data).observe(viewLifecycleOwner) { result ->
                    when (result) {
                        is Result.Error -> {
                        }

                        Result.Loading -> {
                            binding.progressBar.isVisible = true
                        }

                        is Result.Success -> {
                            if (result.data.status) {
                                lifecycleScope.launch(Dispatchers.IO) {
                                    val title = "Transaksi Berhasil"
                                    val body = "Transaksi dengan id ${result.data.invoiceId} berhasil"
                                    notificationViewModel.insertNotification(
                                        data = NotificationEntity(
                                            title = title,
                                            body = body,
                                            image = "",
                                            type = "transaction",
                                            date = result.data.date,
                                            time = result.data.time,
                                        )
                                    )
                                    // send notification
                                    sendNotification(title, body)
                                }

                                val datas =
                                    CheckoutFragmentDirections.actionCheckoutFragmentToRatingFragment(
                                        result.data.invoiceId,
                                        result.data.status,
                                        result.data.date,
                                        result.data.time,
                                        result.data.payment,
                                        result.data.total,
                                        "checkout"
                                    )
                                findNavController().navigate(datas)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun sendNotification(title: String, body: String) {
        val pendingIntent = NavDeepLinkBuilder(requireContext())
            .setComponentName(MainActivity::class.java)
            .setGraph(R.navigation.app_navigation)
            .setDestination(R.id.notificationFragment)
            .createPendingIntent()
        val notificationBuilder = NotificationCompat.Builder(
            requireContext(),
            NOTIFICATION_CHANNEL_ID
        )
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(title)
            .setContentText(body)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
        val notificationManager = requireContext().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel =
                NotificationChannel(
                    NOTIFICATION_CHANNEL_ID,
                    NOTIFICATION_CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT
                )
            notificationBuilder.setChannelId(NOTIFICATION_CHANNEL_ID)
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build())
    }

    private fun navigateBack() {
        binding.topAppBar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CheckoutFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CheckoutFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }

        private const val NOTIFICATION_ID = 2
        private const val NOTIFICATION_CHANNEL_ID = "Firebase Channel"
        private const val NOTIFICATION_CHANNEL_NAME = "transaction"
    }
}
