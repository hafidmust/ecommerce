package com.hafidmust.ecommerce.ui.prelogin.onboarding.data

data class OnboardingItem(
    val id: Int,
    val imageRes: Int
)
