package com.hafidmust.ecommerce.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.google.android.material.badge.ExperimentalBadgeUtils
import com.hafidmust.ecommerce.MainActivity
import com.hafidmust.ecommerce.R
import com.hafidmust.ecommerce.databinding.FragmentMainBinding
import com.hafidmust.ecommerce.ui.main.notification.NotificationViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

@ExperimentalBadgeUtils @AndroidEntryPoint
class MainFragment : Fragment() {

    private val viewModel: MainViewModel by viewModels()
    private val notificationViewModel: NotificationViewModel by viewModels()

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    private val navHostFragment: NavHostFragment by lazy {
        childFragmentManager.findFragmentById(R.id.nhf_child) as NavHostFragment
    }

    private val navController by lazy {
        navHostFragment.navController
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        _binding = FragmentMainBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.getName().firstOrNull {
                binding.topAppBar.title = it
                true
            }
        }

        runBlocking {
            val token = viewModel.getToken().first()
            Log.d("testtoken", token)
            if (token.isEmpty()) {
//                viewModel.deletePref()
//                val navController = Navigation.findNavController(requireActivity(), R.id.nhf_main)
//                navController.navigate(R.id.main_to_prelogin)
                (requireActivity() as MainActivity).logout()
            }
        }

//        lifecycleScope.launch {
//            val name = viewModel.getName().first()
//            val profilePicture = viewModel.getUri().first()
//            if (profilePicture.isEmpty()) {
//                (requireActivity() as MainActivity).checkUsername()
//            } else {
////                binding.topAppBar.title = name
//            }
//        }

        viewLifecycleOwner.lifecycleScope.launch {
            val badges = context?.let { BadgeDrawable.create(it) }
            badges?.let { BadgeUtils.attachBadgeDrawable(it, binding.topAppBar, R.id.cartMenu) }
            val quantity = viewModel.getQuantity().first()
            if (quantity != null) {
                badges?.number = quantity
                badges?.isVisible = quantity > 0
            }
        }

        val badgesNotification = context?.let { BadgeDrawable.create(it) }
        badgesNotification?.let { BadgeUtils.attachBadgeDrawable(it, binding.topAppBar, R.id.notificationMenu) }
        viewLifecycleOwner.lifecycleScope.launch {
            val notification = notificationViewModel.getUnreadNotification.first()
            badgesNotification?.isVisible = notification > 0
            badgesNotification?.number = notification
        }
//        viewLifecycleOwner.lifecycleScope.launch {
//            val notification = notificationViewModel.getUnreadNotification.first()
//            if (notification != null) {
//                badgesNotification?.let { BadgeUtils.attachBadgeDrawable(it, binding.topAppBar, R.id.notificationMenu) }
//                badgesNotification?.number = notification
//            }
//        }

        binding.topAppBar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.cartMenu -> {
                    (requireActivity() as MainActivity).cart()
                    true
                }
                R.id.listMenu -> {
                    (requireActivity() as MainActivity).screen()
                    true
                }
                R.id.notificationMenu -> {
                    (requireActivity() as MainActivity).toNotification()
                    true
                }
                else -> false
            }
        }

        binding.navView?.setupWithNavController(navController)
        binding.navView?.setOnItemReselectedListener {}
        binding.navViewRail?.setupWithNavController(navController)
        binding.navViewRail?.setOnItemReselectedListener {}
        binding.navViewBig?.setupWithNavController(navController)

        val badgeWishlist = binding.navView?.getOrCreateBadge(R.id.wishlistFragment)
        binding.navView?.getOrCreateBadge(R.id.wishlistFragment)
        viewLifecycleOwner.lifecycleScope.launch {
            val qty = viewModel.getQuantityWishlist().first()
            badgeWishlist?.number = qty
            badgeWishlist?.isVisible = qty > 0
        }
//        findNavController().currentBackStack.value.forEach {
//            if (it.destination.displayName == "com.hafidmust.ecommerce:id/transactionFragment"){
//                binding.navView.selectedItemId = R.id.transactionFragment
//            }
//        }

        findNavController().currentBackStack.value.forEach {
            Log.d("1056", it.arguments?.getString("origin").toString())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
