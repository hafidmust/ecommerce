package com.hafidmust.ecommerce.ui.main.store.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hafidmust.ecommerce.databinding.ItemSearchBinding

class SearchAdapter(private val items: List<String>, private val listener: ClickListener) :
    RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
    inner class ViewHolder(private val binding: ItemSearchBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: String, listener: ClickListener) {
            binding.tvSearch.text = data
            binding.root.setOnClickListener {
                listener.onItemClick(data)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemSearchBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], listener)
    }
}

interface ClickListener {
    fun onItemClick(data: String)
}
