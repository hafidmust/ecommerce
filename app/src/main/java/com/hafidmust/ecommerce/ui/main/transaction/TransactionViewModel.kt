package com.hafidmust.ecommerce.ui.main.transaction

import androidx.lifecycle.ViewModel
import com.hafidmust.ecommerce.data.repository.TransactionRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class TransactionViewModel @Inject constructor(private val repository: TransactionRepository) : ViewModel() {
    fun transaction() = repository.transaction()
}
