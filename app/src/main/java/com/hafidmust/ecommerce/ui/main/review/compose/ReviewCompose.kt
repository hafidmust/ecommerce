package com.hafidmust.ecommerce.ui.main.review.compose

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.core.source.network.response.ProductReviewResponse
import com.hafidmust.ecommerce.databinding.ReviewComposeBinding
import com.hafidmust.ecommerce.ui.main.review.ReviewViewModel
import com.hafidmust.ecommerce.ui.ui.theme.EcommerceTheme
import dagger.hilt.android.AndroidEntryPoint

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ReviewCompose.newInstance] factory method to
 * create an instance of this fragment.
 */
@OptIn(ExperimentalMaterial3Api::class)
@AndroidEntryPoint
class ReviewCompose : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var _binding: ReviewComposeBinding? = null
    private val binding get() = _binding!!
    private val viewModel: ReviewViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = ReviewComposeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = ReviewComposeArgs.fromBundle(arguments as Bundle).productId
        binding.composeView.apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                EcommerceTheme {
                    ReviewProductScreen(viewModel, id, onBackPressed = {
                        findNavController().navigateUp()
                    })
                }
            }
        }
    }

    @Composable
    fun ReviewProductScreen(
        viewModel: ReviewViewModel,
        id: String,
        onBackPressed: () -> Unit
    ) {
        val dataReview by viewModel.getReviewProduct(id).observeAsState()
        Scaffold(
            topBar = {
                TopAppBar(
                    title = {
                        Text("Ulasan Pembeli")
                    },
                    navigationIcon = {
                        Icon(
                            Icons.Default.ArrowBack,
                            contentDescription = null,
                            modifier = Modifier.clickable {
                                onBackPressed()
                            }
                        )
                    }
                )
            },
            content = {
                if (dataReview != null) {
                    when (dataReview) {
                        is Result.Error -> {
                        }
                        Result.Loading -> {
                        }
                        is Result.Success -> {
                            ReviewProductContent(
                                modifier = Modifier.padding(it),
                                data = (dataReview as Result.Success<List<ProductReviewResponse.DataReview>>).data
                            )
                        }
                        null -> {
                        }
                    }
                }
            }
        )
    }

    @Composable
    fun ReviewProductContent(
        modifier: Modifier = Modifier,
        data: List<ProductReviewResponse.DataReview>?
    ) {
        LazyColumn(
            modifier = modifier
        ) {
            if (data != null) {
                items(data) { reviews ->
                    ItemReview(
                        image = reviews.userImage,
                        name = reviews.userName,
                        review = reviews.userReview,
                        rating = reviews.userRating
                    )
                }
            }
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ReviewCompose.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ReviewCompose().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
