package com.hafidmust.ecommerce.ui.main.store

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.hafidmust.ecommerce.core.source.network.response.ItemsItem
import com.hafidmust.ecommerce.data.repository.AuthRepository
import com.hafidmust.ecommerce.data.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class StoreViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val mainRepository: MainRepository
) : ViewModel() {

    private val _filterState = MutableStateFlow(FilterItem())
    val filterState: StateFlow<FilterItem> = _filterState.asStateFlow()

    val product = filterState.flatMapLatest {
        mainRepository.getProducts(
            brand = it.brand,
            lowest = it.lower?.toIntOrNull(),
            highest = it.higher?.toIntOrNull(),
            sort = it.sorting,
            search = it.search
        ).asFlow().cachedIn(viewModelScope)
    }

    var gridLayout: Boolean = false
    fun updateFilter(
        idSorting: Int? = null,
        sorting: String? = null,
        idBrand: Int? = null,
        brand: String? = null,
        lower: String? = null,
        higher: String? = null,
        search: String? = null
    ) {
        _filterState.update { currentState ->
            currentState.copy(
                id_sorting = idSorting,
                sorting = sorting,
                id_brand = idBrand,
                brand = brand,
                lower = lower,
                higher = higher,
                search = search
            )
        }
    }

    fun resetFilter() {
        _filterState.update { currentState ->
            currentState.copy(
                id_sorting = null,
                sorting = null,
                id_brand = null,
                brand = null,
                lower = null,
                higher = null,
                search = null
            )
        }
    }
}
