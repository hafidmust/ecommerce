package com.hafidmust.ecommerce.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hafidmust.ecommerce.data.repository.AuthRepository
import com.hafidmust.ecommerce.data.repository.CartRepository
import com.hafidmust.ecommerce.data.repository.WishlistRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val cartRepository: CartRepository,
    private val wishlistRepository: WishlistRepository
) : ViewModel() {
    fun getToken() = authRepository.getRefreshToken()

    fun getName() = authRepository.getNameProfile()

    fun getUri() = authRepository.getUriProfile()
    fun getQuantity() = cartRepository.getQuantity()

    suspend fun getQuantityWishlist() = wishlistRepository.getQuantity()

    fun deletePref() {
        viewModelScope.launch {
            authRepository.deletePref()
            authRepository.deleteAll()
        }
    }
}
