package com.hafidmust.ecommerce.ui.main.cart

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hafidmust.ecommerce.data.repository.CartRepository
import com.hafidmust.ecommerce.core.source.local.entity.CartItemEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(private val cartRepository: CartRepository) : ViewModel() {

    private val _carts = MutableStateFlow(listOf<CartItemEntity>())
    val carts: Flow<List<CartItemEntity>> get() = _carts

    init {
        viewModelScope.launch {
            cartRepository.getCart().collect {
                _carts.value = it
            }
        }
    }

    fun deleteItemCart(item: CartItemEntity) {
        viewModelScope.launch {
            cartRepository.deleteItemCart(item)
        }
    }

    fun deleteAll(item: List<CartItemEntity>) {
        viewModelScope.launch {
            cartRepository.deleteAll(item)
        }
    }

    fun updateQuantity(item: CartItemEntity, newQuantity: Int) {
        viewModelScope.launch {
            cartRepository.updateQuantity(item.productId, newQuantity)
        }
    }

    fun updateCheckState(checked: Boolean, productId: String) {
        viewModelScope.launch {
            cartRepository.updateCheckCart(checked, productId)
        }
    }

    fun update(cartItemEntity: List<CartItemEntity>) {
        viewModelScope.launch {
            cartRepository.update(cartItemEntity)
        }
    }

    fun calculateTotals(cart: List<CartItemEntity>): Int {
        return cart.filter { it.isChecked }
            .sumOf { it.quantity * it.productPrice }
    }

    fun deleteAllCart() {
        viewModelScope.launch(Dispatchers.IO) {
            cartRepository.deleteAllCart()
        }
    }
}
