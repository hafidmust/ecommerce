package com.hafidmust.ecommerce.ui.main.notification

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.hafidmust.ecommerce.core.source.local.entity.NotificationEntity
import com.hafidmust.ecommerce.ui.ui.theme.poppins

@Composable
fun NotificationItem(
    data: NotificationEntity,
    onItemRead: (NotificationEntity) -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(
                if (!data.isRead) Color(0XFFEADDFF) else MaterialTheme.colorScheme.surface
            )
            .clickable {
                onItemRead(data)
            }
            .padding(top = 8.dp)

    ) {
        AsyncImage(
            modifier = Modifier
                .padding(start = 16.dp)
                .clip(RoundedCornerShape(4.dp))
                .size(36.dp),
            model = data.image,
            contentDescription = null
        )
        Column(
            modifier = Modifier.padding(start = 8.dp, end = 16.dp)
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = "Info",
                    style = TextStyle(
                        fontSize = 12.sp,
                        lineHeight = 16.sp,
                        fontFamily = poppins,
                        fontWeight = FontWeight.Normal,
                        color = Color(0xFF49454F),
                    )
                )
                Text(
                    text = "${data.date} ${data.time}",
                    style = TextStyle(
                        fontSize = 12.sp,
                        lineHeight = 16.sp,
                        fontFamily = poppins,
                        fontWeight = FontWeight(400),
                        color = Color(0xFF49454F),
                    )
                )
            }
            Text(
                modifier = Modifier.padding(top = 4.dp),
                text = data.title,
                style = TextStyle(
                    fontSize = 14.sp,
                    lineHeight = 20.sp,
                    fontFamily = poppins,
                    fontWeight = FontWeight.SemiBold,
                    color = Color(0xFF49454F),
                    letterSpacing = 0.25.sp,
                )
            )
            Text(
                text = data.body,
                style = TextStyle(
                    fontSize = 12.sp,
                    lineHeight = 16.sp,
                    fontFamily = poppins,
                    fontWeight = FontWeight.Normal,
                    color = Color(0xFF49454F),
                ),
            )

            Divider(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 12.dp),
                thickness = 1.dp
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun NotificationItemPrev() {
    NotificationItem(
        NotificationEntity(
            title = "Telkomsel",
            body = "Selamat anda mendapatkan kuota 10GB",
            image = "https://firebasestorage.googleapis.com/v0/b/ecommerce-3d0e9.appspot.com/o/ecommerce%2Fimages%2F1629785055.0.jpg?alt=media&token=3b9b9b1a-9b0a-4b0a-9b0a-4b0a9b0a9b0a",
            date = "2021-08-24",
            time = "12:00",
            type = "promo",

        ),
        onItemRead = {}
    )
}
