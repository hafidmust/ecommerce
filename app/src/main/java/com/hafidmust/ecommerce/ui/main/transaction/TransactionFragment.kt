package com.hafidmust.ecommerce.ui.main.transaction

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.hafidmust.ecommerce.MainActivity
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.databinding.FragmentTransactionBinding
import com.hafidmust.ecommerce.ui.adapter.TransactionAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TransactionFragment : Fragment() {

    private var _binding: FragmentTransactionBinding? = null
    private val binding get() = _binding!!

    private val viewModel: TransactionViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentTransactionBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = TransactionAdapter(
            onReviewClick = {
//                val bundle = bundleOf("invoiceId" to it)
//                findNavController().navigate(R.id.transaction_to_rating,bundle)
                (requireActivity() as MainActivity).toRating(
                    it.invoiceId,
                    it.status,
                    it.date,
                    it.time,
                    it.payment,
                    it.total,
                    ""
                )
            }
        )

        viewModel.transaction().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Error -> {
                    binding.linearEmpty.visibility = View.VISIBLE
                    binding.progressBar.isVisible = false
                }
                is Result.Loading -> {
                    binding.progressBar.isVisible = true
                }
                is Result.Success -> {
                    binding.progressBar.isVisible = false
                    if (it.data != null) {
                        adapter.submitList(it.data)
                        binding.rvTransaction.adapter = adapter
                    }
                }
            }
        }

        binding.rvTransaction.layoutManager = LinearLayoutManager(context)

        findNavController().currentBackStack.value.forEach {
            Log.d("d_n", it.destination.displayName)
        }
    }
}
