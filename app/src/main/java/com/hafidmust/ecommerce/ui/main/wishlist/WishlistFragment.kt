package com.hafidmust.ecommerce.ui.main.wishlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.hafidmust.ecommerce.core.source.local.entity.toCartItemEntity
import com.hafidmust.ecommerce.databinding.FragmentWishlistBinding
import com.hafidmust.ecommerce.core.di.EventLogger
import com.hafidmust.ecommerce.ui.adapter.WishlistAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [WishlistFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class WishlistFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private val viewModel: WishlistViewModel by viewModels()

    private var _binding: FragmentWishlistBinding? = null
    private val binding get() = _binding!!

    @Inject lateinit var eventLogger: EventLogger

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentWishlistBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = WishlistAdapter(
            onDeleteClick = {
                eventLogger.logButtonClick(bundleOf("btn_delete" to "delete"))
                viewModel.deleteWishlist(it)
            },
            onCartClick = {
                eventLogger.logButtonClick(bundleOf("btn_add_to_cart" to "add_to_cart"))
                eventLogger.logAddToCart(bundleOf("data" to it.productId))
                viewLifecycleOwner.lifecycleScope.launch {
                    viewModel.insertCart(it.toCartItemEntity())
                }
                Snackbar.make(binding.root, "Berhasil menambahkan keranjang", Snackbar.LENGTH_SHORT).show()
            }
        )

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.wishlist.collect { data ->
                // where is checked true
                adapter.submitList(data)
                binding.tvQty.text = "${data.size} barang"

                if (data.isEmpty()) {
                    binding.apply {
                        linearEmpty.isVisible = true
                        btnChangeLayout.isVisible = false
                        tvQty.isVisible = false
                    }
                } else {
                    binding.linearEmpty.isVisible = false
                }
            }
        }

        val layoutManager = GridLayoutManager(context, 1)
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position == adapter.itemCount && adapter.currentList.isNotEmpty() && adapter.gridLayout) 2 else 1
            }
        }

        binding.rvWishlist.layoutManager = layoutManager

        binding.btnChangeLayout.setOnCheckedChangeListener { _, isChecked ->
            eventLogger.logButtonClick(bundleOf("btn_change_layout" to "change_layout"))
            layoutManager.spanCount = if (isChecked) 2 else 1
            adapter.gridLayout = isChecked
        }

        binding.rvWishlist.adapter = adapter
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment WishlistFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            WishlistFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
