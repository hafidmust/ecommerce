package com.hafidmust.ecommerce.ui.prelogin.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.hafidmust.ecommerce.R
import com.hafidmust.ecommerce.databinding.FragmentOnboardingBinding
import com.hafidmust.ecommerce.ui.prelogin.onboarding.data.OnboardingItem
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

@AndroidEntryPoint
class OnboardingFragment : Fragment() {

    private var _binding: FragmentOnboardingBinding? = null
    private val binding get() = _binding!!

    private val viewModel: OnboardingViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch {
            val state = viewModel.getFirstInstall().first()
            if (!state) {
                findNavController().navigate(R.id.action_onboardingFragment_to_loginFragment)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentOnboardingBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data = listOf(
            OnboardingItem(1, R.drawable.onboarding_1),
            OnboardingItem(2, R.drawable.onboarding_2),
            OnboardingItem(3, R.drawable.onboarding_3),
        )

        val adapter = OnboardingAdapter(data)
        binding.viewpagerOnboarding.adapter = adapter
        binding.dotsIndicator.attachTo(binding.viewpagerOnboarding)

        binding.btnSkip.setOnClickListener {
            lifecycleScope.launch {
                viewModel.setOnboardingState(false)
            }
            findNavController().navigate(R.id.action_onboardingFragment_to_loginFragment)
        }
        binding.btnJoin.setOnClickListener {
            lifecycleScope.launch {
                viewModel.setOnboardingState(false)
            }
            findNavController().navigate(R.id.action_onboardingFragment_to_registerFragment)
        }
        binding.btnNext.setOnClickListener {
            val currentPosition = binding.viewpagerOnboarding.currentItem
            if (currentPosition < data.size - 1) {
                binding.viewpagerOnboarding.setCurrentItem(currentPosition + 1, true)
            } else {
            }
        }

        binding.viewpagerOnboarding.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (position == data.size - 1) {
                    binding.btnNext.visibility = View.INVISIBLE
                } else {
                    binding.btnNext.visibility = View.VISIBLE
                }
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
