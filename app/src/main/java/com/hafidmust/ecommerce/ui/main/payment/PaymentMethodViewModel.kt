package com.hafidmust.ecommerce.ui.main.payment

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ConfigUpdate
import com.google.firebase.remoteconfig.ConfigUpdateListener
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigException
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import com.hafidmust.ecommerce.R
import com.hafidmust.ecommerce.data.repository.TransactionRepository
import com.hafidmust.ecommerce.core.source.network.request.TransactionRequest
import com.hafidmust.ecommerce.core.source.network.response.DataItem
import com.hafidmust.ecommerce.core.source.network.response.DetailPayment
import com.squareup.moshi.Moshi
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PaymentMethodViewModel @Inject constructor(
    private val repository: TransactionRepository
) : ViewModel() {
    fun getPaymentMethod() = repository.paymentMethod()

    private val _listPayment = MutableLiveData<List<DataItem>?>()
    val listPayment: MutableLiveData<List<DataItem>?> = _listPayment

    init {
        loadPaymentMethod()
    }

    fun postFulfillment(data: TransactionRequest) = repository.fulfillment(data)

    fun loadPaymentMethod() {
        val remoteConfig: FirebaseRemoteConfig = Firebase.remoteConfig
        val configSettings = remoteConfigSettings {
            minimumFetchIntervalInSeconds = 3600
        }
        remoteConfig.setConfigSettingsAsync(configSettings)
        remoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)

        remoteConfig.fetchAndActivate()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val moshi = Moshi.Builder().build()
                    val adapter = moshi.adapter(DetailPayment::class.java)
//                    val paymentMethod = remoteConfig.getString("data")
//                    Log.d("paymentMethod", paymentMethod)
                    val response = adapter.fromJson(remoteConfig.getString("payment_method"))?.data
                    _listPayment.postValue(response)
                }
            }

        remoteConfig.addOnConfigUpdateListener(object : ConfigUpdateListener {
            override fun onUpdate(configUpdate: ConfigUpdate) {
                if (configUpdate.updatedKeys.contains("payment_method")) {
                    remoteConfig.activate().addOnCompleteListener {
                        if (it.isSuccessful) {
                            val moshi = Moshi.Builder().build()
                            val adapter = moshi.adapter(DetailPayment::class.java)
                            val response = adapter.fromJson(remoteConfig.getString("payment_method"))?.data
                            _listPayment.postValue(response)
                        }
                    }
                }
            }

            override fun onError(error: FirebaseRemoteConfigException) {
                Log.w("listener update", "Config update error with code: " + error.code, error)
            }
        })
    }
}
