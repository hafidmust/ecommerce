package com.hafidmust.ecommerce.ui.main.cart

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.hafidmust.ecommerce.core.source.local.entity.CartItemEntity
import com.hafidmust.ecommerce.databinding.FragmentCartBinding
import com.hafidmust.ecommerce.core.di.EventLogger
import com.hafidmust.ecommerce.ui.adapter.CartAdapter
import com.hafidmust.ecommerce.utils.rupiahFormatter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CartFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class CartFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var _binding: FragmentCartBinding? = null
    private val binding get() = _binding!!

    private val viewModel: CartViewModel by viewModels()

    @Inject lateinit var eventLogger: EventLogger

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentCartBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        eventLogger.logViewCart(bundleOf("view_cart" to "view_cart"))

        binding.topAppBar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        val adapterCart = CartAdapter(
            onDeleteItem = {
//                Toast.makeText(context, it.productName, Toast.LENGTH_SHORT).show()
                eventLogger.logRemoveCart(bundleOf("product_name" to it.productName))
                viewModel.deleteItemCart(it)
            },
            onAddQuantity = {
                if (it.quantity < it.stock) {
                    viewModel.updateQuantity(it, it.quantity + 1)
                }
            },
            onSubtractQuantity = {
                if (it.quantity > 1) {
                    viewModel.updateQuantity(it, it.quantity - 1)
                }
            },
            onCheckedItem = { data, isChecked ->
                if (isChecked) {
                    viewModel.updateCheckState(true, data)
//                    Log.d("ischecked", data.isChecked.toString())
                } else {
                    viewModel.updateCheckState(false, data)
//                    Log.d("ischecked", data.isChecked.toString())
                }
            }
        )
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.carts.collectLatest { data ->
                Log.d("cart6",data.toString())
//                hideUI(data)
                binding.linearError.isVisible = false
                adapterCart.submitList(data)
                val totalBayar = viewModel.calculateTotals(data)
                updateHarga(totalBayar)

                binding.btnDeleteAll.setOnClickListener {
                    val dataToDelete = data.filter { it.isChecked }
                    viewModel.deleteAll(dataToDelete)
                }

                val isCheckboxChecked = data.all { it.isChecked }


                if (data.isNotEmpty()) {
                    binding.checkbox.isChecked = isCheckboxChecked
                } else {
                    binding.checkbox.isChecked = !isCheckboxChecked
                }

                binding.btnDeleteAll.isVisible = data.any { it.isChecked }

                binding.btnBeli.setOnClickListener {
                    val dataToCheckout = data.filter { it.isChecked }.toTypedArray()
                    val toCheckout = CartFragmentDirections.actionCartFragmentToCheckoutFragment(dataToCheckout)
                    toCheckout.item = dataToCheckout
                    findNavController().navigate(toCheckout)
                }

                binding.btnBeli.isEnabled = data.any { it.isChecked }

            }
        }
        binding.rvCart.apply {
            adapter = adapterCart
            itemAnimator?.changeDuration = 0
        }

        binding.checkbox.setOnClickListener {
            if (binding.checkbox.isChecked){
                binding.btnBeli.isEnabled = true
                binding.btnDeleteAll.isVisible = true
            }else{
                binding.btnDeleteAll.isVisible = false
                binding.btnBeli.isEnabled = false
            }

            val data = adapterCart.currentList.map {
                it.isChecked = binding.checkbox.isChecked
                it
            }
            Log.d("ischecked", data.toString())
            viewModel.update(data)
            adapterCart.notifyDataSetChanged()
        }
    }

    private fun hideUI(data: List<CartItemEntity>) {
        if (data.isEmpty()) {
            binding.apply {
                tvTotalPrice.visibility = View.GONE
                btnDeleteAll.visibility = View.GONE
                checkbox.visibility = View.GONE
                tvTotalPrice.visibility = View.GONE
                labelChooseAll.visibility = View.GONE
                line.visibility = View.GONE
                line1.visibility = View.GONE
                btnBeli.visibility = View.GONE
                linearError.visibility = View.VISIBLE
            }
        }
    }

    private fun updateHarga(bayar: Int) {
        binding.tvTotalPrice.text = rupiahFormatter(bayar)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CartFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CartFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
