package com.hafidmust.ecommerce.ui.main.wishlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hafidmust.ecommerce.data.repository.CartRepository
import com.hafidmust.ecommerce.data.repository.WishlistRepository
import com.hafidmust.ecommerce.core.source.local.entity.CartItemEntity
import com.hafidmust.ecommerce.core.source.local.entity.DataWishListEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WishlistViewModel @Inject constructor(
    private val repository: WishlistRepository,
    private val cartRepository: CartRepository
) :
    ViewModel() {

    val wishlist = repository.getWishlist()

    fun deleteWishlist(data: DataWishListEntity) {
        viewModelScope.launch {
            repository.deleteWishlist(data)
        }
    }

    fun getIsChecked(productId: String) = repository.getIsChecked(productId)

    suspend fun insertCart(product: CartItemEntity) {
        val existItem = cartRepository.getItemById(product.productId).first()
        if (existItem != null) {
            val newQty = existItem.quantity + product.quantity
            cartRepository.updateQuantity(product.productId, newQty)
        } else {
            cartRepository.insertCart(product)
        }
    }

    fun deleteAllWishlist() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteAllWishlist()
        }
    }
}
