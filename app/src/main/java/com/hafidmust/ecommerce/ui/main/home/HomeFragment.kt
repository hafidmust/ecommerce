package com.hafidmust.ecommerce.ui.main.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.os.LocaleListCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.badge.ExperimentalBadgeUtils
import com.hafidmust.ecommerce.MainActivity
import com.hafidmust.ecommerce.databinding.FragmentHomeBinding
import com.hafidmust.ecommerce.core.di.EventLogger
import com.hafidmust.ecommerce.ui.main.cart.CartViewModel
import com.hafidmust.ecommerce.ui.main.notification.NotificationViewModel
import com.hafidmust.ecommerce.ui.main.wishlist.WishlistViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@ExperimentalBadgeUtils
@AndroidEntryPoint
class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private val viewModel: HomeViewModel by viewModels()
    private val notificationViewModel : NotificationViewModel by viewModels()
    private val cartViewModel : CartViewModel by viewModels()
    private val wishlistViewModel : WishlistViewModel by viewModels()

    @Inject lateinit var eventLogger: EventLogger

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.btnLogout.setOnClickListener {
            notificationViewModel.deleteAllNotification()
            cartViewModel.deleteAllCart()
            wishlistViewModel.deleteAllWishlist()
            eventLogger.logButtonClick(bundleOf("btn_logout" to "logout"))
            viewModel.logout()
            (requireActivity() as MainActivity).logout()
        }

        val appLocale: LocaleListCompat = LocaleListCompat.forLanguageTags("in")
        if (AppCompatDelegate.getApplicationLocales().toLanguageTags() == appLocale.toLanguageTags()) {
            binding.switchLanguage.isChecked = true
        }
        binding.switchLanguage.setOnClickListener {
            Log.d("language", binding.switchLanguage.isChecked.toString())
            eventLogger.logButtonClick(bundleOf("btn_language" to "language"))
            if (binding.switchLanguage.isChecked) {
                val locale = LocaleListCompat.forLanguageTags("in")
                AppCompatDelegate.setApplicationLocales(locale)
            } else {
                val locale = LocaleListCompat.forLanguageTags("en")
                AppCompatDelegate.setApplicationLocales(locale)
            }
        }
//        binding.switchLanguage.setOnCheckedChangeListener { _, isChecked ->
//            eventLogger.logButtonClick(bundleOf("btn_language" to "language"))
//            if (isChecked) {
//                AppCompatDelegate.setApplicationLocales(appLocale)
//                requireActivity().recreate()
//            } else {
//                AppCompatDelegate.setApplicationLocales(LocaleListCompat.getDefault())
//            }
//        }

        lifecycleScope.launch {
            val getTheme = viewModel.getTheme().first()
//            if (getTheme) AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES) else AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            binding.switchTheme.isChecked = getTheme
        }

        binding.switchTheme.setOnCheckedChangeListener { _, isChecked ->
            eventLogger.logButtonClick(bundleOf("btn_theme" to "theme"))
            if (isChecked) {
                viewModel.setTheme(true)
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                viewModel.setTheme(false)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
