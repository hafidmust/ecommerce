package com.hafidmust.ecommerce.ui.prelogin.login

import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.hafidmust.ecommerce.R
import com.hafidmust.ecommerce.core.source.network.response.LoginResponse
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.databinding.FragmentLoginBinding
import com.hafidmust.ecommerce.core.di.EventLogger
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment : Fragment() {

    private val viewModel: LoginViewModel by viewModels()

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private val emailStateFlow = MutableStateFlow("")
    private val passwordStateFlow = MutableStateFlow("")

    @Inject
    lateinit var eventLogger: EventLogger

//    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Obtain the FirebaseAnalytics instance.
//        firebaseAnalytics = Firebase.analytics
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentLoginBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycleScope.launch {
//            val stateOnboarding = viewModel.getOnboardingState().first()
//            if (stateOnboarding) {
//                findNavController().navigate(R.id.action_loginFragment_to_onboardingFragment)
//            }
        }

        val validateForm: Flow<Boolean> =
            combine(emailStateFlow, passwordStateFlow) { email, password ->
                isValidEmail(email) && isValidPassword(password)
            }

        termAndPrivacyText()

        setupEmailValidation()
        setupPasswordValidation()

        binding.btnRegister.setOnClickListener {
            eventLogger.logButtonClick(bundleOf("btn_register" to "register"))
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
        binding.btnLogin.setOnClickListener {
            val email = binding.outlineEmail.editText?.text.toString().trim()
            val password = binding.outlinePassword.editText?.text.toString().trim()
            val bundle = Bundle().apply {
                putString("email", email)
            }
            eventLogger.logEvent("login", bundle)
            eventLogger.logButtonClick(bundleOf("btn_login" to "login"))

            viewModel.login(email, password).observe(viewLifecycleOwner, loginObserver)
        }

        viewLifecycleOwner.lifecycleScope.launch {
            validateForm.collect { isValid ->
                binding.btnLogin.isEnabled = isValid
            }
            emailStateFlow.collect { email ->
                Log.d("EmailStateFlow", "Email changed: $email")
            }
        }
    }

    private val loginObserver = Observer<Result<LoginResponse>> {
        when (it) {
            is Result.Success -> {
                binding.progressBar.isVisible = false
                Firebase.messaging.subscribeToTopic("promo")
                    .addOnCompleteListener {
                        Log.d("loginsubscribe", "onComplete: ${it.isComplete}")
                    }
                findNavController().navigate(R.id.pre_login_to_main)
            }
            is Result.Error -> {
                binding.progressBar.isVisible = false
                Toast.makeText(context, it.msg, Toast.LENGTH_SHORT).show() }

            Result.Loading -> {
//                binding.progressBar.visibility = View.VISIBLE
//                context?.let { dialog ->
//                    MaterialAlertDialogBuilder(dialog)
//                        .setView(R.layout.view_dialog)
//                        .show()
//                }

                binding.progressBar.visibility = View.VISIBLE
            }
        }
    }

    private fun setupPasswordValidation() {
        binding.outlinePassword.editText?.doOnTextChanged { text, _, _, count ->
            passwordStateFlow.value = text.toString()
            Log.d("TAG", count.toString())
            if (isValidPassword(text) || text?.length == 0) {
                binding.outlinePassword.error = null
            } else {
                binding.outlinePassword.error = getString(R.string.password_not_valid)
            }
        }
    }

    private fun isValidPassword(text: CharSequence?): Boolean {
        return (text?.length ?: 0) >= 8
    }

    private fun setupEmailValidation() {
        binding.outlineEmail.editText?.doOnTextChanged { text, _, _, _ ->
            emailStateFlow.value = text.toString()
            if (isValidEmail(text) || text?.length == 0) {
                binding.outlineEmail.error = null
            } else {
                binding.outlineEmail.error = getString(R.string.email_not_valid)
                binding.outlineEmail.errorIconDrawable = null
            }
        }
    }

    private fun isValidEmail(text: CharSequence?): Boolean {
        return if (!text.isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(text).matches()) {
            return true
        } else {
            false
        }
    }

    private fun termAndPrivacyText() {
        val text =
            "Dengan masuk disini, kamu menyetujui Syarat & Ketentuan serta Kebijakan Privasi TokoPhincon."
        val spannableString = SpannableString(text)
        val color =
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.light_primary))
        val colorPrimary =
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.light_primary))
        spannableString.setSpan(
            color,
            text.indexOf("Syarat & Ketentuan"),
            text.indexOf("Syarat & Ketentuan") + "Syarat & Ketentuan".length,
            0
        )
        spannableString.setSpan(
            colorPrimary,
            text.indexOf("Kebijakan Privasi"),
            text.indexOf("Kebijakan Privasi") + "Kebijakan Privasi".length,
            0
        )
        binding.tvTermService.text = spannableString
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
