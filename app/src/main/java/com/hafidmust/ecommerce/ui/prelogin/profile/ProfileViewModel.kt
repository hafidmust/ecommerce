package com.hafidmust.ecommerce.ui.prelogin.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hafidmust.ecommerce.data.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val authRepository: AuthRepository
) : ViewModel() {

    suspend fun uploadProfile(image: MultipartBody.Part?, username: MultipartBody.Part) =
        authRepository.profile(
            token = authRepository.getAccessToken().first(),
            image = image,
            username = username
        )

    fun setProfile(uri: String, name: String) {
        viewModelScope.launch {
            authRepository.setProfile(uri, name)
        }
    }
}
