package com.hafidmust.ecommerce.ui.prelogin.profile

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.hafidmust.ecommerce.R
import com.hafidmust.ecommerce.databinding.FragmentProfileBinding
import com.hafidmust.ecommerce.core.di.EventLogger
import com.hafidmust.ecommerce.utils.createCustomTempFile
import com.hafidmust.ecommerce.utils.uriToFile
import com.hafidmust.ecommerce.core.source.network.Result
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFragment : Fragment() {

    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!
    private var selectedImageUri: Uri? = null
    private val viewModel: ProfileViewModel by viewModels()
    private lateinit var currentPhotoPath: String
    private var getFile: File? = null

    @Inject lateinit var eventLogger: EventLogger

    private val testUri: Uri =
        Uri.parse("content://com.android.providers.media.documents/document/image%3A96418")

    private val cameraPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                takePhoto()
            }
        }

    private val galleryLauncher =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri ->
            uri?.let { selectedUri ->
                selectedImageUri = selectedUri
//            binding.ivImagePicker.setImageURI(selectedImageUri)
                context?.let {
                    Glide.with(it)
                        .load(selectedImageUri)
                        .into(binding.ivImagePicker)
                }
                val file = uriToFile(selectedUri, requireContext())
                getFile = file
                binding.icProfile.visibility = View.INVISIBLE
                Log.d("URI", uri.toString())
            }
        }

    private val cameraLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == RESULT_OK) {
                val myFile = File(currentPhotoPath)
                myFile.let { file ->
                    getFile = file
                    Glide.with(requireActivity())
                        .load(file)
                        .into(binding.ivImagePicker)
                    binding.icProfile.visibility = View.INVISIBLE
                }
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentProfileBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setButtonEnable()
        binding.ivImagePicker.setOnClickListener {
            eventLogger.logButtonClick(bundleOf("btn_image_picker" to "image_picker"))
            val options = arrayOf("Kamera", "Galeri")

            MaterialAlertDialogBuilder(requireContext())
                .setTitle(getString(R.string.choose_gambar))
                .setItems(options) { _, int ->
                    when (int) {
                        0 -> {
//                            Toast.makeText(context, "Coming soon", Toast.LENGTH_SHORT).show()
                            requestPermissionCamera()
//                            takePhoto()
                        }

                        1 -> {
                            // galeri
                            chooseImageFromGallery()
                        }
                    }
                }
                .show()
        }

        binding.outlineName.editText?.doOnTextChanged { text, _, _, _ ->
            setButtonEnable()
        }

        binding.btnFinish.setOnClickListener {
            eventLogger.logButtonClick(bundleOf("btn_finish" to "finish"))
            val name = binding.outlineName.editText?.text.toString().trim()
            uploadImage(name)
        }
        termAndPrivacyText()
    }

    private fun uploadImage(name: String) {
        lifecycleScope.launch {
//                val file = getFile?.let { reduceFileImage(it) }
            val requestImage = getFile?.asRequestBody("image/jpeg".toMediaType())
            val imageMultipart: MultipartBody.Part? =
                requestImage?.let {
                    MultipartBody.Part.createFormData(
                        "userImage",
                        getFile?.name,
                        it
                    )
                }
            val usernameMultipart: MultipartBody.Part = MultipartBody.Part.createFormData(
                "userName",
                name
            )
            viewModel.uploadProfile(image = imageMultipart, username = usernameMultipart).observe(viewLifecycleOwner) {
                when (it) {
                    is Result.Error -> {
                        Toast.makeText(requireActivity(), it.msg, Toast.LENGTH_SHORT).show()
                        binding.progressBar.isVisible = false
                    }
                    is Result.Success -> {
                        findNavController().navigate(R.id.action_profileFragment_to_main_navigation)
                    }

                    Result.Loading -> {
                        binding.progressBar.visibility = View.VISIBLE
                    }
                }
            }

            // set profile to datastore
//            viewModel.setProfile(getFile?.toURI().toString(), name)
        }
    }

    private fun requestPermissionCamera() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            takePhoto()
        } else {
            cameraPermissionLauncher.launch(Manifest.permission.CAMERA)
        }
    }

    private fun takePhoto() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.resolveActivity(requireActivity().packageManager)
        createCustomTempFile(requireContext()).also {
            val photoUri: Uri =
                FileProvider.getUriForFile(requireActivity(), "com.hafidmust.ecommerce", it)
            currentPhotoPath = it.absolutePath
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
            cameraLauncher.launch(intent)
        }
    }

    private fun chooseImageFromGallery() {
        galleryLauncher.launch("image/*")
    }

    private fun termAndPrivacyText() {
        val text =
            "Dengan daftar disini, kamu menyetujui Syarat & Ketentuan serta Kebijakan Privasi TokoPhincon."
        val spannableString = SpannableString(text)
        val color =
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.light_primary))
        val colorPrimary =
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.light_primary))
        spannableString.setSpan(
            color,
            text.indexOf("Syarat & Ketentuan"),
            text.indexOf("Syarat & Ketentuan") + "Syarat & Ketentuan".length,
            0
        )
        spannableString.setSpan(
            colorPrimary,
            text.indexOf("Kebijakan Privasi"),
            text.indexOf("Kebijakan Privasi") + "Kebijakan Privasi".length,
            0
        )
        binding.tvTermService.text = spannableString
    }

    private fun setButtonEnable(isEnable: Boolean = true) {
        binding.apply {
            val email = outlineName.editText?.text
            btnFinish.isEnabled = !email.isNullOrEmpty() && isEnable
        }
    }
}
