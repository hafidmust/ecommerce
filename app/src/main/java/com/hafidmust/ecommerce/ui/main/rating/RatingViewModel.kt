package com.hafidmust.ecommerce.ui.main.rating

import androidx.lifecycle.ViewModel
import com.hafidmust.ecommerce.data.repository.TransactionRepository
import com.hafidmust.ecommerce.core.source.network.request.RatingRequest
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RatingViewModel @Inject constructor(private val transactionRepository: TransactionRepository) : ViewModel() {

    fun postRating(data: RatingRequest) = transactionRepository.rating(data)
}
