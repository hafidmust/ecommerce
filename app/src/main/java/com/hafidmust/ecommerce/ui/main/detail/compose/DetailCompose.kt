package com.hafidmust.ecommerce.ui.main.detail.compose

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import coil.compose.AsyncImage
import com.hafidmust.ecommerce.R
import com.hafidmust.ecommerce.core.source.network.response.DetailProductResponse
import com.hafidmust.ecommerce.core.source.network.response.toCartEntity
import com.hafidmust.ecommerce.core.source.network.response.toWishlistEntity
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.databinding.DetailComposeBinding
import com.hafidmust.ecommerce.core.di.EventLogger
import com.hafidmust.ecommerce.ui.main.detail.DetailViewModel
import com.hafidmust.ecommerce.ui.ui.theme.EcommerceTheme
import com.hafidmust.ecommerce.ui.ui.theme.poppins
import com.hafidmust.ecommerce.utils.rupiahFormatter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class DetailCompose : Fragment() {

    private var _binding: DetailComposeBinding? = null
    private val binding get() = _binding!!

    private val viewModel: DetailViewModel by viewModels()

    @Inject lateinit var eventLogger: EventLogger

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = DetailComposeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val id = "1"
//        val id = DetailComposeArgs.fromBundle(arguments as Bundle).productId
//        eventLogger.logViewItem(bundleOf("id" to id))
//        val id = DetailFragmentArgs.fromBundle(arguments as Bundle).productId


        binding.composeView.apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                EcommerceTheme {
                    DetailProductScreen(viewModel, id, navigateToBuyNow = {
//                        val action = DetailComposeDirections.actionDetailFragmentToCheckoutFragment(
//                            arrayOf(it.toCartEntity())
//                        )
//                        findNavController().navigate(action)
                    },
                        onBackClicked = {
                            findNavController().navigateUp()
                        })
                }
            }
        }
    }


    @OptIn(ExperimentalMaterial3Api::class, ExperimentalFoundationApi::class)
    @Composable
    fun DetailProductScreen(
        viewModel: DetailViewModel,
        productId: String,
        navigateToBuyNow: (DetailProductResponse.Data) -> Unit,
        onBackClicked: () -> Unit
    ) {
        //library observeAs
        var sendData: DetailProductResponse.Data? = null

        val dataDetail by viewModel.detailProduct(productId).observeAsState()

        val wishlist by viewModel.wishlist.collectAsState(emptyList())

        val isFavorite = wishlist.any { it.productId == productId }

        val btnFavState by remember { mutableStateOf(false) }

        val addToFavorite: () -> Unit = {
            if (!isFavorite) {
                lifecycleScope.launch {
                    viewModel.insertWishlist(sendData!!.toWishlistEntity())
                }
            }
        }

        val removeFromFavorite: () -> Unit = {
            if (isFavorite) {
                val favoriteItem = wishlist.find { it.productId == productId }
                lifecycleScope.launch {
                    favoriteItem?.let { viewModel.deleteWishlist(it) }
                }
            }
        }

        val scope = rememberCoroutineScope()
        val snackbarHostState = remember { SnackbarHostState() }


        Scaffold(
            snackbarHost = {
                SnackbarHost(snackbarHostState)
            },
            topBar = {
                TopAppBar(
                    title = {
                        Text("Detail Produk")
                    },
                    navigationIcon = {
                        IconButton(onClick = {
                            onBackClicked()
                        }) {
                            Icon(
                                imageVector = Icons.Default.ArrowBack, contentDescription = null
                            )
                        }
                    })
            },
            content = {

                if (dataDetail != null) {
                    when (dataDetail) {
                        is Result.Error -> {

                        }

                        Result.Loading -> {
                            CircularProgressIndicator()
                        }

                        is Result.Success -> {
                            val data =
                                (dataDetail as Result.Success<DetailProductResponse.Data>).data



                            sendData = data

                            Column(
                                modifier = Modifier
                                    .padding(it)
                                    .fillMaxSize()
                                    .verticalScroll(rememberScrollState())
                            ) {
                                //viewpager
                                val state = rememberPagerState()
                                val scope = rememberCoroutineScope()
                                var btnFavState by remember { mutableStateOf(false) }

                                var selectedVariant by remember {
                                    mutableStateOf<DetailProductResponse.Data.ProductVariant?>(
                                        null
                                    )
                                }


                                HorizontalPager(
                                    pageCount = data.image.size,
                                    state = state
                                ) { page ->
                                    Box {
                                        AsyncImage(
                                            modifier = Modifier
                                                .fillMaxWidth()
                                                .height(309.dp),
                                            model = data.image[page],
                                            contentScale = ContentScale.Crop,
                                            contentDescription = null
                                        )
                                        Row(
                                            modifier = Modifier.fillMaxWidth().align(Alignment.BottomCenter),
                                            horizontalArrangement = Arrangement.Center
                                        ) {
                                            repeat(data.image.size) { iteration ->
                                                val color =
                                                    if (state.currentPage == iteration) Color.DarkGray else Color.LightGray
                                                Box(
                                                    modifier = Modifier
                                                        .padding(2.dp)
                                                        .clip(CircleShape)
                                                        .background(color)
                                                        .size(8.dp)

                                                )
                                            }
                                        }
                                    }


                                }




//        AsyncImage(
//            modifier = Modifier
//                .fillMaxWidth()
//                .height(309.dp),
//            model = "",
//            contentDescription = null
//        )

                                Row(
                                    modifier = Modifier.fillMaxWidth(),
                                    horizontalArrangement = Arrangement.SpaceBetween,
                                    verticalAlignment = Alignment.CenterVertically
                                ) {
                                    val newPriceSum =
                                        selectedVariant?.variantPrice?.plus(data.productPrice)
                                    Text(
                                        modifier = Modifier
                                            .weight(1f)
                                            .padding(start = 16.dp),
                                        text = rupiahFormatter(newPriceSum ?: data.productPrice),
                                        style = TextStyle(
                                            fontWeight = FontWeight.SemiBold,
                                            fontSize = 20.sp,
                                            fontFamily = poppins,
                                        )

                                    )
                                    IconButton(onClick = {
                                        val sendIntent = Intent().apply {
                                            action = Intent.ACTION_SEND
                                            putExtra(
                                                Intent.EXTRA_TEXT,
                                                "Coba cek ini di Ecommerce, deh. Harganya ${
                                                    rupiahFormatter(
                                                        data.productPrice
                                                    )
                                                } \n" +
                                                        "https://ecommerce.id/${data.productId}"
                                            )
                                            type = "text/plain"
                                        }

                                        val shareIntent = Intent.createChooser(sendIntent, null)
                                        startActivity(shareIntent)
                                    }, content = {
                                        Icon(
                                            painter = painterResource(id = R.drawable.ic_share),
                                            contentDescription = null
                                        )
                                    }) //share

                                    if (isFavorite) {
                                        Icon(
                                            modifier = Modifier
                                                .padding(end = 16.dp)
                                                .clickable {
                                                    removeFromFavorite()
                                                },
                                            imageVector = Icons.Default.Favorite,
                                            contentDescription = null
                                        )
                                    } else {
                                        Icon(
                                            modifier = Modifier
                                                .padding(end = 16.dp)
                                                .clickable {
                                                    eventLogger.logAddToWishlist(
                                                        bundleOf(
                                                            "wishlist" to data.productId
                                                        ))
                                                    addToFavorite()
                                                },
                                            imageVector = Icons.Default.FavoriteBorder,
                                            contentDescription = null
                                        )
                                    }


                                }

                                Text(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(horizontal = 16.dp),
                                    text = data.productName,
                                    style = TextStyle(
                                        fontSize = 14.sp,
                                        fontWeight = FontWeight.Normal,
                                        fontFamily = poppins,
                                    )
                                )

                                Row(
                                    modifier = Modifier.padding(bottom = 12.dp, top = 10.dp),
                                    verticalAlignment = Alignment.CenterVertically
                                ) {
                                    Text(
                                        modifier = Modifier
                                            .padding(horizontal = 16.dp),
                                        text = "Terjual ${data.sale}",
                                        style = TextStyle(
                                            fontSize = 12.sp,
                                            fontWeight = FontWeight.Normal,
                                            fontFamily = poppins,
                                        )
                                    )

                                    ChipRating(
                                        ratingProduct = data.productRating,
                                        totalRating = data.totalRating
                                    )
                                }

                                Divider(thickness = 1.dp)
                                Text(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(top = 16.dp)
                                        .padding(horizontal = 16.dp),
                                    text = "Pilih varian",
                                    style = TextStyle(
                                        fontSize = 16.sp,
                                        fontWeight = FontWeight.Medium,
                                        fontFamily = poppins,
                                    )
                                )
                                //chip group
//        FlowRow {
//            repeat(data.productVariant.size) {
//                InputChip(
//                    modifier = Modifier.padding(start = 16.dp),
//                    selected = false, onClick = {
//
//                    }, label = {
//                        Text(text = data.productVariant[it].variantName)
//                    })
//            }
//        }
                                ChipVariants(
                                    modifier = Modifier.padding(start = 16.dp),
                                    variants = data.productVariant, onVariantSelected = {
                                        selectedVariant = it
                                    })



                                Divider(
                                    modifier = Modifier.padding(top = 12.dp),
                                    thickness = 1.dp
                                )

                                Text(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(horizontal = 16.dp),
                                    text = "Deskripsi produk",
                                    style = TextStyle(
                                        fontSize = 16.sp,
                                        fontWeight = FontWeight.Medium,
                                        fontFamily = poppins,
                                    )
                                )
                                Text(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(horizontal = 16.dp),
                                    text = data.description,
                                    style = TextStyle(
                                        fontSize = 14.sp,
                                        fontWeight = FontWeight.Normal,
                                        fontFamily = poppins,
                                    )
                                )
                                Divider(
                                    modifier = Modifier.padding(top = 12.dp),
                                    thickness = 1.dp
                                )

                                Row(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(horizontal = 16.dp),
                                    horizontalArrangement = Arrangement.SpaceBetween,
                                    verticalAlignment = Alignment.CenterVertically
                                ) {
                                    Text(
                                        text = "Ulasan pembeli",
                                        style = TextStyle(
                                            fontSize = 16.sp,
                                            fontWeight = FontWeight.Medium,
                                            fontFamily = poppins,
                                        )
                                    )
                                    TextButton(onClick = {
//                                        val action =
//                                            DetailComposeDirections.actionDetailFragmentToReviewFragment(
//                                                productId
//                                            )
//                                        findNavController().navigate(action)
                                    }) {
                                        Text(
                                            text = "Lihat Semua",
                                            style = TextStyle(
                                                fontSize = 16.sp,
                                                fontWeight = FontWeight.Medium,
                                                fontFamily = poppins,
                                            )
                                        )
                                    }
                                }

                                Row(
                                    verticalAlignment = Alignment.CenterVertically
                                ) {
                                    Icon(
                                        modifier = Modifier.padding(start = 16.dp),
                                        painter = painterResource(id = R.drawable.ic_star_24),
                                        contentDescription = null
                                    )
                                    Text(
                                        text = "${data.productRating}", style = TextStyle(
                                            fontFamily = poppins,
                                            fontSize = 20.sp,
                                            fontWeight = FontWeight.SemiBold
                                        )
                                    )
                                    Text(
                                        text = "/5.0", style = TextStyle(
                                            fontSize = 10.sp,
                                            fontWeight = FontWeight.Normal,
                                            fontFamily = poppins,
                                        )
                                    )

                                    Column(modifier = Modifier.padding(start = 32.dp)) {
                                        Text(
                                            text = "${data.totalSatisfaction}% pembeli merasa puas",
                                            style = TextStyle(
                                                fontSize = 12.sp,
                                                fontWeight = FontWeight.SemiBold,
                                                fontFamily = poppins,
                                            )
                                        )
                                        Text(
                                            text = "${data.totalRating} rating · ${data.totalReview} ulasan",
                                            style = TextStyle(
                                                fontSize = 12.sp,
                                                fontWeight = FontWeight.Normal,
                                                fontFamily = poppins,
                                            )
                                        )
                                    }
                                }
                            }

//                            DetailProductContent(
//                                modifier = Modifier.padding(it),
//                                data = data,
//                                isFav = isFavorite,
//                                toggleUpdate = {
//                                    scope.launch {
//                                        if (btnFavState) {
//                                            viewModel.deleteWishlist(data.toWishlistEntity())
//                                        } else {
//                                            viewModel.insertWishlist(data.toWishlistEntity())
//                                        }
//                                    }
//
//                                },
//                                onReviewClick = {
//                                    val action =
//                                        DetailComposeDirections.actionDetailFragmentToReviewFragment(
//                                            it
//                                        )
//                                    findNavController().navigate(action)
//                                },
//
//                                onShareClick = {
//                                    val sendIntent = Intent().apply {
//                                        action = Intent.ACTION_SEND
//                                        putExtra(
//                                            Intent.EXTRA_TEXT,
//                                            "Coba cek ini di Ecommerce, deh. Harganya ${
//                                                rupiahFormatter(
//                                                    data.productPrice
//                                                )
//                                            } \n" +
//                                                    "https://ecommerce.id/${data.productId}"
//                                        )
//                                        type = "text/plain"
//                                    }
//
//                                    val shareIntent = Intent.createChooser(sendIntent, null)
//                                    startActivity(shareIntent)
//                                },
//                                onSaveWishlist = {
//                                    addToFavorite()
//
//                                },
//                                onDeleteWishlist = {
//                                    removeFromFavorite()
//                                }
//                            )
                        }

                        else -> {

                        }
                    }
                }

            },
            bottomBar = {
                Column(
                ) {
                    Divider(thickness = 1.dp)
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 8.dp, bottom = 8.dp)
                            .padding(horizontal = 16.dp)
                    ) {

                        OutlinedButton(
                            modifier = Modifier.weight(1f),
                            onClick = {
                                sendData?.let { navigateToBuyNow(it) }
                            }) {
                            Text(text = "Beli Langsung")
                        }
                        Spacer(modifier = Modifier.width(16.dp))
                        ElevatedButton(
                            modifier = Modifier.weight(1f),
                            onClick = {
                                scope.launch {
                                    val data =
                                        (dataDetail as Result.Success<DetailProductResponse.Data>).data.toCartEntity(
                                            ""
                                        )
                                    eventLogger.logAddToCart(bundleOf("cart" to data.productId))
                                    if (viewModel.isQuantityValid(productId, data.stock)) {
                                        viewModel.insertCart(data)
                                        snackbarHostState.showSnackbar("Berhasil menambahkan keranjang")
                                    } else {
//                                        viewModel.insertCart(data)
                                        snackbarHostState.showSnackbar("Stock habis")
                                    }
//                                    viewModel.insertCart(
//                                        data
//                                    )
                                }

                            },
                            colors = ButtonDefaults.elevatedButtonColors(
                                containerColor = MaterialTheme.colorScheme.primary,
                                contentColor = Color.White
                            )
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_add),
                                contentDescription = null
                            )
                            Text(text = "Keranjang")
                        }
                    }

                }
            }
        )
    }
}

//@Preview(showBackground = true)
//@Composable
//fun DetailContentPrev() {
//        DetailProductContent(
//            data = DetailProductResponse.Data(
//            productId = "1",
//            productName = "Lenovo Legion 7 16 I7 11800 16GB 1TB SSD RTX3070 8GB Windows 11 QHD IPS",
//            productPrice = 10000000,
//            image = listOf("https://ecs7.tokopedia.net/img/cache/700/product-1/2021/7/16/119744/119744_5e3b3b7a-7b1a-4b1a-9b0a-9b0a0a0a0a0a_700_700",""),
//            brand = "Lenovo",
//            description = "This is description",
//            store = "Lenovo Official Store",
//            sale = 100,
//            stock = 10,
//            totalRating = 10,
//            totalReview = 10,
//            totalSatisfaction = 10,
//            productRating = 4.5,
//            productVariant = listOf(DetailProductResponse.Data.ProductVariant( "Variant 1",  10000000))
//        ))
//}

@OptIn(
    ExperimentalFoundationApi::class, ExperimentalMaterial3Api::class,
    ExperimentalLayoutApi::class
)
@Composable
fun DetailProductContent(
    data: DetailProductResponse.Data,
    modifier: Modifier = Modifier,
    isFav: Boolean,
    toggleUpdate: () -> Unit,
    onSaveWishlist: () -> Unit,
    onDeleteWishlist: () -> Unit,
    onReviewClick: (String) -> Unit,
    onShareClick: () -> Unit
) {

}

@Composable
fun ChipRating(
    ratingProduct: Double,
    totalRating: Int
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .border(width = 1.dp, color = Color(0xFF79747E), shape = RoundedCornerShape(4.dp))
            .padding(start = 4.dp, end = 8.dp)
            .padding(vertical = 2.dp)
    ) {
        Image(
            modifier = Modifier.size(15.dp),
            painter = painterResource(id = R.drawable.ic_star_12), contentDescription = null
        )
        Text(
            text = "$ratingProduct ($totalRating)", style = TextStyle(
                color = MaterialTheme.colorScheme.onSurfaceVariant,
                fontSize = 12.sp,
                fontWeight = FontWeight.Normal,
                fontFamily = poppins,
            )
        )
    }
}

@Preview(showBackground = true)
@Composable
fun ChipRatingPrev() {
    ChipRating(4.5, 100)
}


