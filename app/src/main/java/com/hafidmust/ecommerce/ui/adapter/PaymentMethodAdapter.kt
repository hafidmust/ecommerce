package com.hafidmust.ecommerce.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.hafidmust.ecommerce.core.source.network.response.DataItem
import com.hafidmust.ecommerce.databinding.ItemHeaderPaymentBinding

class PaymentMethodAdapter(private val data: List<DataItem>) :
    ListAdapter<DataItem, ContentPaymentViewHolder>(CheckoutDiffUtils()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentPaymentViewHolder {
        val binding =
            ItemHeaderPaymentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ContentPaymentViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ContentPaymentViewHolder, position: Int) {
        holder.bindContent(getItem(position))
        val adapter = ChildMemberAdapter(data[position].item)
        holder.binding.rvChild.layoutManager = LinearLayoutManager(holder.itemView.context)
        holder.binding.rvChild.adapter = adapter
    }
}

class ContentPaymentViewHolder(val binding: ItemHeaderPaymentBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bindContent(data: DataItem) {
        binding.tvTypeTransfer.text = data.title
    }
}

class HeaderPaymentViewHolder(private val binding: ItemHeaderPaymentBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bindHeader(text: String) {
        binding.tvTypeTransfer.text = text
    }
}

class CheckoutDiffUtils : DiffUtil.ItemCallback<DataItem>() {
    override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem.title == newItem.title
    }

    override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem == newItem
    }
}
