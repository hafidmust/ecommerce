package com.hafidmust.ecommerce.ui.main.store

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.chip.Chip
import com.hafidmust.ecommerce.MainActivity
import com.hafidmust.ecommerce.databinding.FragmentStoreBinding
import com.hafidmust.ecommerce.core.di.EventLogger
import com.hafidmust.ecommerce.ui.adapter.LoadingStateAdapter
import com.hafidmust.ecommerce.ui.main.store.search.SearchFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

@AndroidEntryPoint
class StoreFragment : Fragment(), BottomSheetListener {

    private var _binding: FragmentStoreBinding? = null
    private val binding get() = _binding!!

    private val viewModel: StoreViewModel by viewModels()
    private var passData: FilterItem? = null
    private var query: String? = null

    @Inject
    lateinit var eventLogger: EventLogger

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentStoreBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = StoreAdapter(object : ClickListener {
            override fun onItemClick(idProduct: String) {
//                val navController = Navigation.findNavController(requireActivity(), R.id.nhf_main)
//                navController.navigate()
                val bundle = Bundle()
                bundle.putString("idProduct", idProduct)
                eventLogger.logSelectItem(bundle)
                (requireActivity() as MainActivity).detail(idProduct)
            }
        })
//        val gridLayoutManager = GridLayoutManager(context, 2)
        val footer = LoadingStateAdapter()
        binding.rvStore.adapter = adapter.withLoadStateFooter(
            footer = footer
        )

        binding.swipeRefresh.setOnRefreshListener {
            adapter.refresh()
            binding.swipeRefresh.isRefreshing = false
        }
        binding.btnRetry.setOnClickListener {
            adapter.refresh()
            binding.linearError.isVisible = false
        }
        binding.btnResetConnection.setOnClickListener {
            adapter.refresh()
            binding.linearErrorConnection.isVisible = false
        }
        viewLifecycleOwner.lifecycleScope.launch {
            adapter.loadStateFlow.collectLatest {
                val stateLoading = it.refresh is LoadState.Loading
                Log.d("stateLoading", stateLoading.toString())


                //show loading
                binding.shimmerGrid.isVisible = stateLoading && viewModel.gridLayout
                binding.shimmerLinear.isVisible = stateLoading && !viewModel.gridLayout

                if (it.refresh is LoadState.Error) {
                    val error = it.refresh as LoadState.Error
                    val e = error.error
                    if (e is HttpException) {
                        if (e.code() == 404) {
                            binding.linearEmpty.isVisible = true
                            binding.rvStore.isVisible = false
                            binding.linearError.isVisible = false
                        }else{
                            binding.linearError.isVisible = true
                            binding.rvStore.isVisible = false
                            binding.linearEmpty.isVisible = false
                        }
                    }else if (e is IOException){
                        binding.linearErrorConnection.isVisible = true
                        binding.rvStore.isVisible = false
                        binding.linearEmpty.isVisible = false
                    }
                }

//                binding.linearError.isVisible = it.refresh is LoadState.Error
                binding.rvStore.isVisible = it.refresh is LoadState.NotLoading
//                binding.shimmerGrid.isVisible = it.refresh is LoadState.Loading

            }
        }


        lifecycleScope.launch {
            viewModel.product.collect {
                adapter.submitData(lifecycle, it)
            }
        }

        setFragmentResultListener("queryKey") { requestKey, bundle ->
            val result = bundle.getString("bundleKey")
            query = result
            binding.tilSearch.editText?.setText(result)
//            viewModel.getStore(search = result, brand = null, lowest = null, highest = null, sort = null).observe(viewLifecycleOwner){
//                adapter.submitData(lifecycle, it)
//            }
            passData?.search = query
            viewModel.updateFilter(
                search = result,
                brand = passData?.brand,
                lower = passData?.lower,
                higher = passData?.higher,
                sorting = passData?.sorting
            )



        }
        binding.btnResetEmpty.setOnClickListener {
            binding.linearEmpty.isVisible = false
            query = null
            passData = null
            binding.tilSearch.editText?.text = null
            viewModel.updateFilter(
                brand = null,
                lower = null,
                higher = null,
                sorting = null
            )
            binding.filterGroup.removeAllViewsInLayout()
        }
        binding.tilSearch.editText?.setText(query)

        binding.tilSearch.editText?.setOnClickListener {
            eventLogger.logButtonClick(bundleOf("btn_search" to "search"))
            showSearchDialog(query)
        }

        binding.chipFilter.setOnClickListener {
            eventLogger.logButtonClick(bundleOf("btn_filter" to "filter"))
            val filterBottomSheet = FilterBottomSheet.newInstance(passData)
            filterBottomSheet.setListener(this)
            val bundle = Bundle()
            bundle.putParcelable("filter_key", passData)
            filterBottomSheet.show(childFragmentManager, FilterBottomSheet.TAG)
        }

        val layoutManager = GridLayoutManager(context, 1)
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position == adapter.itemCount && footer.itemCount > 0 && adapter.gridLayout) 2 else 1
            }
        }

        binding.rvStore.layoutManager = layoutManager

        binding.btnChangeLayout.setOnCheckedChangeListener { buttonView, isChecked ->
            eventLogger.logButtonClick(bundleOf("btn_change_layout" to "change_layout"))
            layoutManager.spanCount = if (isChecked) 2 else 1
            adapter.gridLayout = isChecked
            viewModel.gridLayout = isChecked
        }
    }

    private fun showSearchDialog(query: String? = null) {
        val searchFragment = SearchFragment.newInstance(query)
        searchFragment.show(parentFragmentManager, SearchFragment.TAG)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onDataPassed(data: FilterItem) {
        passData = data.copy()
        binding.filterGroup.removeAllViewsInLayout()
        if (!data.sorting.isNullOrEmpty()) {
            binding.filterGroup.addView(
                Chip(requireActivity()).apply {
                    text = data.sorting
                }
            )
        }
        if (!data.brand.isNullOrEmpty()) {
            binding.filterGroup.addView(
                Chip(requireActivity()).apply {
                    text = data.brand
                }
            )
        }
        if (!data.lower.isNullOrEmpty()) {
            binding.filterGroup.addView(
                Chip(requireActivity()).apply {
                    text = data.lower
                }
            )
        }
        if (!data.higher.isNullOrEmpty()) {
            binding.filterGroup.addView(
                Chip(requireActivity()).apply {
                    text = data.higher
                }
            )
        }
    }
}
