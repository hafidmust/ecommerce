package com.hafidmust.ecommerce.ui.main.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.hafidmust.ecommerce.data.repository.CartRepository
import com.hafidmust.ecommerce.data.repository.MainRepository
import com.hafidmust.ecommerce.data.repository.WishlistRepository
import com.hafidmust.ecommerce.core.source.local.entity.CartItemEntity
import com.hafidmust.ecommerce.core.source.local.entity.DataWishListEntity
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.core.source.network.response.DetailProductResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val mainRepository: MainRepository,
    private val cartRepository: CartRepository,
    private val wishlistRepository: WishlistRepository
) : ViewModel() {

    val wishlist = wishlistRepository.getWishlist()
    private val _dataDetail = MutableLiveData<Result<DetailProductResponse.Data>?>(null)
    val dataDetail get() = _dataDetail
    fun detailProduct(productId: String) = mainRepository.detailProduct(productId)


//    private val _state = MutableStateFlow(DetailState())
//    val state = _state.asStateFlow()

    suspend fun insertCart(product: CartItemEntity) {
        val existItem = cartRepository.getItemById(product.productId).first()
        if (existItem != null) {
            val newQty = existItem.quantity + product.quantity
            cartRepository.updateQuantity(product.productId, newQty)
        } else {
            cartRepository.insertCart(product)
        }
    }

    fun getIsChecked(productId: String) = wishlistRepository.getIsChecked(productId)

    fun updateIsChecked(isChecked: Boolean, data: DataWishListEntity) {
        viewModelScope.launch {
            if (isChecked) {
                wishlistRepository.insertWishlist(data)
            } else {
                wishlistRepository.deleteWishlist(data)
            }
        }
    }

    suspend fun insertWishlist(data: DataWishListEntity) {
        wishlistRepository.insertWishlist(data)
    }

    suspend fun deleteWishlist(data: DataWishListEntity) {
        wishlistRepository.deleteWishlist(data)
    }

    suspend fun updateWishlist(ischecked: Boolean, data: DataWishListEntity) {
        wishlistRepository.updateIsChecked(ischecked, data.productId)
    }

    fun getWishlistbyId(productId: String) = wishlistRepository.getWishlistbyId(productId)

    suspend fun getQuantityByProductId(id: String) = cartRepository.getQuantityByProductId(id)

    suspend fun isQuantityValid(productId: String, quantity: Int): Boolean {
        val availableStock = cartRepository.getQuantityByProductId(productId).first()
        if (availableStock != null) {
            return availableStock < quantity
        }
        return true
    }

    private val _favoriteStatus = MutableStateFlow(false)
    val favoriteStatus: StateFlow<Boolean> get() = _favoriteStatus.asStateFlow()

    fun checkFavoriteStatus(productId: String) {
        viewModelScope.launch {
            val data = wishlistRepository.getWishlistbyId(productId).first()
            if (data != null) {
                _favoriteStatus.value = true
            }
        }
    }

    fun deleteStatus() {
        _favoriteStatus.value = false
    }

    fun toggleFavoriteStatus(data: DataWishListEntity) {
        _favoriteStatus.value = !_favoriteStatus.value
        viewModelScope.launch {
            wishlistRepository.update(data.copy(isChecked = !_favoriteStatus.value))
        }
    }
}
