package com.hafidmust.ecommerce.ui.main.detail.compose

import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.InputChip
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.hafidmust.ecommerce.core.source.network.response.DetailProductResponse

@OptIn(ExperimentalMaterial3Api::class, ExperimentalLayoutApi::class)
@Composable
fun ChipVariants(
    modifier: Modifier = Modifier,
    variants: List<DetailProductResponse.Data.ProductVariant>,
    onVariantSelected: (DetailProductResponse.Data.ProductVariant) -> Unit
) {
    var selectedIndex by remember { mutableStateOf(0) }

    FlowRow(modifier = modifier) {
        variants.forEachIndexed { index, variant ->
            val isSelected = index == selectedIndex

            InputChip(
                modifier = Modifier
                    .padding(4.dp),
                onClick = {
                    if (!isSelected) {
                        selectedIndex = index
                        onVariantSelected(variant)
                    }
                },
                label = {
                    Text(
                        text = variant.variantName,
                    )
                },
                selected = isSelected
            )
        }
    }
}
