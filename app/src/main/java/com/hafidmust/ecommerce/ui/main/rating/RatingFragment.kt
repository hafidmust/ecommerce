package com.hafidmust.ecommerce.ui.main.rating

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.fragment.findNavController
import com.hafidmust.ecommerce.MainActivity
import com.hafidmust.ecommerce.R
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.core.source.network.request.RatingRequest
import com.hafidmust.ecommerce.databinding.FragmentRatingBinding
import com.hafidmust.ecommerce.ui.main.notification.NotificationViewModel
import com.hafidmust.ecommerce.utils.rupiahFormatter
import dagger.hilt.android.AndroidEntryPoint

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [RatingFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class RatingFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var _binding: FragmentRatingBinding? = null
    private val binding get() = _binding!!

    private val viewModel: RatingViewModel by viewModels()
    private lateinit var savedStateHandle: SavedStateHandle

    private val notificationViewModel: NotificationViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentRatingBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val invoiceId = RatingFragmentArgs.fromBundle(arguments as Bundle).invoiceId
        val status = RatingFragmentArgs.fromBundle(arguments as Bundle).status
        val date = RatingFragmentArgs.fromBundle(arguments as Bundle).date
        val time = RatingFragmentArgs.fromBundle(arguments as Bundle).time
        val total = RatingFragmentArgs.fromBundle(arguments as Bundle).total
        val origin = RatingFragmentArgs.fromBundle(arguments as Bundle).origin
        Log.d("origins", origin.ifEmpty { "kosong" })
        val check = origin.isEmpty()
        binding.tvIdTransaksi.text = invoiceId
        binding.tvIdTgl.text = date
        binding.tvTime.text = time
        binding.tvTotalPembayaran.text = rupiahFormatter(total)

        binding.btnFinish.setOnClickListener {
            val rating = binding.ratingBar.rating
            val review = binding.tilReview.editText?.text

            viewModel.postRating(RatingRequest(invoiceId, rating.toInt(), review.toString())).observe(
                viewLifecycleOwner
            ) {
                when (it) {
                    is Result.Error -> {
                    }
                    Result.Loading -> {
                    }
                    is Result.Success -> {
                        findNavController().navigate(R.id.action_ratingFragment_to_main_navigation)
//                        (requireActivity() as MainActivity).backTransaction()
//                        findNavController().currentBackStack.value.forEach {
//                            Log.d("coba", it.destination.displayName)
//                        }
//                        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner){
//                            if (origin.isEmpty()){
//
//                            }else{
//                                findNavController().navigate(R.id.action_ratingFragment_to_main_navigation)
//                            }
//                        }
//                        findNavController().popBackStack()
                    }
                }
            }
        }

        Log.d("previous", findNavController().previousBackStackEntry?.destination?.displayName.toString())

        findNavController().currentBackStack.value.forEach {
            Log.d("d_n", it.destination.displayName)
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RatingFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RatingFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
