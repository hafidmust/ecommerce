package com.hafidmust.ecommerce.ui.main.store

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hafidmust.ecommerce.R
import com.hafidmust.ecommerce.core.source.network.response.ItemsItem
import com.hafidmust.ecommerce.databinding.ItemGridBinding
import com.hafidmust.ecommerce.databinding.ItemStoreBinding
import com.hafidmust.ecommerce.utils.rupiahFormatter

class StoreAdapter(private val listener: ClickListener) : PagingDataAdapter<ItemsItem, RecyclerView.ViewHolder>(
    DIFF_CALLBACK
) {

    var gridLayout = false

    class LinearViewHolder(private val binding: ItemStoreBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ItemsItem, listener: ClickListener) {
            binding.itemTvTitle.text = data.productName
            binding.itemTvPrice.text = rupiahFormatter(data.productPrice)
            binding.itemSeller.text = data.store
            binding.itemRating.text =
                binding.root.context.getString(R.string.sold, data.productRating, data.sale)
            Glide.with(binding.root)
                .load(data.image)
                .into(binding.itemIvProduct)

            binding.root.setOnClickListener {
                listener.onItemClick(data.productId)
            }
        }
    }

    class GridViewHolder(private val binding: ItemGridBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ItemsItem, listener: ClickListener){
            binding.itemGridName.text = data.productName
            binding.itemGridPrice.text = rupiahFormatter(data.productPrice)
            binding.itemGridSeller.text = data.store
            binding.itemGridRating.text = "${data.productRating} | Terual ${data.sale}"
            Glide.with(binding.root)
                .load(data.image)
                .into(binding.itemGridImage)

            binding.root.setOnClickListener {
                listener.onItemClick(data.productId)
            }
        }
    }

    companion object {
        const val VIEW_TYPE_LINEAR = 0
        const val VIEW_TYPE_GRID = 1

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ItemsItem>() {
            override fun areItemsTheSame(oldItem: ItemsItem, newItem: ItemsItem): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: ItemsItem, newItem: ItemsItem): Boolean {
                return oldItem.productId == newItem.productId
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_LINEAR -> LinearViewHolder(
                ItemStoreBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )

            VIEW_TYPE_GRID -> GridViewHolder(
                ItemGridBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )

            else -> throw IllegalArgumentException("undefined view type")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (gridLayout) VIEW_TYPE_GRID else VIEW_TYPE_LINEAR
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LinearViewHolder -> {
                getItem(position)?.let { holder.bind(it, listener) }
            }

            is GridViewHolder -> {
                getItem(position)?.let {
                    holder.bind(it, listener)
                }
            }

            else -> throw IllegalArgumentException("undefined view type")
        }
    }
}

interface ClickListener {
    fun onItemClick(idProduct: String)
}
