package com.hafidmust.ecommerce.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hafidmust.ecommerce.core.source.local.entity.DataWishListEntity
import com.hafidmust.ecommerce.databinding.ItemWishGridBinding
import com.hafidmust.ecommerce.databinding.ItemWishlistBinding
import com.hafidmust.ecommerce.utils.rupiahFormatter

class WishlistAdapter(
    val onDeleteClick: (DataWishListEntity) -> Unit,
    val onCartClick: (DataWishListEntity) -> Unit
) : ListAdapter<DataWishListEntity, RecyclerView.ViewHolder>(WishlistDiffUtil) {

    var gridLayout = false

    inner class ViewHolder(private val binding: ItemWishlistBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: DataWishListEntity) {
            binding.apply {
                Glide.with(binding.root)
                    .load(data.image)
                    .into(itemIvProduct)
                itemTvPrice.text = rupiahFormatter(data.productPrice)
                itemTvTitle.text = data.productName
                itemSeller.text = data.store
                itemRating.text = "${data.productRating} | Terjual ${data.sale}"
                btnDelete.setOnClickListener {
                    onDeleteClick(data)
                }
                btnCart.setOnClickListener {
                    onCartClick(data)
                }
            }
        }
    }

    class GridViewHolder(private val binding: ItemWishGridBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: DataWishListEntity) {
            binding.apply {
                Glide.with(binding.root)
                    .load(data.image)
                    .into(itemGridImage)
                itemGridPrice.text = rupiahFormatter(data.productPrice)
                itemGridName.text = data.productName
                itemGridSeller.text = data.store
                itemGridRating.text = "${data.productRating} | Terjual ${data.sale}"
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (gridLayout) VIEW_TYPE_GRID else VIEW_TYPE_LINEAR
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_LINEAR -> ViewHolder(
                ItemWishlistBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )

            VIEW_TYPE_GRID -> GridViewHolder(
                ItemWishGridBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )

            else -> throw IllegalArgumentException("undefined view type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolder -> {
                holder.bind(getItem(position))
            }

            is GridViewHolder -> {
                holder.bind(getItem(position))
            }
        }
    }

    companion object {
        const val VIEW_TYPE_LINEAR = 0
        const val VIEW_TYPE_GRID = 1

        private val WishlistDiffUtil = object : DiffUtil.ItemCallback<DataWishListEntity>() {
            override fun areItemsTheSame(
                oldItem: DataWishListEntity,
                newItem: DataWishListEntity
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: DataWishListEntity,
                newItem: DataWishListEntity
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}
