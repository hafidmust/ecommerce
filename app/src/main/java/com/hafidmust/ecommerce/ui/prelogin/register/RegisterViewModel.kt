package com.hafidmust.ecommerce.ui.prelogin.register

import androidx.lifecycle.ViewModel
import com.hafidmust.ecommerce.data.repository.AuthRepository
import com.hafidmust.ecommerce.core.source.network.request.UserRequest
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(private val authRepository: AuthRepository) :
    ViewModel() {
    fun register(email: String, password: String) = authRepository.register(
        UserRequest(email = email, password = password, firebaseToken = "")
    )

    fun registerEvent(email: String) = authRepository.registerEvent(email)
}
