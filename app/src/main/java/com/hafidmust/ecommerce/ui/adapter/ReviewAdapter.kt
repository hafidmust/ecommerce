package com.hafidmust.ecommerce.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hafidmust.ecommerce.core.source.network.response.ProductReviewResponse
import com.hafidmust.ecommerce.databinding.ItemReviewBinding

class ReviewAdapter(private val reviews: List<ProductReviewResponse.DataReview>) :
    RecyclerView.Adapter<ReviewAdapter.ViewHolder>() {
    inner class ViewHolder(private val binding: ItemReviewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ProductReviewResponse.DataReview) {
            binding.apply {
                Glide.with(binding.root)
                    .load(data.userImage)
                    .into(imgReview)

                tvReviewName.text = data.userName
                tvDetail.text = data.userReview
                rbReview.rating = data.userRating.toFloat()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = ItemReviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = reviews.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(reviews[position])
    }
}
