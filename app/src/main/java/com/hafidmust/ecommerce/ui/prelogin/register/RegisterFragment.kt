package com.hafidmust.ecommerce.ui.prelogin.register

import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.hafidmust.ecommerce.R
import com.hafidmust.ecommerce.core.source.network.response.RegisterResponse
import com.hafidmust.ecommerce.core.source.network.Result
import com.hafidmust.ecommerce.databinding.FragmentRegisterBinding
import com.hafidmust.ecommerce.core.di.EventLogger
import com.hafidmust.ecommerce.utils.Validation
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class RegisterFragment : Fragment() {

    private val viewModel: RegisterViewModel by viewModels()

    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!
    private val emailStateFlow = MutableStateFlow("")
    private val passwordStateFlow = MutableStateFlow("")

    @Inject
    lateinit var eventLogger: EventLogger

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val validateForm: Flow<Boolean> =
            combine(emailStateFlow, passwordStateFlow) { email, password ->
                Validation.email(email) && Validation.password(password)
            }

        viewLifecycleOwner.lifecycleScope.launch {
            validateForm.collect {
                binding.btnDaftar.isEnabled = it
            }
            Log.d("register", emailStateFlow.value)
        }

        setupEmailValidation()
        setupPasswordValidation()
        termAndPrivacyText()

        binding.btnLogin.setOnClickListener {
            eventLogger.logButtonClick(bundleOf("btn_login" to "login"))
            it?.findNavController()?.navigate(R.id.action_registerFragment_to_loginFragment)
        }
        binding.btnDaftar.setOnClickListener {
            eventLogger.logButtonClick(bundleOf("btn_daftar" to "daftar"))
            val email = binding.outlineEmail.editText?.text.toString().trim()
            val password = binding.outlinePassword.editText?.text.toString().trim()
            eventLogger.logEvent(
                "register",
                Bundle().apply {
                    putString("email", email)
                }
            )
            viewModel.register(email, password).observe(viewLifecycleOwner, registerObserver)

//            findNavController().navigate(R.id.action_registerFragment_to_profileFragment)
        }
    }

    private val registerObserver = Observer<Result<RegisterResponse>> {
        when (it) {
            is Result.Success -> {
                Firebase.messaging.subscribeToTopic("promo")
                findNavController().navigate(R.id.action_registerFragment_to_profileFragment)
            }

            is Result.Error -> {
                binding.progressBar.isVisible = false
                Toast.makeText(context, it.msg, Toast.LENGTH_SHORT).show()
            }

            Result.Loading -> {
                binding.progressBar.visibility = View.VISIBLE
            }
        }
    }

    private fun setupEmailValidation() {
        binding.outlineEmail.editText?.doOnTextChanged { text, _, _, _ ->
            emailStateFlow.value = text.toString()
            if (Validation.email(text) || text?.length == 0) {
                binding.outlineEmail.error = null
            } else {
                binding.outlineEmail.error = getString(R.string.email_not_valid)
            }
        }
    }

    private fun setupPasswordValidation() {
        binding.outlinePassword.editText?.doOnTextChanged { text, _, _, _ ->
            passwordStateFlow.value = text.toString()
            if (Validation.password(text) || text?.length == 0) {
                binding.outlinePassword.error = null
            } else {
                binding.outlinePassword.error = getString(R.string.password_not_valid)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun termAndPrivacyText() {
        val text =
            "Dengan daftar disini, kamu menyetujui Syarat & Ketentuan serta Kebijakan Privasi TokoPhincon."
        val spannableString = SpannableString(text)
        val color =
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.light_primary))
        val colorPrimary =
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.light_primary))
        spannableString.setSpan(
            color,
            text.indexOf("Syarat & Ketentuan"),
            text.indexOf("Syarat & Ketentuan") + "Syarat & Ketentuan".length,
            0
        )
        spannableString.setSpan(
            colorPrimary,
            text.indexOf("Kebijakan Privasi"),
            text.indexOf("Kebijakan Privasi") + "Kebijakan Privasi".length,
            0
        )
        binding.tvTermService.text = spannableString
    }
}
