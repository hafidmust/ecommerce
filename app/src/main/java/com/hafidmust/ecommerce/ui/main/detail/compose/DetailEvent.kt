package com.hafidmust.ecommerce.ui.main.detail.compose

import com.hafidmust.ecommerce.core.source.network.response.DetailProductResponse

sealed class DetailEvent {
    data class OnGetDetail(
        val data: List<DetailProductResponse.Data>,
        val id: String
    ) : DetailEvent()
}
