package com.hafidmust.ecommerce.ui.main.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.hafidmust.ecommerce.data.repository.AuthRepository
import com.hafidmust.ecommerce.data.repository.CartRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val cartRepository: CartRepository
) : ViewModel() {

//    private val _getDataName = MutableLiveData("")
//    val getDataName: LiveData<String>
//        get() = _getDataName

    init {
//        getNameProfile()
    }

    private fun getNameProfile() {
        viewModelScope.launch {
            val name = authRepository.getNameProfile()
//            _getDataName.value = name
        }
    }

    fun getName(): LiveData<String> {
        return authRepository.getNameProfile().asLiveData()
    }

    fun getToken() = authRepository.getRefreshToken()

    fun getQuantity() = cartRepository.getQuantity()
    fun logout() {
        viewModelScope.launch {
            authRepository.logout()
        }
    }

    fun setTheme(isDark: Boolean) {
        viewModelScope.launch {
            authRepository.setTheme(isDark)
        }
    }

    suspend fun getTheme() = authRepository.getTheme()
}
