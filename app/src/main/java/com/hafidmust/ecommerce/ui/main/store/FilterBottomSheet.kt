package com.hafidmust.ecommerce.ui.main.store

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import com.hafidmust.ecommerce.databinding.FragmentFilterBottomSheetBinding
import com.hafidmust.ecommerce.core.di.EventLogger
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val ARG_FILTER = "filter"

@AndroidEntryPoint
class FilterBottomSheet : BottomSheetDialogFragment() {

    private var dataFilter: FilterItem? = null

    private var listener: BottomSheetListener? = null

    private var _binding: FragmentFilterBottomSheetBinding? = null
    private val binding get() = _binding!!
    private var dataFromFragment: FilterItem? = null

    @Inject
    lateinit var eventLogger: EventLogger

    private val viewModel: StoreViewModel by viewModels(
        ownerProducer = { requireParentFragment() }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            dataFilter = it.getParcelable(ARG_FILTER)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFilterBottomSheetBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dataFromFragment = arguments?.getParcelable("filter_key")

//        resetValue()
        dataFromFragment?.id_sorting?.let { binding.chipSorting.check(it) }
        dataFromFragment?.id_brand?.let { binding.chipCategory.check(it) }
        binding.tilLowerPrice.editText?.setText(dataFromFragment?.lower)
        binding.tilHighPrice.editText?.setText(dataFromFragment?.higher)

        var dataSorting: String? = null
        var dataCategory: String? = null
        var lowerPrice: String? = null
        var highPrice: String? = null
        var idSort: Int? = null
        var idBrand: Int? = null

        dataSorting = dataFromFragment?.sorting
        dataCategory = dataFromFragment?.brand
        lowerPrice = dataFromFragment?.lower
        highPrice = dataFromFragment?.higher

        lifecycleScope.launch {
            viewModel.filterState.collect {
                if (dataFromFragment?.sorting != null || dataFromFragment?.brand != null || dataFromFragment?.lower != null || dataFromFragment?.higher != null) {
                    binding.btnReset.visibility = View.VISIBLE
                } else {
                    binding.btnReset.visibility = View.INVISIBLE
                }
            }
        }

        binding.btnReset.setOnClickListener {
            binding.chipSorting.clearCheck()
            binding.chipCategory.clearCheck()
            binding.tilLowerPrice.editText?.text?.clear()
            binding.tilHighPrice.editText?.text?.clear()
//            listener?.onDataPassed(FilterItem())
//            viewModel.resetFilter()
            dataFromFragment = null
            dataSorting = null
            dataCategory = null
            idSort = null
            idBrand = null
        }

        binding.chipSorting.setOnCheckedStateChangeListener { group, checkedIds ->
            checkedIds.forEach {
                val checkedId = group.findViewById<Chip>(it)
                if (checkedId != null) {
                    dataSorting = checkedId.text.toString()
                    idSort = it

//                    Toast.makeText(context, checkedId.text, Toast.LENGTH_SHORT).show()
                }
            }
            val selectedID = checkedIds.firstOrNull()
            val selectedChip: Chip? = view.findViewById(selectedID ?: -1)

            if (selectedChip != null) {
                val text = selectedChip.text.toString()
                dataSorting = text
            }

            binding.btnReset.isVisible = true
        }

        binding.chipCategory.setOnCheckedStateChangeListener { group, checkedIds ->
            checkedIds.forEach {
                val selectedChip: Chip? = group.findViewById(it ?: -1)
                if (selectedChip != null) {
                    val selectedText = selectedChip.text.toString()
                    dataCategory = selectedText
                    idBrand = it
//                    Toast.makeText(context, selectedChip.text, Toast.LENGTH_SHORT).show()

                    binding.chipCategory.isVisible = true
                }
            }
        }

        binding.tilLowerPrice.editText?.doOnTextChanged { text, _, _, _ ->
            lowerPrice = text.toString()
        }

        binding.tilHighPrice.editText?.doOnTextChanged { text, _, _, _ ->
            highPrice = text.toString()
        }

        binding.btnShowProduct.setOnClickListener {
//            lowerPrice = binding.tilLowerPrice.editText?.text.toString().trim()
//            highPrice = binding.tilHighPrice.editText?.text.toString().trim()
//            listener?.onDataPassed(
//                sorting = dataSorting,
//                category = dataCategory,
//                lower = lowerPrice,
//                higher = highPrice
//            )
            listener?.onDataPassed(
                FilterItem(
                    sorting = dataSorting,
                    brand = dataCategory,
                    lower = lowerPrice,
                    higher = highPrice,
                    id_brand = idBrand,
                    id_sorting = idSort
                )
            )
            viewModel.updateFilter(
                idSorting = idSort,
                sorting = dataSorting,
                brand = dataCategory,
                idBrand = idBrand,
                lower = lowerPrice,
                higher = highPrice
            )
            eventLogger.logEvent(
                "filter",
                Bundle().apply {
                    putString("sorting", dataSorting)
                    putString("brand", dataCategory)
                    putString("lower", lowerPrice)
                    putString("higher", highPrice)
                }
            )
            dismiss()
        }
    }

    private fun resetValue() {
//        viewModel.updateFilter(idSorting = null, sorting = null, brand = null, idBrand = null, lower = null, higher = null)
    }

    fun setListener(listener: BottomSheetListener) {
        this.listener = listener
    }

    companion object {
        const val TAG = "FilterBottomSheet"
        const val SORTING = "Sorting"
        const val CATEGORY = "Category"
        const val LOWER = "Lower"
        const val HIGHER = "Higher"

        @JvmStatic
        fun newInstance(data: FilterItem?) = FilterBottomSheet().apply {
            arguments = Bundle().apply {
                putParcelable("filter_key", data)
            }
        }
    }
}

interface BottomSheetListener {
    //    fun onDataPassed(sorting: String?, category: String?, lower: String?, higher: String?)
    fun onDataPassed(data: FilterItem)
}
