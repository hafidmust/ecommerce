package com.hafidmust.ecommerce.ui.main.payment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.hafidmust.ecommerce.databinding.FragmentPaymentMethodBinding
import com.hafidmust.ecommerce.core.di.EventLogger
import com.hafidmust.ecommerce.ui.adapter.PaymentMethodAdapter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PaymentMethodFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

@AndroidEntryPoint
class PaymentMethodFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var _binding: FragmentPaymentMethodBinding? = null
    private val binding get() = _binding!!

    private val viewModel: PaymentMethodViewModel by viewModels()

    @Inject lateinit var eventLogger: EventLogger

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentPaymentMethodBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navigateBack()
        binding.progressBar.isVisible = true
        viewModel.listPayment.observe(viewLifecycleOwner) {
            binding.progressBar.isVisible = false
            if (it != null) {
                val adapter = PaymentMethodAdapter(it)
                adapter.submitList(it)
                binding.rvParentPayment.adapter = adapter
            }
        }

//        viewModel.getPaymentMethod().observe(viewLifecycleOwner){
//            when(it){
//                is Result.Error -> {
//
//                }
//                Result.Loading -> {
//
//                }
//                is Result.Success -> {
//                    val adapter = PaymentMethodAdapter(it.data)
//                    adapter.submitList(it.data)
//                    binding.rvParentPayment.adapter = adapter
//                }
//            }
//        }
    }

    private fun navigateBack() {
        binding.topAppBar.setNavigationOnClickListener {
            eventLogger.logPaymentInfo(bundleOf("payment_info" to "payment_info"))
            findNavController().navigateUp()
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PaymentMethodFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PaymentMethodFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
