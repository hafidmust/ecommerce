package com.hafidmust.ecommerce.ui.main.store.search

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.progressindicator.CircularProgressIndicator
import com.google.android.material.textfield.TextInputLayout
import com.hafidmust.ecommerce.R
import com.hafidmust.ecommerce.core.di.EventLogger
import com.hafidmust.ecommerce.core.source.network.Result
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject


private const val ARG_SEARCH = "search"

@AndroidEntryPoint
class SearchFragment : DialogFragment() {

    private var searchQuery: String? = null

    private val viewModel: SearchViewModel by viewModels()

    @Inject
    lateinit var eventLogger: EventLogger

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            searchQuery = it.getString(ARG_SEARCH)
        }
        setStyle(STYLE_NORMAL, R.style.DialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rvSearch = view.findViewById<RecyclerView>(R.id.rv_search)
        val progressBar = view.findViewById<ProgressBar>(R.id.progressBar)

        val tilSearch = view.findViewById<TextInputLayout>(R.id.til_search)
        tilSearch.editText?.setText(searchQuery)

        tilSearch.editText?.requestFocus()
        dialog.let {
            it?.window?.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE
            )
        }
        Log.d("search2", searchQuery.toString())
        tilSearch.editText?.doOnTextChanged { text, _, _, _ ->
            lifecycleScope.launch {
                viewModel.queryChannel.value = text.toString()
            }
        }

        tilSearch.editText?.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                lifecycleScope.launch {
                    val data = tilSearch.editText?.text.toString()
                    eventLogger.logSearch(bundleOf("search" to data))
                    parentFragmentManager.setFragmentResult(
                        "queryKey",
                        bundleOf("bundleKey" to data)
                    )
                }
                dismiss()
            }
            true
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.searchResult.collect { result ->
                when (result) {
                    is Result.Success -> {
                        progressBar.isVisible = false
                        val adapter =
                            SearchAdapter(
                                result.data ?: emptyList(),
                                object : ClickListener {
                                    override fun onItemClick(data: String) {
//                                Toast.makeText(context, data, Toast.LENGTH_SHORT).show()
                                        parentFragmentManager.setFragmentResult(
                                            "queryKey",
                                            bundleOf("bundleKey" to data)
                                        )
                                        dismiss()
                                    }
                                }
                            )
                        rvSearch.adapter = adapter
                    }

                    is Result.Error -> {
                        Toast.makeText(context, result.msg, Toast.LENGTH_SHORT).show()
                        progressBar.isVisible = false
                    }

                    is Result.Loading -> {
                        progressBar.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    companion object {

        const val TAG = "search"

        @JvmStatic
        fun newInstance(query: String?) =
            SearchFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_SEARCH, query)
                }
            }
    }
}
