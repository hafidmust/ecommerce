package com.hafidmust.ecommerce.ui.main.store.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import com.hafidmust.ecommerce.data.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flatMapLatest
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(private val mainRepository: MainRepository) :
    ViewModel() {

    val queryChannel = MutableStateFlow("")

    val searchResult = queryChannel
        .debounce(3000)
        .distinctUntilChanged()
        .filter {
            it.trim().isNotEmpty()
        }
        .flatMapLatest {
            mainRepository.searchProduct(it).asFlow()
        }

    fun searchProducts(query: String) = mainRepository.searchProduct(query)
}
