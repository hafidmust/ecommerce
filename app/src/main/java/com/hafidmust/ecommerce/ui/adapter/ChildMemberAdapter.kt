package com.hafidmust.ecommerce.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hafidmust.ecommerce.core.source.network.response.ItemItemPayment
import com.hafidmust.ecommerce.databinding.ItemContentPaymentBinding

open class ChildMemberAdapter(val data: List<ItemItemPayment>) :
    RecyclerView.Adapter<ChildMemberAdapter.ViewHolder>() {

    inner class ViewHolder(private val binding: ItemContentPaymentBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ItemItemPayment) {
            binding.tvPaymentMethod.text = data.label
            Glide.with(binding.root)
                .load(data.image)
                .into(binding.ivPaymentMethod)

            if (!data.status) {
                binding.root.alpha = 0.5f
                binding.root.isEnabled = false
            }

            binding.itemChild.setOnClickListener {
                binding.root.findNavController().previousBackStackEntry?.savedStateHandle?.set(
                    "paymentMethod",
                    data
                )
                binding.root.findNavController().popBackStack()
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ChildMemberAdapter.ViewHolder {
        val binding =
            ItemContentPaymentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ChildMemberAdapter.ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }
}
