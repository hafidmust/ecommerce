package com.hafidmust.ecommerce.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hafidmust.ecommerce.R
import com.hafidmust.ecommerce.core.source.local.entity.CartItemEntity
import com.hafidmust.ecommerce.databinding.ItemCartBinding
import com.hafidmust.ecommerce.utils.rupiahFormatter

class CartAdapter(
    private val onDeleteItem: (CartItemEntity) -> Unit,
    private val onAddQuantity: (CartItemEntity) -> Unit,
    private val onSubtractQuantity: (CartItemEntity) -> Unit,
    private val onCheckedItem: (String, Boolean) -> Unit
) : ListAdapter<CartItemEntity, CartAdapter.CartViewHolder>(CardDiffUtils()) {
    inner class CartViewHolder(private val binding: ItemCartBinding) :
        RecyclerView.ViewHolder(binding.root) {

        val chekbox = binding.checkbox

        @SuppressLint("SetTextI18n")
        fun bind(cartItemEntity: CartItemEntity) {
            with(binding) {
                checkbox.isChecked = cartItemEntity.isChecked
                itemTvTitle.text = cartItemEntity.productName
                itemVariant.text = cartItemEntity.productVariant
                if (cartItemEntity.stock < 10) {
                    itemStok.setTextColor(binding.root.context.getColor(R.color.error))
                }
                itemStok.text = if (cartItemEntity.stock < 10) "Tersisa ${cartItemEntity.stock} " else "Stok ${cartItemEntity.stock}"
                itemPrice.text = rupiahFormatter(cartItemEntity.productPrice)
                Glide.with(binding.root)
                    .load(cartItemEntity.image)
                    .into(binding.itemIvProduct)
                tvQty.text = cartItemEntity.quantity.toString()

                binding.btnDelete.setOnClickListener {
                    onDeleteItem(cartItemEntity)
                }
                binding.btnRemove.setOnClickListener {
                    onSubtractQuantity(cartItemEntity)
//                    onCheckedItem(cartItemEntity.productId,true)
                }
                binding.btnAdd.setOnClickListener {
                    onAddQuantity(cartItemEntity)
//                    onCheckedItem(cartItemEntity.productId,true)
                }
                binding.checkbox.setOnClickListener {
                    onCheckedItem(cartItemEntity.productId, binding.checkbox.isChecked)
                }
            }
        }
    }

    class CardDiffUtils : DiffUtil.ItemCallback<CartItemEntity>() {
        override fun areItemsTheSame(oldItem: CartItemEntity, newItem: CartItemEntity): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: CartItemEntity, newItem: CartItemEntity): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val binding = ItemCartBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CartViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        holder.bind(getItem(position))
//        val data = getItem(position)
//        holder.chekbox.setOnCheckedChangeListener { _, isChecked ->
//            onCheckedItem(data.productId, isChecked)
//        }
    }
}
