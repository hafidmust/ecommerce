package com.hafidmust.ecommerce.ui.main.notification

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hafidmust.ecommerce.data.repository.MainRepository
import com.hafidmust.ecommerce.core.source.local.entity.NotificationEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NotificationViewModel @Inject constructor(
    private val mainRepository: MainRepository
) : ViewModel() {
    suspend fun insertNotification(data: NotificationEntity) = mainRepository.insertNotification(data)

    suspend fun updateNotification(isRead: Boolean, id: Int) = mainRepository.updateNotification(isRead, id)

    private val _notification = MutableStateFlow<List<NotificationEntity>>(emptyList())
    private val _unreadNotification = MutableStateFlow(0)
    val getAllNotification get() = _notification
    val getUnreadNotification get() = _unreadNotification

    init {
        viewModelScope.launch {
            mainRepository.getAllNotification().collect {
                _notification.value = it
            }
            mainRepository.getUnreadNotification().collect {
                _unreadNotification.value = it
            }
        }
    }

    fun deleteAllNotification() {
        viewModelScope.launch(Dispatchers.IO) {
            mainRepository.deleteAllNotification()
        }
    }
}
