package com.hafidmust.ecommerce.ui.main.store

import android.os.Parcelable
import androidx.annotation.IdRes
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize
@Keep
@Parcelize
data class FilterItem(
    @IdRes var id_sorting: Int? = null,
    var sorting: String? = null,
    @IdRes var id_brand: Int? = null,
    var brand: String? = null,
    var lower: String? = null,
    var higher: String? = null,
    var search: String? = null
) : Parcelable
