package com.hafidmust.ecommerce

import android.Manifest
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import com.hafidmust.ecommerce.databinding.ActivityMainBinding
import com.hafidmust.ecommerce.ui.prelogin.onboarding.OnboardingViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: OnboardingViewModel by viewModels()

    private val navHostFragment: NavHostFragment by lazy {
        supportFragmentManager.findFragmentById(R.id.nhf_main) as NavHostFragment
    }

    private val navController by lazy {
        navHostFragment.navController
    }

    private val requestNotificationPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ){}

    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen()
        super.onCreate(savedInstanceState)


        lifecycleScope.launch {
            viewModel.getTheme().collect { getTheme ->
                if (getTheme) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                } else AppCompatDelegate.setDefaultNightMode(
                    AppCompatDelegate.MODE_NIGHT_NO
                )
            }
        }
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= 33) {
            requestNotificationPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
        }

        // get session

        // check session
        runBlocking {
            val session = viewModel.getAccessToken().first()
            val refresh = viewModel.getRefreshToken().first()
            if (session.isNullOrEmpty() || refresh.isNullOrEmpty()) {
                navController.navigate(R.id.main_to_prelogin)
            }
        }
        lifecycleScope.launch {
//            val data = viewModel.onboardingState.first() //true
//
//            if (data) {
//                navController.navigate(R.id.action_loginFragment_to_onboardingFragment)
//                viewModel.setOnboardingState(false)
//            } else {
// //                navController.navigate(R.id.app_navigation)
//            }
        }
    }

    fun checkUsername() {
        navController.navigate(R.id.main_to_profile)
    }

    fun logout() {
        navController.navigate(R.id.main_to_prelogin)
    }

    fun detail(idProduct: String) {
        val toDetailProduct = AppNavigationDirections.mainToDetail(idProduct)
        navController.navigate(toDetailProduct)
    }

    fun cart() {
        navController.navigate(R.id.main_to_cart)
    }

    fun toNotification() {
        navController.navigate(R.id.main_to_notification)
    }

    fun backTransaction() {
        navController.navigateUp()
    }

    fun toRating(
        invoiceId: String,
        status: Boolean,
        date: String,
        time: String,
        payment: String,
        total: Int,
        origin: String
    ) {
        val toRating = AppNavigationDirections.transactionToRating(
            invoiceId,
            status,
            date,
            time,
            payment,
            total,
            origin,
        )
        navController.navigate(toRating)
    }

    fun screen() {
        navController.navigate(R.id.main_to_screen)
    }
}
