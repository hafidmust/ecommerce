package com.hafidmust.ecommerce.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.navigation.NavDeepLinkBuilder
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.hafidmust.ecommerce.MainActivity
import com.hafidmust.ecommerce.R
import com.hafidmust.ecommerce.data.repository.MainRepository
import com.hafidmust.ecommerce.core.source.local.entity.NotificationEntity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.runBlocking
import java.net.URL
import javax.inject.Inject

@AndroidEntryPoint
class FirebaseCloudMessaging : FirebaseMessagingService() {

    @Inject lateinit var mainRepository: MainRepository

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d(TAG, "onNewToken: $token")
        Log.d("onNewToken", "onNewToken: $token")
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        Log.d("ini_tag", FirebaseCloudMessaging::class.java.simpleName)
        Log.d(TAG, "From: ${message.from}")
        Log.d(TAG, "Message data payload: " + message.data)
        Log.d(TAG, "Message Notification Body: ${message.notification?.body}")
        Log.d(TAG, "title: " + message.data.getValue("title"))
        sendNotification(message.data.getValue("title"), message.data.getValue("body"), message.data.getValue("image"))
        runBlocking {
            mainRepository.insertNotification(
                NotificationEntity(
                    title = message.data.getValue("title"),
                    body = message.data.getValue("body"),
                    image = message.data.getValue("image"),
                    date = "",
                    time = "",
                    type = "promo"
                )
            )
        }
    }

    private fun sendNotification(title: String?, body: String?, image: String?) {
        val url = URL(image)
        val imageBody = BitmapFactory.decodeStream(url.openConnection().getInputStream())
        val contentIntent = Intent(applicationContext, MainActivity::class.java)
        val pendingIntent = NavDeepLinkBuilder(applicationContext)
            .setComponentName(MainActivity::class.java)
            .setGraph(R.navigation.app_navigation)
            .setDestination(R.id.notificationFragment)
            .createPendingIntent()

        val notificationBuilder = NotificationCompat.Builder(
            applicationContext,
            NOTIFICATION_CHANNEL_ID
        )
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(title)
            .setContentText(body)
            .setStyle(NotificationCompat.BigPictureStyle().bigPicture(imageBody))
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                NOTIFICATION_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationBuilder.setChannelId(NOTIFICATION_CHANNEL_ID)
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build())
    }

    companion object {
        private val TAG = FirebaseCloudMessaging::class.java.simpleName
        private const val NOTIFICATION_ID = 1
        private const val NOTIFICATION_CHANNEL_ID = "Firebase Channel"
        private const val NOTIFICATION_CHANNEL_NAME = "promo"
    }
}
