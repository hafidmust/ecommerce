package com.hafidmust.ecommerce.utils

import android.util.Patterns

object Validation {
    fun email(text: CharSequence?): Boolean {
        return if (!text.isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(text).matches()) {
            return true
        } else {
            false
        }
    }

    fun password(text: CharSequence?): Boolean {
        return (text?.length ?: 0) >= 8
    }
}
